#ifndef INCLUDE_REGULATORINJUNCTIVERELAXATION_H_
#define INCLUDE_REGULATORINJUNCTIVERELAXATION_H_

#include "repast_hpc/SharedContext.h"

#include "Regulator.h"
#include "Agent.h"
#include "InjunctiveNormEntity.h"

class RegulatorInjunctiveRelaxation : public Regulator {

private:
	repast::SharedContext<Agent> *mpContext;
	double* mpAdjustmentLevelsPrev;
	InjunctiveNormEntity *mpInjunctiveNormEntity;
	double** mpProportionCurrentDrinkers;

public:
	RegulatorInjunctiveRelaxation(repast::SharedContext<Agent> *context);
	~RegulatorInjunctiveRelaxation();

	void updateAdjustmentLevel() override;

	void setInjNormEntity(InjunctiveNormEntity *pInjNormEntity);

	double getAdjustmentLevelPrev(int groupId);
	double getPropotionCurrentDrinkers(int groupId, int schemaId);

	int* mTransformationalTriggerCount;
	void resetCount();
	void resetAdjustmentLevel();
};


#endif /* INCLUDE_REGULATORINJUNCTIVERELAXATION_H_ */
