#ifndef INCLUDE_INJUNCTIVE_NORM_ENTITY_H_
#define INCLUDE_INJUNCTIVE_NORM_ENTITY_H_

#include <string>

#include "StructuralEntity.h"
#include "NormReferenceGroup.h"

class InjunctiveNormEntity : public StructuralEntity {

private:
	double** mpInjunctiveNormsPrev;
	int mIntervalPunish;
	int mIntervalRelax;

	//EXPERIMENT 3
	//bool mInterventionFlag = false;

public:
	InjunctiveNormEntity(std::vector<Regulator*> regulatorList, std::vector<double> powerList,
			int intervalPunish, int intervalRelax,
			std::vector<std::string> normDataPrev);
	~InjunctiveNormEntity();

	void doTransformation() override;
	double getInjNormPrev(int groupId, int schemaId); //getter to read value from inj norm array
};

#endif /* INCLUDE_INJUNCTIVE_NORM_ENTITY_H_ */
