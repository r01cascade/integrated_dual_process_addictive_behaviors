#ifndef INCLUDE_NORMS_GLOBALS_H_
#define INCLUDE_NORMS_GLOBALS_H_

#include "NormReferenceGroup.h"

// Globals for the injunctive norms transformational mechanisms
extern int INJUNCTIVE_THRESHOLD; // Threshold above which society punishes drinking
extern double INJUNCTIVE_PROPORTION; // Proportion of people that have to be above the injunctive threshold to trigger
extern double INJUNCTIVE_ADJUSTMENT; // Factor by which the injunctive norm decreases when criteria are met for too heavy drinking
extern double INJ_RELAXATION_PREV_ADJUSTMENT; // the factor by which the injunctive norm gamma increases when relaxation criteria are met
extern double INJ_PUNISHMENT_PREV_ADJUSTMENT; // the factor by which the injunctive norm gamma decreases when punishment criteria are met

// Globals for descriptive norms
extern const int DESCRIPTIVE_INCUBATION_PERIOD; // number of days within which the descriptive norm has to be above the injunctive norm
extern const double DESCRIPTIVE_INCUBATION_PERCENT; // percentage of days over which the descriptive norm has to be above the injunctive norm

// This is the bias in perceiving quantity
extern double PERCEPTION_BIAS; // a weighting factor when perceiving descriptive norms

// Time periods over which drinking behaviour it is evaluated
extern int N_DAYS_DESCRIPTIVE; // is used to generate descriptive norms on prevalence and average quantity
extern int COMP_DAYS_PUNISH; // Used in the binge punishment (average drinks in comp days > injunctive threshold)
extern int COMP_DAYS_RELAX; // Used in the injunctive relaxation (prevalence of one drink over comp days)

extern const int MAX_DRINK_LEVEL;
extern const int MIN_DRINK_LEVEL;

// CP Edit: add the discounting factor to decrease payoff with increasing drinks
extern double DISCOUNT_MALE;
extern double DISCOUNT_FEMALE;

extern double DESIRE_MULTIPLIER_ABSTAINER;
extern double DESIRE_MULTIPLIER_DRINKER;

extern NormReferenceGroup *P_REFERENCE_GROUP;

#endif /* INCLUDE_NORMS_GLOBALS_H_ */
