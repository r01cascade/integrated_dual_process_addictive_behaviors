#include "repast_hpc/SVDataSetBuilder.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include <vector>
#include <stdio.h>

#include "NormModel.h"
#include "Agent.h"
#include "TheoryMediator.h"
#include "Theory.h"
#include "StructuralEntity.h"
#include "MediatorForOneTheory.h"
#include "NormTheory.h"
#include "InjunctiveNormEntity.h"
#include "RegulatorInjunctiveBingePunishment.h"
#include "RegulatorInjunctiveRelaxation.h"
#include "DescriptiveNormEntity.h"
#include "NormGlobals.h"

NormModel::NormModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm) :
					Model(propsFile, argc, argv, comm) {
	// Read parameters for injunctive norms and payoff adjustment from model.props file
	std::string strInjThreshold = props->getProperty("norms.injunctive.threshold");
	std::string strInjProportion = props->getProperty("norms.injunctive.proportion");
	std::string strInjRelaxationPrevAdjustment = props->getProperty("norms.injunctive.relaxation.prev.adjustment");
	std::string strInjPunishmentPrevAdjustment = props->getProperty("norms.injunctive.punishment.prev.adjustment");
	std::string strNDaysDes = props->getProperty("norms.n.days.descriptive");
	std::string strCompDaysPunish = props->getProperty("norms.com.days.punish");
	std::string strCompDaysRelax = props->getProperty("norms.com.days.relax");
	std::string strPerceptionBias = props->getProperty("bias.factor");
	std::string strDiscountMale = props->getProperty("discount.male");
	std::string strDiscountFemale = props->getProperty("discount.female");
	std::string strDesireMultipilerAbstainer = props->getProperty("desire.multiplier.abstainer");
	std::string strDesireMultipilerDrinker = props->getProperty("desire.multiplier.drinker");

	if (!strInjThreshold.empty()) { INJUNCTIVE_THRESHOLD = repast::strToInt(strInjThreshold); }
	if (!strInjProportion.empty()) { INJUNCTIVE_PROPORTION = repast::strToDouble(strInjProportion); }
	if (!strInjRelaxationPrevAdjustment.empty()) { INJ_RELAXATION_PREV_ADJUSTMENT = repast::strToDouble(strInjRelaxationPrevAdjustment); }
	if (!strInjPunishmentPrevAdjustment.empty()) { INJ_PUNISHMENT_PREV_ADJUSTMENT = repast::strToDouble(strInjPunishmentPrevAdjustment); }
	if (!strNDaysDes.empty()) {N_DAYS_DESCRIPTIVE = repast::strToDouble(strNDaysDes); }
	if (!strCompDaysPunish.empty()) {COMP_DAYS_PUNISH = repast::strToDouble(strCompDaysPunish); }
	if (!strCompDaysRelax.empty()) {COMP_DAYS_RELAX = repast::strToDouble(strCompDaysRelax); }
	if (!strPerceptionBias.empty()) {PERCEPTION_BIAS = repast::strToDouble(strPerceptionBias);}
	if (!strDiscountMale.empty()) {DISCOUNT_MALE = repast::strToDouble(strDiscountMale);}
	if (!strDiscountFemale.empty()) {DISCOUNT_FEMALE = repast::strToDouble(strDiscountFemale);}
	if (!strDesireMultipilerAbstainer.empty()) {DESIRE_MULTIPLIER_ABSTAINER = repast::strToDouble(strDesireMultipilerAbstainer);}
	if (!strDesireMultipilerDrinker.empty()) {DESIRE_MULTIPLIER_DRINKER = repast::strToDouble(strDesireMultipilerDrinker);}

	//read parameters from file and store in a table for init agents function (used by Model as well)
	int rank = repast::RepastProcess::instance()->rank();
	std::string rankFileNameProperty = "file.rank" + std::to_string(rank);
	std::string rankFileName = props->getProperty(rankFileNameProperty);
	readRankFileForTheory(rankFileName);

	//regulators that affect the structural entities
	std::vector<Regulator*> regulatorListInj;
	mpRegPunishment = new RegulatorInjunctiveBingePunishment(&context);
	mpRegRelaxation = new RegulatorInjunctiveRelaxation(&context);
	regulatorListInj.push_back(mpRegPunishment);
	regulatorListInj.push_back(mpRegRelaxation);

	//power of each regulator (sum = 1)
	std::vector<double> powerListInj;
	powerListInj.push_back(0.5);
	powerListInj.push_back(0.5);

	//dummy list for Des Norm
	std::vector<Regulator*> regulatorListDes;
	std::vector<double> powerListDes;

	//create a structural entities
	std::vector<std::string> injNormPrev;
	repast::tokenize(props->getProperty("norms.injunctive.prevalence"), injNormPrev, ",");

	mIntervalDesNorm = repast::strToInt(props->getProperty("transformational.interval.descriptive.norm"));
	mIntervalPunish = repast::strToInt(props->getProperty("transformational.interval.punish"));
	mIntervalRelax = repast::strToInt(props->getProperty("transformational.interval.relax"));

	//InjunctiveNormEntity *pInjNorm = new InjunctiveNormEntity(regulatorListInj, powerListInj, mIntervalPunish, mIntervalRelax, injNormGate, injNormGamma, injNormLambda);
	InjunctiveNormEntity *pInjNorm = new InjunctiveNormEntity(regulatorListInj, powerListInj, mIntervalPunish, mIntervalRelax, injNormPrev);
	DescriptiveNormEntity *pDesNorm = new DescriptiveNormEntity(regulatorListDes, powerListDes, mIntervalDesNorm, &context);
	structuralEntityList.push_back(pInjNorm);
	structuralEntityList.push_back(pDesNorm);

	//Norm outputs (1 core)
	if (THEORY_SPECIFIC_OUTPUT) {
		std::string annualFileName = addUniqueSuffix("outputs/annual_norm_output.csv");
		/*annualNormOutput.open(annualFileName);
		annualNormOutput << "Year,";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				for (int k=0; j<NUM_SCHEMA; ++k)
					annualNormOutput << "DesPrev" << i << j << k << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				for (int k=0; j<NUM_SCHEMA; ++k)
					annualNormOutput << "InjPrev" << i << j << k << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "IntervalPunishment" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "IntervalRelaxation" << i << j << ",";
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				annualNormOutput << "PopulationCount" << i << j << ",";
		annualNormOutput << std::endl;*/
		mpAnnualNormOutputFile = fopen(annualFileName.c_str(),"w");
		fprintf(mpAnnualNormOutputFile,"%s,%s,","Year","Tick");
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				for (int k=0; k<NUM_SCHEMA; ++k)
					fprintf(mpAnnualNormOutputFile,"%s%d%d%d,","DesPrev", i, j, k);
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				for (int k=0; k<NUM_SCHEMA; ++k)
					fprintf(mpAnnualNormOutputFile,"%s%d%d%d,","InjPrev", i, j, k);
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				fprintf(mpAnnualNormOutputFile,"%s%d%d,","IntervalPunishment", i, j);
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				fprintf(mpAnnualNormOutputFile,"%s%d%d,","IntervalRelaxation", i, j);
		for (int i=0; i<NUM_SEX; ++i)
			for (int j=0; j<NUM_AGE_GROUPS; ++j)
				fprintf(mpAnnualNormOutputFile,"%s%d%d,","PopulationCount", i, j);
		fprintf(mpAnnualNormOutputFile,"\n");
	}
}

NormModel::~NormModel() {
	if (THEORY_SPECIFIC_OUTPUT && mpAnnualNormOutputFile!=NULL) fclose(mpAnnualNormOutputFile);
}

//read parameters from file and store in a table
void NormModel::readRankFileForTheory(std::string rankFileName) {
#ifdef DEBUG
		std::cout << "Reading the file: " << rankFileName <<std::endl;
#endif
	ifstream myfile(rankFileName);
	if (myfile.is_open()) {
		//read the csv file
		infoTable = readCSV(myfile);
		myfile.close();
	} else {
		std::cerr << "Unable to open file: " << rankFileName << std::endl;
	}

	//find index of theory-specific variables
	mIndexAutonomy = -1;
	std::vector<string> headerLine = infoTable.front();
	for (int i = 0; i < headerLine.size(); ++i) {
		if ( headerLine[i]=="norms.autonomy" )
			mIndexAutonomy = i;
	}
	if (mIndexAutonomy==-1)
		std::cerr << "Index (theory-specific) not found" << std::endl;
}

void NormModel::initMediatorAndTheoryWithRandomParameters(Agent *agent) {
	//create theory(ies) and a mediator for each agent
	std::vector<Theory*> theoryList;
	NormTheory* theory = new NormTheory(&context, (InjunctiveNormEntity *) structuralEntityList[0],
				(DescriptiveNormEntity *) structuralEntityList[1]);
	theoryList.push_back(theory);
	TheoryMediator *mediator = new MediatorForOneTheory(theoryList);

	//link agent with the mediator
	agent->setMediator(mediator);
	theory->initDesires();
}

void NormModel::initMediatorAndTheoryFromFile(Agent *agent, std::vector<std::string> info) {
	double autonomy = repast::strToDouble(info[mIndexAutonomy]);

	//create theory(ies) and a mediator for each agent
	std::vector<Theory*> theoryList;
	NormTheory* theory = new NormTheory(&context, (InjunctiveNormEntity *) structuralEntityList[0],
				(DescriptiveNormEntity *) structuralEntityList[1], autonomy);
	theoryList.push_back(theory);
	TheoryMediator *mediator = new MediatorForOneTheory(theoryList);

	//link agent with the mediator
	agent->setMediator(mediator);
	theory->initDesires();
}

void NormModel::initForTheory(repast::ScheduleRunner& runner) {
	//update avg drinking values before 1st situational mechanism
	((DescriptiveNormEntity *) structuralEntityList[1])->updateDescriptiveGroupDrinking();

	//If low impulsivity (<0.3) and high autonomy (>0.7), desire = habit.
	//Otherwise, randomly copy another agents within reference group.
	//(pick 100 random agents, if one is with the same reference group, copy desire arrays)
	repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
	repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
	while (iter != iterEnd) {
		//get norm theory to get autonomy
		NormTheory* pNormTheory1;
		if (!(*iter)->getTheory(&pNormTheory1)) {
			std:cerr << "initForTheory. Agent has no norm theory.\n";
			throw MPI::Exception(MPI::ERR_OTHER);
		}
		
		//assuming desire already init to habit, check "randomly copy" condition and copy
		if (!((*iter)->getTraitImpulsivity()<0.3 && pNormTheory1->getAutonomy()>0.7)) {
			//this agent reference group id
			int groupId1 = P_REFERENCE_GROUP->getId((*iter)->getSex(), (*iter)->findAgeGroup());

			//randomly pick 100 agents (or total count if there are less than 100)
			std::vector <Agent*> randomAgents;
			int numberOfAgents = context.size()>=100 ? 100 : context.size(); //use the smaller number between 100 and total agent count
			context.getRandomAgents(numberOfAgents,randomAgents);

			//copy if same reference group
			NormTheory* pNormTheory2;
			for (auto agent : randomAgents) {
				//other agent reference group id
				int groupId2 = P_REFERENCE_GROUP->getId(agent->getSex(), agent->findAgeGroup());

				//if the same reference group, copy
				if (groupId1 == groupId2) {
					//get norm theory to get desires
					if (!agent->getTheory(&pNormTheory2)) {
						std::cerr << "initForTheory. Agent has no norm theory.\n";
						throw MPI::Exception(MPI::ERR_OTHER);
					}

					//std::cout << (*iter)->getId().id() << " copies desires from " << agent->getId().id() << std::endl;

					//copy
					pNormTheory1->setDesires(pNormTheory2->getDesires());
					break;
				}
			}
		}
		iter++;
    }
}

std::string NormModel::addUniqueSuffix(std::string fileName){

	if (!boost::filesystem::exists(fileName)){ //check if the file doesn't exist.
		return fileName;				       //if not, return the filename as is.

	}else{
		//I will assume that if the file basename doesn't exists, none of the numbered versions do either.
		//If numbered versions exist while the base fileName file doens't exist, this will overwrite them.
		int i = 1;
		std::string noExtensionFileName = fileName;
		for (int count = 0; count < 4; ++count){
			noExtensionFileName.pop_back();
		}
		std::string testFileName = noExtensionFileName + "_" + to_string(i) + ".csv";
		while (boost::filesystem::exists(testFileName))
		{
			++i;
			testFileName = noExtensionFileName + "_" + to_string(i) + ".csv";
		}
		return testFileName;
	}
}

void NormModel::writeAnnualTheoryDataToFile() {
	//if (THEORY_SPECIFIC_OUTPUT && annualNormOutput.is_open()){
	if (THEORY_SPECIFIC_OUTPUT && mpAnnualNormOutputFile!=NULL){
		int size = P_REFERENCE_GROUP->size();

		fprintf(mpAnnualNormOutputFile,"%d,",simYear);
		fprintf(mpAnnualNormOutputFile,"%d,",(int)floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick()));
		DescriptiveNormEntity* pDesNormEntity = (DescriptiveNormEntity *) structuralEntityList[1];
		for (int i=0; i<NUM_SEX; ++i) {
			for (int j=0; j<NUM_AGE_GROUPS; ++j) {
				double* tempPrevArr = pDesNormEntity->getAvgPrevalence(P_REFERENCE_GROUP->getId(i,j));
				for (int k=0; k<NUM_SCHEMA; ++k) {
					fprintf(mpAnnualNormOutputFile,"%.5f,",tempPrevArr[k]);
				}
			}
		}
		
		InjunctiveNormEntity* pInjNormEntity = (InjunctiveNormEntity *) structuralEntityList[0];
		for (int i=0; i<NUM_SEX; ++i) {
			for (int j=0; j<NUM_AGE_GROUPS; ++j) {
				for (int k=0; k<NUM_SCHEMA; ++k) {
					fprintf(mpAnnualNormOutputFile,"%.5f,",pInjNormEntity->getInjNormPrev(P_REFERENCE_GROUP->getId(i,j), k));
				}
			}
		}

		for (int i=0; i<size; ++i)
			fprintf(mpAnnualNormOutputFile,"%.5f,",mpRegPunishment->mTransformationalTriggerCount[i]/(365.0/mIntervalPunish));
		for (int i=0; i<size; ++i)
			fprintf(mpAnnualNormOutputFile,"%.5f,",mpRegRelaxation->mTransformationalTriggerCount[i]/(365.0/mIntervalRelax));

		int count[size] = {0};
		repast::SharedContext<Agent>::const_local_iterator iter = context.localBegin();
		repast::SharedContext<Agent>::const_local_iterator iterEnd = context.localEnd();
		while (iter != iterEnd) {
			count[P_REFERENCE_GROUP->getId((*iter)->getSex(), (*iter)->findAgeGroup())] += 1;
			iter++;
		}
		
		for (int i=0; i<size; ++i) {
			fprintf(mpAnnualNormOutputFile,"%d,",count[i]);
		}
		fprintf(mpAnnualNormOutputFile,"\n");

		pInjNormEntity->mTransformationalTriggerCount = 0;
		pDesNormEntity->mTransformationalTriggerCount = 0;
		mpRegPunishment->resetCount();
		mpRegRelaxation->resetCount();
	} else {
		std::cerr << "Error: Can't write to annual_norm_output.csv, file not open." << std::endl;
	}
}
