#include "NormReferenceGroup.h"
#include "globals.h"

#include <iostream>

NormReferenceGroup::NormReferenceGroup() {
	// for loop, init all ref groups
	mNumberOfReferenceGroups = NUM_SEX*NUM_AGE_GROUPS;
	mReferenceGroups = new std::tuple<int, int>[mNumberOfReferenceGroups];
	int index = 0;
	for (int i=0; i<NUM_SEX; ++i) {
		for (int j=0; j<NUM_AGE_GROUPS; ++j) {
			mReferenceGroups[index] = std::make_tuple(i, j);
			index++;
		}
	}
}

NormReferenceGroup::~NormReferenceGroup() {}

int NormReferenceGroup::size() {return mNumberOfReferenceGroups;}

int NormReferenceGroup::getId(int sex, int ageGroup) {
	std::tuple<int, int> refGroup = std::make_tuple(sex, ageGroup);
	for (int i=0; i<mNumberOfReferenceGroups; i++) {
		if (mReferenceGroups[i] == refGroup)
			return i;
	}

	std::cerr << "Error: Do not find any reference group for [" << sex << " " << ageGroup << "]." << std::endl;
	return -1; //do not find any matching reference group
}

int NormReferenceGroup::compare(int id1, int id2) {
	// declare local integer as counter
	int numberShared = 0;

	// compare and count number of shared attributes
	std::tuple<int, int> refGroup1 = mReferenceGroups[id1];
	std::tuple<int, int> refGroup2 = mReferenceGroups[id2];

	// sum the difference (note: cannot loop the index for tuple)
	numberShared += int (std::get<0>(refGroup1) == std::get<0>(refGroup2));
	numberShared += int (std::get<1>(refGroup1) == std::get<1>(refGroup2));

	// return number of shared attributes
	return numberShared;
}