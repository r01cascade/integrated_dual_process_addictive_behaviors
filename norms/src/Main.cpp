#include <boost/mpi.hpp>
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/initialize_random.h"

#include "Model.h"
#include "NormModel.h"

int main(int argc, char** argv){
	std::string configFile = argv[1];
	std::string propsFile = argv[2];

	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator world;

	//Read random seed from model.props. If there is no random seed in props file, proceed as normal
	repast::Properties* props = new repast::Properties(propsFile, argc, argv, &world);
	initializeRandom(*props, &world);

	//Set up model
	repast::RepastProcess::init(configFile);

	MPI::COMM_WORLD.Set_errhandler(MPI::ERRORS_THROW_EXCEPTIONS);

	try {
		/***** FOR TESTING: THROW RANDOM ERRORS *****/
		/*
		if (repast::Random::instance()->nextDouble() < 0.3)
			throw MPI::Exception(MPI::ERR_DIMS); //code 11
		else if (repast::Random::instance()->nextDouble() < 0.6)
			throw MPI::Exception(MPI::ERR_INTERN); //code 16
		*/
		/***** FOR TESTING: THROW RANDOM ERRORS *****/

		Model* model = new NormModel(propsFile, argc, argv, &world);
		repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();
		
		model->initialize(runner); //manange schedule (core)

		runner.run();
		delete model;
		repast::RepastProcess::instance()->done();
		return 0;
	} catch ( MPI::Exception e ) {
		//std::cerr << "Error " << e.Get_error_code() << ": " << e.Get_error_string() << std::endl;
		repast::RepastProcess::instance()->done();
		MPI::COMM_WORLD.Abort(e.Get_error_code());
		return e.Get_error_code();
	}
}
