#ifndef INCLUDE_THEORYMEDIATOR_H_
#define INCLUDE_THEORYMEDIATOR_H_

class Agent;
#include "Theory.h"
#include "DrinkingPlan.h"
#include "globals.h"
#include <vector>

class TheoryMediator {

protected:
	std::vector<Theory*> mTheoryList;
	Agent *mpAgent;

	/* NEW ACTION MECH */
	//mediated Attitude, Norm, PBC
	double mProbOppIn;
	double mProbOppOut;

	//Dynamically allocating arrays
	double *mAttitude{ new double[NUM_SCHEMA]{} };
	double *mNorm{ new double[NUM_SCHEMA]{} };
	double *mPbc{ new double[NUM_SCHEMA]{} };
	double *mIntentionProb {new double[NUM_SCHEMA]{} };

public:
	TheoryMediator(std::vector<Theory*> theoryList);
	virtual ~TheoryMediator();
	void linkAgent(Agent *agent); //link agent to this mediator and all theories in the theory list

	template <typename derivedTheory>
	bool getTheory(derivedTheory** ppTheory) { //write a theory (from mTheoryList) with the matching type to derivedTheory, return true if success.
		for (std::vector<Theory*>::iterator it=mTheoryList.begin(); it!=mTheoryList.end(); ++it) {
				if (dynamic_cast<derivedTheory*>(*it)!=0) {
					*ppTheory = dynamic_cast<derivedTheory*>(*it);
					return true;
				}
			}
			return false;
	};

	virtual void mediateSituation() = 0;
	virtual void mediateAction() = 0;
	virtual void mediateGatewayDisposition() = 0;
	virtual void mediateNextDrinksDisposition() = 0;
	virtual void mediateNonDrinkingActions() = 0;
	
	/* NEW ACTION MECH */
	double getProbOppIn() {return mProbOppIn;}
	double getProbOppOut() {return mProbOppOut;}
	double getIntentionProbability(int i) {return mIntentionProb[i];}
	void setIntentionProbability(int i, double value) { mIntentionProb[i] = value;}
	virtual void mediateThoughtPathway() = 0; //must be overrided to mediate Attitude, Norm, PBC
	void updateIntentionProbabilities();
	DrinkingPlan getProportionalSelectionIntentionPlan();
	DrinkingPlan getMaxIntentionPlan();
};

#endif /* INCLUDE_THEORYMEDIATOR_H_ */
