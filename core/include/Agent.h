/* Agent.h */

#ifndef AGENT
#define AGENT

#include "globals.h"
#include "TheoryMediator.h"
#include "Theory.h"

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "SocialNetworkEntity.h"
#include "DrinkingPlan.h"

#include <cmath>

class SocialNetworkEntity;

enum class Opportunity {
	NONE=0,
	IN=1,
	OUT=2,
	TEST_ERROR=3
};

/* Agents */
class Agent {
	
private:
	repast::AgentId  mId;
	int              mAge;
	bool             mSex;
	std::string      mRace;
	bool       	     mIsDrinkingToday;
	int 			 mNumberDrinksToday;
	bool 			 mIs12MonthDrinker; //true if this agent has a drink in the last year. Reset every year.
	int              mDrinkFrequencyLevel;
	double           mBaselineMonthlyDrink;
	std::vector<int> mPastYearDrinks;
	int              mTotalDrinksPerAnnum;//total drinks per annum added by Hao
	int              mMaritalStatus;//added by Hao for the purpose of role prevalence
	int              mParenthoodStatus;//added by Hao for the purpose of role prevalence
	int              mEmploymentStatus;//added by Hao for the purpose of role prevalence
	double           mMeanDrinksToday;//added by Hao
	double           mSDDrinksToday;//added by Hao
	int              mDayOfRoleTransition;//added by Hao for role transition
	bool             mRoleChangingTransient;//iteration2 - marking transient of role change - true on the day of transition if transited
	int              mRoleChangeStatus;//iteration2 - marking the role changing status -1 - less roles; 0 - unchanged; 1 - more roles
	int              mIncome;
	int              mAnnualFrequency;
	double           mGramsPerDay;
//personal mortality and migration rate.  Updated once a year
	int             mYear;
	std::string     mMortalityHashKey;
	std::string     mMigrationHashKey;
	double          mMortality;
	double          mMigrationOutRate;

	TheoryMediator      *mpMediator;
	std::vector<double> mDispositions;

	void initDispositions(); //init dispositions array:size MAX_DRINK, all values 0
	void doDisposition(); //calculate disposition array from theories
	void doDrinkingEngine(); //calculate number of drinks
	static int myrandom (int i); //generator for random_shuffle

	int mPastYearN; //number of drinking days
	double mPastYearMeanDrink; //accumulates the mean of drinks within drinking days
	double mPastYearSquaredDistanceDrink; //aggregates the squared distance from the mean
	void updateForwardMeanVariance(int addedValue);
	void updateBackwardMeanVariance(int removedValue);

	template<typename T>
	void shuffleList(std::vector<T>& elementList); //shuffle a list using Fisher-Yates shuffle

//update hashKey only once a year when we age agents.
	std::string makeMigrationOutHashKey(std::string year, std::string sex, std::string race, std::string age);//create hashkey for searching
	std::string makeDeathRateHashKey(std::string year, std::string sex, std::string age);//create hashkey for searching

	//Network
	SocialNetworkEntity *mpSocialNetwork;
	std::vector<Agent*> mPotentialBuddies;
	std::vector<Agent*> mDrinkingBuddies;
	int selectIndexFromProbabilityVector(std::vector<double> probVector);

	/* NEW ACTION MECHANIMS */
	std::vector<DrinkingSchema> mPastYearSchemas;
	void initPastYearSchemas();
	int mHabitUpdateInterval;
	double mTraitImpulsivity;
	Opportunity mTodayOpportunity;
	DrinkingSchema mTodaySchema;
	void doDrinking(DrinkingSchema schema); //replace doDrinkingEngine;
	int getDrinksFromSchema(DrinkingSchema schema);
	DrinkingSchema getSchemaFromDrinks(int drinks);
	void updateHabitProbabilities(int numberOfDays);
	double *mHabitStock{new double [NUM_SCHEMA]{} };
	double *mHabitProbability{new double [NUM_SCHEMA]{} };
	void refillStock();
	FILE * mpActionMechFile = NULL;
	int sampleFromGammaDistPdf(double shape, double scale, int min, int max);
	void initPastYearSchemasDrinks(std::vector<double> schemaProfile);
	std::vector<DrinkingSchema> mHabitBag;
	DrinkingSchema drawSchemaFromHabitBag();
	void updateHabitBag(int numberOfDays);
	std::vector<double> initSchemaProfiles();

public:
	Agent(repast::AgentId id);  // Constructor
	Agent(repast::AgentId id, bool sex, int age, std::string race, int maritalStatus, int parenthoodStatus, int employmentStatus, 
		int income, int drinking, int drinkFrequencyLevel, int monthlyDrinks, double monthlyDrinksSDPct, 
		int year, double traitImpulsivity, int annualFrequency, double gramsPerDay, int habiUpdateInterval);
	virtual ~Agent(); // Destructor
	
	/* Required Getters */
	virtual repast::AgentId& getId(){                   return mId;    }
	virtual const repast::AgentId& getId() const{       return mId;    }
	
	/* Getters specific to this kind of Agent */
	int    getAge(){                                    return mAge;    }
	bool   getSex(){                                    return mSex;    }
	std::string getRace(){                              return mRace;   }
	bool   isDrinkingToday(){                        return mIsDrinkingToday;}
	int    getNumberDrinksToday(){						return mNumberDrinksToday;}
	bool   is12MonthDrinker() {						return mIs12MonthDrinker;}
	bool   isHaveKDrinksOverNDays(int numberOfDays, int kNumberDrinks);
	double getAvgDrinksNDays(int numberOfDays);
	double getAvgDrinksNDays(int numberOfDays, bool perOccasion);
	int    getNumDaysHavingKDrinksOverNDays(int numberOfDays, int kNumberDrinks);
	int    getTotalDrinksPerAnnum(){                    return mTotalDrinksPerAnnum;}//added by Hao
	int    getMaritalStatus(){                          return mMaritalStatus;}//added by Hao for the purpose of role prevalence
	int    getParenthoodStatus(){                       return mParenthoodStatus;}//added by Hao for the purpose of role prevalence
	int    getEmploymentStatus(){                       return mEmploymentStatus;}//added by Hao for the purpose of role prevalence
	int    getIncome(){                                 return mIncome;}
	double getMeanDrinksToday(){                        return mMeanDrinksToday;}//added by Hao
	double getSDDrinksToday(){                          return mSDDrinksToday;}//added by Hao
	int    getDayOfRoleTransition(){                    return mDayOfRoleTransition;}//added by Hao
	int    getRoleChangeStatus(){                       return mRoleChangeStatus;}//iteration2 - added by Hao
	bool   getRoleChangedTransient(){                   return mRoleChangingTransient;}//iteration2 - added by Hao
	
	double getPastYearMeanDrink() {                     return mPastYearMeanDrink; }
	double getPastYearVarianceDrink() {                 return (mPastYearN<=1 ? 0 : mPastYearSquaredDistanceDrink / (mPastYearN)); }
	double getPastYearSdDrink() {                       return sqrt(getPastYearVarianceDrink()); }
	int    getDrinkFrequencyLevel(){                    return mDrinkFrequencyLevel;}
	std::vector<int> getPastYearDrinks(){               return mPastYearDrinks;     }
	std::vector<double> getDisposition(){               return mDispositions;       }
	int getPastYearN(){                                 return mPastYearN;}
	int getAnnualFrequency(){return mAnnualFrequency;}
	
	//give the address of a theory within this agent that matched the type of provided theory ppTheory,
	//return true if success.
	template <typename derivedTheory>
	bool getTheory(derivedTheory** ppTheory) {
		mpMediator->getTheory(ppTheory);
	}
	
	double getMortalityRate(){                          return mMortality;}
	double getMigrationOutRate(){                       return mMigrationOutRate;}
	double getTraitImpulsivity(){ return mTraitImpulsivity;}
	
	/* Setter */
	void set(int currentRank, int age, bool sex, bool currentDrinking, int currentQuantity);
	void set(int currentRank, int age, bool sex, bool currentDrinking, int currentQuantity, int currentFrequency,
	         std::vector<int> pastYearDrinks);
	void setMediator(TheoryMediator *mediator);
	void setDispositionByIndex(int index, double value);
	void setMaritalStatus(int maritalStatus){mMaritalStatus = maritalStatus;}//added by Hao for the purpose of role prevalence
	void setParenthoodStatus(int parenthoodStatus){mParenthoodStatus = parenthoodStatus;}//added by Hao for the purpose of role prevalence
	void setEmploymentStatus(int employmentStatus){mEmploymentStatus = employmentStatus;}//added by Hao for the purpose of role prevalence
	void setIncome(int income){mIncome = income;}
	void setRoleChangeStatus(int roleChangeStatus){mRoleChangeStatus = roleChangeStatus;}//iteration2 - added by Hao
	bool setRoleChangedTransient(bool roleChangedTransient){mRoleChangingTransient = roleChangedTransient;}//iteration2 - added by Hao

	void setDrinkFrequencyLevel(int iInput){mDrinkFrequencyLevel=iInput;}

	/* Situational mechanisms */
	void doSituation();

	/* Action mechanisms */
	void doAction();

	// Make agent older (in years)
	void ageAgent();

	void reset12MonthDrinker();
	void resetTotalDrinksPerAnnum();

	int findAgeGroup(); //TODO: decide to put age here or remove it

	void initPastYearDrinks(int monthlyDrinks, double monthlyDrinksSDPct);
	void updatePastYearDrinks();


	/*initialize potential friends override*/
	void initPotentialBuddies();
	void removeAgentFromPotentialBuddies(repast::AgentId agentId);

	void selectDrinkingBuddies(bool age, bool sex, bool race, bool isDrinkingToday, bool numberDrinksToday, bool is12MonthDrinker,  bool drinkFrequencyLevel, 
	                        bool totalDrinksPerAnnum, bool maritalStatus, bool parenthoodStatus, bool employmentStatus, 
	                        bool meanDrinksToday, bool sDDrinksToday, bool income, bool pastYearN, bool pastYearMeanDrink, 
	                        bool outdegree, bool reciprocity, bool preferentialAttachment, bool transitiveTriples);
	std::vector<double> calculateSumEffectVector(std::vector<int> friendVector, std::vector<double> internalVector);
	std::vector<double> generateChoiceProbabilityVector(
	                                            std::vector<double> totalAgeSimilarityVector, 
	                                            std::vector<double> totalSexSimilarityVector, 
	                                            std::vector<double> totalRaceSimilarityVector,
	                                            std::vector<double> totalIsDrinkingTodaySimilarityVector, 
	                                            std::vector<double> totalNumberDrinksTodaySimilarityVector,
	                                            std::vector<double> totalIs12MonthDrinkersSimilarityVector, 
	                                            std::vector<double> totalDrinkFrequencyLevelSimilarityVector,
	                                            std::vector<double> totalTotalDrinksPerAnnumSimilarityVector, 
	                                            std::vector<double> totalMaritalStatusSimilarityVector, 
	                                            std::vector<double> totalParenthoodStatusSimilarityVector, 
	                                            std::vector<double> totalEmploymentStatusSimilarityVector, 
	                                            std::vector<double> totalMeanDrinksTodaySimilarityVector, 
	                                            std::vector<double> totalSDDrinksTodaySimilarityVector, 
	                                            std::vector<double> totalIncomeSimilarityVector,
	                                            std::vector<double> totalPastYearNSimilarityVector, 
	                                            std::vector<double> totalPastYearMeanDrinkSimilarityVector, 
	                                            std::vector<double> totalReciprocityVector, 
	                                            std::vector<double> totalOutdegreeVector, 
	                                            std::vector<double> totalPreferentialAttachmentVector, 
	                                            std::vector<double> totalTransitiveTriplesVector);  
	void changeFriendTie (int choiceIndex, std::vector<Agent*> peopleConsidered, std::vector<int> friendVector);

	void setSocialNetwork(SocialNetworkEntity *pSocialNetwork) {mpSocialNetwork = pSocialNetwork;}
	std::vector<Agent*> getDrinkingBuddies() {return mDrinkingBuddies;}

	/* NEW ACTION MECHANIMS */
	void generateOpportunity(double probOppIn, double probOppOut);
	DrinkingPlan getMaxHabitualDrinkingPlan(int days);
	DrinkingPlan getProportionalSelectionHabitualDrinkingPlan(int days);
	DrinkingPlan getProportionalSelectionHabitualDrinkingPlan();
	int* getSchemaCountOverNDays(int numberOfDays);

	bool flagDryJanuary = false;
	int getHabitUpdateInterval() {return mHabitUpdateInterval;}
};

/* Serializable Agent Package */
struct AgentPackage{
	
public:
	int     id;
	int     rank;
	int     type;
	int     currentRank;
	int     age;
	bool    sex;
	bool    isDrinkingToday; // these are the properties that are observable by other agents which is all that is required by the norms model
	int 	numberDrinksToday;
	int     drinkFrequencyLevel; //this is observable by other agents and needed by the CONTAGION model
	std::vector<int> pastYearDrinks;

	/* Constructors */
	AgentPackage(); // For serialization
	AgentPackage(int _id, int _rank, int _type, int _currentRank, int _age, bool _sex, bool _isDrinkingToday, int _numberDrinksToday);
	AgentPackage(int _id, int _rank, int _type, int _currentRank, int _age, bool _sex, bool _isDrinkingToday, 
                 int _numberDrinksToday, int _drinkFrequencyLevel, std::vector<int> _pastYearDrinks);
	

	/* For archive packaging */
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        ar & age;
        ar & sex;
        ar & isDrinkingToday;
        ar & numberDrinksToday;
        ar & drinkFrequencyLevel;
        ar & pastYearDrinks;
	}
};


#endif
