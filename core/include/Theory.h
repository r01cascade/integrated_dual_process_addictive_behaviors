#ifndef INCLUDE_THEORY_H_
#define INCLUDE_THEORY_H_

class Agent; //forward declare, but DO NOT define

#include <utility>
#include "DrinkingPlan.h"

class Theory {

protected:
	Agent *mpAgent;
	double mProbOppIn;
	double mProbOppOut;

	double safeOdds(double numerator, double denominator);
	double safeLog(double value);

public:
	virtual ~Theory() {};

	void setAgent(Agent *agent);
	virtual void doSituation() = 0;
	virtual void doGatewayDisposition() = 0;
	virtual void doNextDrinksDisposition() = 0;
	virtual void doNonDrinkingActions() = 0;

	//correct mean and sd for ERFC function
	std::pair<double, double> generateCorrectedMeanSD (double desiredMean, double desiredSd);
	std::pair<double, double> doLookup(double mean, double sd);
	std::pair<double, double> doFunctionLookup(double desiredMean, double desiredSd);

	/* NEW ACTION MECH */
	virtual void updateDrinkingOpportunity() = 0;
	double getProbOppIn() {return mProbOppIn;}
	double getProbOppOut() {return mProbOppOut;}

	virtual double getAttitude(DrinkingSchema schema) = 0;
	virtual double getNorm(DrinkingSchema schema) = 0;
	virtual double getPerceivedBehaviourControl(DrinkingSchema schema) = 0;
};

#endif /* INCLUDE_THEORY_H_ */
