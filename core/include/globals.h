#ifndef INCLUDE_GLOBALS_H_
#define INCLUDE_GLOBALS_H_
#include <map>
#include <utility>
#include <vector>
#include <unordered_map>

extern bool FAIL_FAST;
extern bool SOCIAL_NETWORK;

extern const bool MALE;
extern const bool FEMALE;
extern const int NUM_SEX;
extern const int NUM_AGE_GROUPS;
extern const int MIN_AGE;
extern const int MAX_AGE;
extern const int MAX_DRINKS;
extern const int AGE_GROUPS[9];
extern const int AGE_RANGE;

extern const int MAX_DRINK_LEVEL;
extern const int MIN_DRINK_LEVEL;
extern const int DRINK_RANGE;

extern const int MAX_INCOME;

extern std::map<int, std::pair<double, double> > MEAN_SD_LOOKUP_TABLE;
extern std::unordered_map<std::string, double> deathRateTable;//storing death rate by characteristics
extern std::unordered_map<std::string, double> migrationOutTable;//storing migration-out rate by characteristics

/* NEW ACTION MECH */
extern const int NUM_SCHEMA;
extern double BETA_ATTITUDE;
extern double BETA_NORM;
extern double BETA_PBC;
extern int HABIT_STOCK_DAYS;
//extern int HABIT_UPDATE_INTERVAL;

extern double AGE_SIMILARITY_BETA;
extern double SEX_SIMILARITY_BETA;
extern double RACE_SIMILARITY_BETA;
extern double IS_DRINKING_TODAY_SIMILARITY_BETA;
extern double NUMBER_DRINKS_TODAY_SIMILARITY_BETA;
extern double IS_12_MONTH_DRINKERS_SIMILARITY_BETA;
extern double SELECTION_FREQUENCY_BETA;
extern double TOTAL_DRINKS_PER_ANNUM_SIMILARITY_BETA;
extern double MARITAL_STATUS_SIMILARITY_BETA;
extern double PARENTHOOD_STATUS_SIMILARITY_BETA;
extern double EMPLOYMENT_STATUS_SIMILARITY_BETA;
extern double MEAN_DRINKS_TODAY_SIMILARITY_BETA;
extern double SD_DRINKS_TODAY_SIMILARITY_BETA;
extern double INCOME_SIMILARITY_BETA;
extern double PAST_YEAR_N_SIMILARITY_BETA;
extern double SELECTION_QUANTITY_BETA;
extern double OUTDEGREE_BETA;
extern double RECIPROCITY_BETA;
extern double PREFERENTIAL_ATTACHMENT_BETA;
extern double TRANSITIVE_TRIPLES_BETA;

extern bool AGE_SIMILARITY_FLAG;
extern bool SEX_SIMILARITY_FLAG;
extern bool RACE_SIMILARITY_FLAG;
extern bool IS_DRINKING_TODAY_SIMILARITY_FLAG;
extern bool NUMBER_DRINKS_TODAY_SIMILARITY_FLAG;
extern bool IS_12_MONTH_DRINKERS_SIMILARITY_FLAG;
extern bool SELECTION_FREQUENCY_FLAG;
extern bool TOTAL_DRINKS_PER_ANNUM_SIMILARITY_FLAG;
extern bool MARITAL_STATUS_SIMILARITY_FLAG;
extern bool PARENTHOOD_STATUS_SIMILARITY_FLAG;
extern bool EMPLOYMENT_STATUS_SIMILARITY_FLAG;
extern bool MEAN_DRINKS_TODAY_SIMILARITY_FLAG;
extern bool SD_DRINKS_TODAY_SIMILARITY_FLAG;
extern bool INCOME_SIMILARITY_FLAG;
extern bool PAST_YEAR_N_SIMILARITY_FLAG;
extern bool SELECTION_QUANTITY_FLAG;
extern bool OUTDEGREE_FLAG;
extern bool RECIPROCITY_FLAG;
extern bool PREFERENTIAL_ATTACHMENT_FLAG;
extern bool TRANSITIVE_TRIPLES_FLAG;

extern bool AGENT_LEVEL_OUTPUT;
//TODO: Move theory-specific global variables out of core

/**** RATIONAL CHOICE ****/
extern double UNIT_PRICE[40];
extern double TEMP_LEGAL_RISK;
extern double DECAY_BASE_RATE;
extern int DAYS_TO_DEVELOP_WITHDRAWAL;
extern int WITHDRAWAL_WASHOUT_DIVISOR; 
extern double HOURS_FREE_TIME_MEAN; 
extern double HOURS_FREE_TIME_SD;
extern int TOTAL_YEARS_CONSUMPTION_STOCK; 
extern int HEAVY_DRINKS_PER_DAY;
/**** RATIONAL CHOICE ****/


/**** CONTAGION ****/
extern const int POTENTIAL_BUDDIES_SIZE;
extern  double FREQUENCY_INFLUENCE_BETA;
extern  double QUANTITY_INFLUENCE_BETA;
extern  double INFLUENCE_LAMBDA;
extern  double SELECTION_LAMBDA;

/**** CONTAGION ****/

/**** ROLES ****/
extern const int THRESHOLD_QUANTITY_HEAVEY_MALE_DRINKER;
extern const int THRESHOLD_QUANTITY_HEAVEY_FEMALE_DRINKER;
extern const int IDFOROUTPUT;
extern bool ROLES_SOCIALISATION_ON;
/**** ROLES ****/

#endif /* INCLUDE_GLOBALS_H_ */
