#ifndef INCLUDE_REGULATOR_H_
#define INCLUDE_REGULATOR_H_

class Regulator {

public:
	virtual ~Regulator() {};

	virtual void updateAdjustmentLevel() = 0; 
	

// for unit test purposes only
//    Regulator(){ }
};

#endif /* INCLUDE_REGULATOR_H_ */

