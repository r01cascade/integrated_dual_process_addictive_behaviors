// This file was created automatically by coreMakeTests.py

#include <string>
#include <iostream>
#include <chrono>
#include <random>
#include <ctime>
#define IN_RANGE
#include "Agent.h"
#include "globals.h"
#include <numeric>

void test_Agent_1(int nTimes){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_1

#ifdef TEST_AGENT_1


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.


       std::cout << "Before using constructor\n";
       Agent thisClass = Agent(
		/* repast::AgentId */ id);
       std::cout << "After using constructor\n";

    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_1
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_1 elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_2(int nTimes){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_2

#ifdef TEST_AGENT_2


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.


       std::cout << "Before using constructor\n";
       Agent thisClass = Agent(
		/* repast::AgentId */ id,
		/* bool */ sex,
		/* int */ age,
		/* std::string */ race,
		/* int */ maritalStatus,
		/* int */ parenthoodstatus,
		/* int */ employmentStatus,
		/* int */ income,
		/* int */ drinking,
		/* int */ drinkFrequencyLevel,
		/* int */ monthlyDrinks,
		/* double */ monthlyDrinksSDPct,
		/* int */ year);
       std::cout << "After using constructor\n";

    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_2
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_2 elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_setMediator(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_SETMEDIATOR
//#define TEST_AGENT_SETMEDIATOR_IN_RANGE

#ifdef TEST_AGENT_SETMEDIATOR
    TheoryMediator my_mediator;
    void result_0;
    void expected_0;
    void expected_min_0;
    void expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_mediator = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::setMediator(TheoryMediator *mediator) {
// Agent.cpp:     mpMediator = mediator;
// Agent.cpp:     mpMediator->linkAgent(this);
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.setMediator(my_mediator);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_SETMEDIATOR_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_SETMEDIATOR
#ifdef TEST_AGENT_SETMEDIATOR_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_setMediator elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_initDispositions(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_INITDISPOSITIONS
//#define TEST_AGENT_INITDISPOSITIONS_IN_RANGE

#ifdef TEST_AGENT_INITDISPOSITIONS


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::initDispositions(){
// Agent.cpp: 	std::vector<double> vect(MAX_DRINKS, 0.0);
// Agent.cpp: 	mDispositions = vect;
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       thisClass.initDispositions();
       std::cout << "After using Agent\n";

#ifdef TEST_AGENT_INITDISPOSITIONS_IN_RANGE


#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_INITDISPOSITIONS
#ifdef TEST_AGENT_INITDISPOSITIONS_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_initDispositions elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_setDispositionByIndex(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_SETDISPOSITIONBYINDEX
//#define TEST_AGENT_SETDISPOSITIONBYINDEX_IN_RANGE

#ifdef TEST_AGENT_SETDISPOSITIONBYINDEX
    int my_index;
    double my_value;
    void result_0;
    void expected_0;
    void expected_min_0;
    void expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_index = XYZ;
        my_value = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::setDispositionByIndex(int index, double value) {
// Agent.cpp: 	mDispositions[index] = value;
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.setDispositionByIndex(my_index, my_value);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_SETDISPOSITIONBYINDEX_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_SETDISPOSITIONBYINDEX
#ifdef TEST_AGENT_SETDISPOSITIONBYINDEX_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_setDispositionByIndex elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_doSituation(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_DOSITUATION
//#define TEST_AGENT_DOSITUATION_IN_RANGE

#ifdef TEST_AGENT_DOSITUATION


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::doSituation() {
// Agent.cpp: 	mpMediator->mediateSituation();
// Agent.cpp: 	generateOpportunity(mpMediator->getProbOppIn(), mpMediator->getProbOppOut());
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       thisClass.doSituation();
       std::cout << "After using Agent\n";

#ifdef TEST_AGENT_DOSITUATION_IN_RANGE


#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_DOSITUATION
#ifdef TEST_AGENT_DOSITUATION_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_doSituation elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_doDisposition(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_DODISPOSITION
//#define TEST_AGENT_DODISPOSITION_IN_RANGE

#ifdef TEST_AGENT_DODISPOSITION


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::doDisposition() {
// Agent.cpp: 	mpMediator->mediateGatewayDisposition();
// Agent.cpp: 	mpMediator->mediateNextDrinksDisposition();
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       thisClass.doDisposition();
       std::cout << "After using Agent\n";

#ifdef TEST_AGENT_DODISPOSITION_IN_RANGE


#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_DODISPOSITION
#ifdef TEST_AGENT_DODISPOSITION_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_doDisposition elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_doDrinkingEngine(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_DODRINKINGENGINE
//#define TEST_AGENT_DODRINKINGENGINE_IN_RANGE

#ifdef TEST_AGENT_DODRINKINGENGINE


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::doDrinkingEngine() {
// Agent.cpp:     bool stillDrinking = true;
// Agent.cpp: 	int numberOfDrinks = 0;
// Agent.cpp: 	do {
// Agent.cpp: 		if(repast::Random::instance()->nextDouble() > mDispositions[numberOfDrinks]){
// Agent.cpp: 			stillDrinking = false;
// Agent.cpp: 		} else{
// Agent.cpp: 			numberOfDrinks++;
// Agent.cpp: 		}
// Agent.cpp: 	} while (stillDrinking == true && numberOfDrinks < MAX_DRINKS);
// Agent.cpp: //	std::cout << "id: "<< getId().id() << " number drinks: " << numberOfDrinks << std::endl;
// Agent.cpp: 	mIsDrinkingToday = (numberOfDrinks > 0);
// Agent.cpp: 	mNumberDrinksToday = numberOfDrinks;
// Agent.cpp: 
// Agent.cpp: 	mTotalDrinksPerAnnum += numberOfDrinks;
// Agent.cpp: 	#ifdef DEBUG
// Agent.cpp: 		if (mId.id() == IDFOROUTPUT){
// Agent.cpp: 			std::cout<<"mTotalDrinksPerAnnum = "<<mTotalDrinksPerAnnum<<std::endl;
// Agent.cpp: 			std::cout<<"mNumberDrinksToday = "<<mNumberDrinksToday<<std::endl;
// Agent.cpp: 		}
// Agent.cpp: 	#endif
// Agent.cpp: 
// Agent.cpp: 	if (mIsDrinkingToday) {mIs12MonthDrinker = true;} //turn on the 12-month drinker flag if this agent drinks
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       thisClass.doDrinkingEngine();
       std::cout << "After using Agent\n";

#ifdef TEST_AGENT_DODRINKINGENGINE_IN_RANGE


#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_DODRINKINGENGINE
#ifdef TEST_AGENT_DODRINKINGENGINE_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_doDrinkingEngine elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_doAction(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_DOACTION
//#define TEST_AGENT_DOACTION_IN_RANGE

#ifdef TEST_AGENT_DOACTION


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::doAction() {
// Agent.cpp: 	// OLD ACTION
// Agent.cpp: 	//doDisposition();
// Agent.cpp: 	//doDrinkingEngine();
// Agent.cpp: 
// Agent.cpp: 	if (mTodayOpportunity != Opportunity::NONE) {
// Agent.cpp: 		DrinkingPlan drinkingPlan;
// Agent.cpp: 		if (!DUAL_PROCESS) {
// Agent.cpp: 			//dual_process: choose either habit or thought
// Agent.cpp: 			if (repast::Random::instance()->nextDouble() > mThresholdThoughtHabit)
// Agent.cpp: 				drinkingPlan = getHabitualDrinkingPlan(30);
// Agent.cpp: 			else drinkingPlan = mpMediator->getIntention();
// Agent.cpp: 		} else {
// Agent.cpp: 			//weight between habit vs thought
// Agent.cpp: 			DrinkingPlan habitualPlan = getHabitualDrinkingPlan(30);
// Agent.cpp: 			DrinkingPlan intentionPlan = mpMediator->getIntention();
// Agent.cpp: 			if (intentionPlan.probability > habitualPlan.probability)
// Agent.cpp: 				drinkingPlan = intentionPlan;
// Agent.cpp: 			else drinkingPlan = habitualPlan;
// Agent.cpp: 		}
// Agent.cpp: 		doDrinking(drinkingPlan.schema);
// Agent.cpp: 	} else {
// Agent.cpp: 		doDrinking(DrinkingSchema::NONE);
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	updatePastYearDrinks();
// Agent.cpp: 	mpMediator->mediateNonDrinkingActions();
// Agent.cpp: 
// Agent.cpp: //	#ifdef DEBUG
// Agent.cpp: //	std::cout << "Current quantity: " << mCurrentQuantity << std::endl;
// Agent.cpp: //	#endif
// Agent.cpp: /*	//added test code for doDrinkingEngine
// Agent.cpp: 	int myDrinks = doDrinkingEngine(drinkingValue);
// Agent.cpp: 	#ifdef DEBUG
// Agent.cpp: 	std::cout << "my deterministic drinking number: id " << mId << ", number drinks: " << myDrinks;
// Agent.cpp: 	#endif
// Agent.cpp: 	myDrinks = doDrinkingEngine(drinkingValue, false);
// Agent.cpp: 	#ifdef DEBUG
// Agent.cpp: 	std::cout << "my probablistic drinking number: " << myDrinks << std::endl;
// Agent.cpp: 	#endif
// Agent.cpp: */
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       thisClass.doAction();
       std::cout << "After using Agent\n";

#ifdef TEST_AGENT_DOACTION_IN_RANGE


#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_DOACTION
#ifdef TEST_AGENT_DOACTION_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_doAction elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_ageAgent(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_AGEAGENT
//#define TEST_AGENT_AGEAGENT_IN_RANGE

#ifdef TEST_AGENT_AGEAGENT


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::ageAgent(){
// Agent.cpp: 	mAge++;
// Agent.cpp: 	mYear++;
// Agent.cpp: 	std::string mortalityHashKey = makeDeathRateHashKey(std::to_string(mYear), std::to_string(mSex), std::to_string(mAge));
// Agent.cpp: 	std::string migrationHashKey =  makeMigrationOutHashKey(std::to_string(mYear), std::to_string(mSex), mRace, std::to_string(mAge));;
// Agent.cpp: 	mMortality = deathRateTable[mortalityHashKey];
// Agent.cpp: 	mMigrationOutRate = migrationOutTable[migrationHashKey];
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       thisClass.ageAgent();
       std::cout << "After using Agent\n";

#ifdef TEST_AGENT_AGEAGENT_IN_RANGE


#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_AGEAGENT
#ifdef TEST_AGENT_AGEAGENT_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_ageAgent elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_reset12MonthDrinker(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_RESET12MONTHDRINKER
//#define TEST_AGENT_RESET12MONTHDRINKER_IN_RANGE

#ifdef TEST_AGENT_RESET12MONTHDRINKER


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::reset12MonthDrinker() {
// Agent.cpp: 	mIs12MonthDrinker = false;
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       thisClass.reset12MonthDrinker();
       std::cout << "After using Agent\n";

#ifdef TEST_AGENT_RESET12MONTHDRINKER_IN_RANGE


#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_RESET12MONTHDRINKER
#ifdef TEST_AGENT_RESET12MONTHDRINKER_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_reset12MonthDrinker elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_resetTotalDrinksPerAnnum(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_RESETTOTALDRINKSPERANNUM
//#define TEST_AGENT_RESETTOTALDRINKSPERANNUM_IN_RANGE

#ifdef TEST_AGENT_RESETTOTALDRINKSPERANNUM


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::resetTotalDrinksPerAnnum(){
// Agent.cpp: 	mTotalDrinksPerAnnum = 0;
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       thisClass.resetTotalDrinksPerAnnum();
       std::cout << "After using Agent\n";

#ifdef TEST_AGENT_RESETTOTALDRINKSPERANNUM_IN_RANGE


#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_RESETTOTALDRINKSPERANNUM
#ifdef TEST_AGENT_RESETTOTALDRINKSPERANNUM_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_resetTotalDrinksPerAnnum elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_findAgeGroup(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_FINDAGEGROUP
//#define TEST_AGENT_FINDAGEGROUP_IN_RANGE

#ifdef TEST_AGENT_FINDAGEGROUP

    int result_0;
    int expected_0;
    int expected_min_0;
    int expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: int Agent::findAgeGroup() {
// Agent.cpp: 	int myAgeGroup = 0;
// Agent.cpp: 
// Agent.cpp: 	if (mAge <= MAX_AGE) {
// Agent.cpp: 		while (mAge > AGE_GROUPS[myAgeGroup]) {
// Agent.cpp: 			myAgeGroup++;
// Agent.cpp: 		}
// Agent.cpp: 	} else {
// Agent.cpp: 		myAgeGroup = NUM_AGE_GROUPS - 1;
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	return myAgeGroup; // Age group 0 is the youngest age groups
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.findAgeGroup();
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_FINDAGEGROUP_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_FINDAGEGROUP
#ifdef TEST_AGENT_FINDAGEGROUP_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_findAgeGroup elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_isHaveKDrinksOverNDays(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_ISHAVEKDRINKSOVERNDAYS
//#define TEST_AGENT_ISHAVEKDRINKSOVERNDAYS_IN_RANGE

#ifdef TEST_AGENT_ISHAVEKDRINKSOVERNDAYS
    int my_numberOfDays;
    int my_kNumberDrinks;
    bool result_0;
    bool expected_0;
    bool expected_min_0;
    bool expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_numberOfDays = XYZ;
        my_kNumberDrinks = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: bool Agent::isHaveKDrinksOverNDays(int numberOfDays, int kNumberDrinks) {
// Agent.cpp: 	bool nDaysKDrinking = false;
// Agent.cpp: 	int index = mPastYearDrinks.size()-1;
// Agent.cpp: 	while (numberOfDays > 0) {
// Agent.cpp: 		if (mPastYearDrinks[index] >= kNumberDrinks){
// Agent.cpp: 			nDaysKDrinking = true;
// Agent.cpp: 		}
// Agent.cpp: 		index--;
// Agent.cpp: 		numberOfDays--;
// Agent.cpp: 	}
// Agent.cpp: 	return nDaysKDrinking;
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.isHaveKDrinksOverNDays(my_numberOfDays, my_kNumberDrinks);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_ISHAVEKDRINKSOVERNDAYS_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_ISHAVEKDRINKSOVERNDAYS
#ifdef TEST_AGENT_ISHAVEKDRINKSOVERNDAYS_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_isHaveKDrinksOverNDays elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_getAvgDrinksNDays(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_GETAVGDRINKSNDAYS
//#define TEST_AGENT_GETAVGDRINKSNDAYS_IN_RANGE

#ifdef TEST_AGENT_GETAVGDRINKSNDAYS
    int my_numberOfDays;
    double result_0;
    double expected_0;
    double expected_min_0;
    double expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_numberOfDays = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: double Agent::getAvgDrinksNDays(int numberOfDays){
// Agent.cpp: 	double averageDrinks = 0;
// Agent.cpp: 	int totalDays = numberOfDays;
// Agent.cpp: 	int index = mPastYearDrinks.size()-1;
// Agent.cpp: 	while (numberOfDays > 0) {
// Agent.cpp: 		averageDrinks = averageDrinks + mPastYearDrinks[index];
// Agent.cpp: 		index--;
// Agent.cpp: 		numberOfDays--;
// Agent.cpp: 	}
// Agent.cpp: 	return averageDrinks/totalDays;
// Agent.cpp: }
// Agent.cpp: double  Agent::getAvgDrinksNDays(int numberOfDays, bool perOccasion){
// Agent.cpp: 	double averageDrinks = 0;
// Agent.cpp: 	int index = mPastYearDrinks.size()-1;
// Agent.cpp: 	int totalDays;
// Agent.cpp: 	if (perOccasion == false) {
// Agent.cpp: 		totalDays = numberOfDays;
// Agent.cpp: 		while (numberOfDays > 0) {
// Agent.cpp: 			averageDrinks = averageDrinks + mPastYearDrinks[index];
// Agent.cpp: 			index--;
// Agent.cpp: 			numberOfDays--;
// Agent.cpp: 		}
// Agent.cpp: 	} else {
// Agent.cpp: 		totalDays = 0;
// Agent.cpp: 		while (numberOfDays > 0) {
// Agent.cpp: 			if (mPastYearDrinks[index] > 0) {
// Agent.cpp: 			averageDrinks = averageDrinks + mPastYearDrinks[index];
// Agent.cpp: 			totalDays++;
// Agent.cpp: 		}
// Agent.cpp: 		index--;
// Agent.cpp: 		numberOfDays--;
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	}
// Agent.cpp: 	if (totalDays == 0){
// Agent.cpp: 		averageDrinks = 0;
// Agent.cpp: 	} else {
// Agent.cpp: 		averageDrinks = averageDrinks/totalDays;
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	return averageDrinks;
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.getAvgDrinksNDays(my_numberOfDays);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_GETAVGDRINKSNDAYS_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_GETAVGDRINKSNDAYS
#ifdef TEST_AGENT_GETAVGDRINKSNDAYS_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_getAvgDrinksNDays elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_getNumDaysHavingKDrinksOverNDays(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_GETNUMDAYSHAVINGKDRINKSOVERNDAYS
//#define TEST_AGENT_GETNUMDAYSHAVINGKDRINKSOVERNDAYS_IN_RANGE

#ifdef TEST_AGENT_GETNUMDAYSHAVINGKDRINKSOVERNDAYS
    int my_numberOfDays;
    int my_kNumberDrinks;
    int result_0;
    int expected_0;
    int expected_min_0;
    int expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_numberOfDays = XYZ;
        my_kNumberDrinks = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: int Agent::getNumDaysHavingKDrinksOverNDays(int numberOfDays, int kNumberDrinks) {
// Agent.cpp: 	int countDays = 0;
// Agent.cpp: 	int index = mPastYearDrinks.size()-1;
// Agent.cpp: 	while (numberOfDays > 0) {
// Agent.cpp: 		if (mPastYearDrinks[index] >= kNumberDrinks){
// Agent.cpp: 			countDays++;
// Agent.cpp: 		}
// Agent.cpp: 		index--;
// Agent.cpp: 		numberOfDays--;
// Agent.cpp: 	}
// Agent.cpp: 	return countDays;
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.getNumDaysHavingKDrinksOverNDays(my_numberOfDays, my_kNumberDrinks);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_GETNUMDAYSHAVINGKDRINKSOVERNDAYS_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_GETNUMDAYSHAVINGKDRINKSOVERNDAYS
#ifdef TEST_AGENT_GETNUMDAYSHAVINGKDRINKSOVERNDAYS_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_getNumDaysHavingKDrinksOverNDays elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_shuffleList(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_SHUFFLELIST
//#define TEST_AGENT_SHUFFLELIST_IN_RANGE

#ifdef TEST_AGENT_SHUFFLELIST
    std::vector<T>& my_elementList;
    void result_0;
    void expected_0;
    void expected_min_0;
    void expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_elementList = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::shuffleList(std::vector<T>& elementList){
// Agent.cpp:   if(elementList.size() <= 1) return;
// Agent.cpp:   repast::DoubleUniformGenerator rnd = repast::Random::instance()->createUniDoubleGenerator(0, 1);
// Agent.cpp:   T swap;
// Agent.cpp:   for(int pos = elementList.size() - 1; pos > 0; pos--){
// Agent.cpp: 	  int range = pos + 1;
// Agent.cpp: 	  int other = (int)(rnd.next() * (range));
// Agent.cpp: 	  swap = elementList[pos];
// Agent.cpp: 	  elementList[pos] = elementList[other];
// Agent.cpp: 	  elementList[other] = swap;
// Agent.cpp:   }
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.shuffleList(my_elementList);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_SHUFFLELIST_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_SHUFFLELIST
#ifdef TEST_AGENT_SHUFFLELIST_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_shuffleList elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_initPastYearDrinks(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_INITPASTYEARDRINKS
//#define TEST_AGENT_INITPASTYEARDRINKS_IN_RANGE

#ifdef TEST_AGENT_INITPASTYEARDRINKS
    int my_monthlyDrinks;
    double my_monthlyDrinksSDPct;
    void result_0;
    void expected_0;
    void expected_min_0;
    void expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_monthlyDrinks = XYZ;
        my_monthlyDrinksSDPct = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::initPastYearDrinks(int monthlyDrinks, double monthlyDrinksSDPct){
// Agent.cpp: 	double doubleMonthlyDrinks = double (monthlyDrinks);
// Agent.cpp: 
// Agent.cpp: 	int pastYearDrinksLength; //number of non-negative entries in pastYearDrinks vector.  365 for everyday, 0 for abstinence
// Agent.cpp: 	std::vector<int> pastYearDrinks;
// Agent.cpp: 
// Agent.cpp: 	double meanDrinksToday = 0;
// Agent.cpp: 	double drinksSD = 0;
// Agent.cpp: 
// Agent.cpp: 	switch(mDrinkFrequencyLevel){
// Agent.cpp: 		case 1:
// Agent.cpp: 			if (mIs12MonthDrinker == false){
// Agent.cpp: 				pastYearDrinksLength = 0;
// Agent.cpp: 			}else {
// Agent.cpp: 				pastYearDrinksLength = 1 + repast::Random::instance()->nextDouble() * 10;
// Agent.cpp: 				mDrinkFrequencyLevel = 2;
// Agent.cpp: 			}
// Agent.cpp: 			break;
// Agent.cpp: 		case 2:
// Agent.cpp: 			pastYearDrinksLength = 11 + repast::Random::instance()->nextDouble() * 7;
// Agent.cpp: 			break;
// Agent.cpp: 		case 3:
// Agent.cpp: 			pastYearDrinksLength = 19 + repast::Random::instance()->nextDouble() * 11;
// Agent.cpp: 			break;
// Agent.cpp: 		case 4:
// Agent.cpp: 			pastYearDrinksLength = 31 + repast::Random::instance()->nextDouble() * 22;
// Agent.cpp: 			break;
// Agent.cpp: 		case 5:
// Agent.cpp: 			pastYearDrinksLength = 54 + repast::Random::instance()->nextDouble() * 117;
// Agent.cpp: 			break;
// Agent.cpp: 		case 6:
// Agent.cpp: 			pastYearDrinksLength = 172 + repast::Random::instance()->nextDouble() * 164;
// Agent.cpp: 			break;
// Agent.cpp: 		case 7:
// Agent.cpp: 			pastYearDrinksLength = 337 + repast::Random::instance()->nextDouble() * 28;
// Agent.cpp: 			break;
// Agent.cpp: 	}
// Agent.cpp: 	if (pastYearDrinksLength == 0){                                  //nondrinkers get a year of 0
// Agent.cpp: 		std::vector<int> pastYearDrinksTemp(365, 0);
// Agent.cpp: 		pastYearDrinks = pastYearDrinksTemp;
// Agent.cpp: 	}else if (mDrinkFrequencyLevel > 1 && doubleMonthlyDrinks == 0){ //0 past month drinks >0 past year drinking, 1 for all drinking days
// Agent.cpp: 		int fillers = 365 - pastYearDrinksLength;
// Agent.cpp: 		int counter = pastYearDrinksLength;
// Agent.cpp: 		std::vector<int> pastYearDrinksTemp(fillers, 0);
// Agent.cpp: 		pastYearDrinks = pastYearDrinksTemp;
// Agent.cpp: 		while (counter != 0){
// Agent.cpp: 			pastYearDrinks.push_back(1);
// Agent.cpp: 			--counter;
// Agent.cpp: 		}
// Agent.cpp: 	//	std::cout << "I drink VERY VERY little" << std::endl;
// Agent.cpp: 	}else{
// Agent.cpp: 		meanDrinksToday = doubleMonthlyDrinks / double(pastYearDrinksLength) * 12 ;
// Agent.cpp: 		drinksSD = meanDrinksToday * monthlyDrinksSDPct;
// Agent.cpp: 		if (meanDrinksToday < 1) //TUONG: add this to fix problem of high freq but low drink => sample 0 every day
// Agent.cpp: 			drinksSD = 1;
// Agent.cpp: 		repast::NormalGenerator normGen = repast::Random::instance()->createNormalGenerator(meanDrinksToday, drinksSD);
// Agent.cpp: 		int yearCounter = 0;
// Agent.cpp: 		int quantityCounter = 0;
// Agent.cpp: 		while (yearCounter != 365){
// Agent.cpp: 			if (pastYearDrinksLength == quantityCounter){
// Agent.cpp: 				pastYearDrinks.push_back(0);
// Agent.cpp: 			}else{
// Agent.cpp: 				double doubleDrinksToday = normGen.next();
// Agent.cpp: 				int numDrinksToday;
// Agent.cpp: 				if (doubleDrinksToday < 1){
// Agent.cpp: 					numDrinksToday = 1;
// Agent.cpp: 				}else{
// Agent.cpp: 					numDrinksToday = round(doubleDrinksToday);
// Agent.cpp: 				}
// Agent.cpp: 				pastYearDrinks.push_back(numDrinksToday);
// Agent.cpp: 				++quantityCounter;
// Agent.cpp: 			}
// Agent.cpp: 			++yearCounter;
// Agent.cpp: 		}
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	shuffleList(pastYearDrinks);
// Agent.cpp: 	mPastYearDrinks = pastYearDrinks;
// Agent.cpp: 	mMeanDrinksToday = meanDrinksToday;
// Agent.cpp: 	mSDDrinksToday = drinksSD;
// Agent.cpp: 
// Agent.cpp: 	//calculate mean and variance from history using Welford method
// Agent.cpp: 	mPastYearN = 0;
// Agent.cpp: 	mPastYearMeanDrink = 0;
// Agent.cpp: 	mPastYearSquaredDistanceDrink = 0;
// Agent.cpp: 	double oldMean = 0;
// Agent.cpp: 	for (int k=1; k<=365; k++) { //length of past-year-drink vector must be 365
// Agent.cpp: 		int x = pastYearDrinks[k-1];
// Agent.cpp: 		if (x!=0) { //only account for drinking days (number of drinks > 0)
// Agent.cpp: 			mPastYearN++;
// Agent.cpp: 			oldMean = mPastYearMeanDrink;
// Agent.cpp: 			mPastYearMeanDrink = mPastYearMeanDrink + (x - mPastYearMeanDrink) / mPastYearN;
// Agent.cpp: 			mPastYearSquaredDistanceDrink = mPastYearSquaredDistanceDrink + (x - mPastYearMeanDrink)*(x-oldMean);
// Agent.cpp: 		}
// Agent.cpp: 	}
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.initPastYearDrinks(my_monthlyDrinks, my_monthlyDrinksSDPct);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_INITPASTYEARDRINKS_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_INITPASTYEARDRINKS
#ifdef TEST_AGENT_INITPASTYEARDRINKS_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_initPastYearDrinks elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_updatePastYearDrinks(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_UPDATEPASTYEARDRINKS
//#define TEST_AGENT_UPDATEPASTYEARDRINKS_IN_RANGE

#ifdef TEST_AGENT_UPDATEPASTYEARDRINKS


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::updatePastYearDrinks(){
// Agent.cpp: 	/* update drinks vector */
// Agent.cpp: 	if (mIsDrinkingToday == true){
// Agent.cpp: 		mPastYearDrinks.push_back(mNumberDrinksToday);
// Agent.cpp: 		updateForwardMeanVariance(mNumberDrinksToday);
// Agent.cpp: 	} else {
// Agent.cpp: 		mPastYearDrinks.push_back(0);
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	if ( mPastYearDrinks.front() != 0 ) {
// Agent.cpp: 		updateBackwardMeanVariance(mPastYearDrinks.front());
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	mPastYearDrinks.erase(mPastYearDrinks.begin());
// Agent.cpp: 
// Agent.cpp: 	/* update schema vector */
// Agent.cpp: 	mPastYearSchemas.push_back(mTodaySchema);
// Agent.cpp: 	mPastYearSchemas.erase(mPastYearSchemas.begin());
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       thisClass.updatePastYearDrinks();
       std::cout << "After using Agent\n";

#ifdef TEST_AGENT_UPDATEPASTYEARDRINKS_IN_RANGE


#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_UPDATEPASTYEARDRINKS
#ifdef TEST_AGENT_UPDATEPASTYEARDRINKS_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_updatePastYearDrinks elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_updateForwardMeanVariance(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_UPDATEFORWARDMEANVARIANCE
//#define TEST_AGENT_UPDATEFORWARDMEANVARIANCE_IN_RANGE

#ifdef TEST_AGENT_UPDATEFORWARDMEANVARIANCE
    int my_addedValue;
    void result_0;
    void expected_0;
    void expected_min_0;
    void expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_addedValue = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::updateForwardMeanVariance(int addedValue) {
// Agent.cpp: 	mPastYearN++;
// Agent.cpp: 	if (mPastYearN != 0) {
// Agent.cpp: 		double oldMean = mPastYearMeanDrink;
// Agent.cpp: 		mPastYearMeanDrink = mPastYearMeanDrink + (addedValue - mPastYearMeanDrink) / mPastYearN;
// Agent.cpp: 		mPastYearSquaredDistanceDrink = mPastYearSquaredDistanceDrink + (addedValue - oldMean)*(addedValue - mPastYearMeanDrink);
// Agent.cpp: 	} else {
// Agent.cpp: 		mPastYearMeanDrink = 0;
// Agent.cpp: 		mPastYearSquaredDistanceDrink = 0;
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	//limit estimated-mean between 0 and MAX_DRINKS
// Agent.cpp: 	if (mPastYearMeanDrink < 0) {mPastYearMeanDrink = 0;}
// Agent.cpp: 	if (mPastYearMeanDrink > MAX_DRINKS) {mPastYearMeanDrink = MAX_DRINKS;}
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.updateForwardMeanVariance(my_addedValue);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_UPDATEFORWARDMEANVARIANCE_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_UPDATEFORWARDMEANVARIANCE
#ifdef TEST_AGENT_UPDATEFORWARDMEANVARIANCE_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_updateForwardMeanVariance elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_updateBackwardMeanVariance(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_UPDATEBACKWARDMEANVARIANCE
//#define TEST_AGENT_UPDATEBACKWARDMEANVARIANCE_IN_RANGE

#ifdef TEST_AGENT_UPDATEBACKWARDMEANVARIANCE
    int my_removedValue;
    void result_0;
    void expected_0;
    void expected_min_0;
    void expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_removedValue = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::updateBackwardMeanVariance(int removedValue) {
// Agent.cpp: 	mPastYearN--;
// Agent.cpp: 	if (mPastYearN != 0) {
// Agent.cpp: 		double oldMean = mPastYearMeanDrink;
// Agent.cpp: 		mPastYearMeanDrink = ((mPastYearN+1)*mPastYearMeanDrink - removedValue) / mPastYearN;
// Agent.cpp: 		mPastYearSquaredDistanceDrink = mPastYearSquaredDistanceDrink - (removedValue - oldMean)*(removedValue - mPastYearMeanDrink);
// Agent.cpp: 	} else {
// Agent.cpp: 		mPastYearMeanDrink = 0;
// Agent.cpp: 		mPastYearSquaredDistanceDrink = 0;
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	//limit estimated-mean between 0 and MAX_DRINKS
// Agent.cpp: 	if (mPastYearMeanDrink < 0) {mPastYearMeanDrink = 0;}
// Agent.cpp: 	if (mPastYearMeanDrink > MAX_DRINKS) {mPastYearMeanDrink = MAX_DRINKS;}
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.updateBackwardMeanVariance(my_removedValue);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_UPDATEBACKWARDMEANVARIANCE_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_UPDATEBACKWARDMEANVARIANCE
#ifdef TEST_AGENT_UPDATEBACKWARDMEANVARIANCE_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_updateBackwardMeanVariance elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_makeMigrationOutHashKey(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_MAKEMIGRATIONOUTHASHKEY
//#define TEST_AGENT_MAKEMIGRATIONOUTHASHKEY_IN_RANGE

#ifdef TEST_AGENT_MAKEMIGRATIONOUTHASHKEY
    std::string my_year;
    std::string my_sex;
    std::string my_race;
    std::string my_age;
    std::string result_0;
    std::string expected_0;
    std::string expected_min_0;
    std::string expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_year = XYZ;
        my_sex = XYZ;
        my_race = XYZ;
        my_age = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: std::string Agent::makeMigrationOutHashKey(std::string year, std::string sex, std::string race, std::string age) {
// Agent.cpp: 	std::hash<std::string> tp_hash;
// Agent.cpp: 	std::string hash_index("");
// Agent.cpp: 	hash_index = year + sex + race + age;
// Agent.cpp: 	return std::to_string(tp_hash(hash_index));
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.makeMigrationOutHashKey(my_year, my_sex, my_race, my_age);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_MAKEMIGRATIONOUTHASHKEY_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_MAKEMIGRATIONOUTHASHKEY
#ifdef TEST_AGENT_MAKEMIGRATIONOUTHASHKEY_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_makeMigrationOutHashKey elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_makeDeathRateHashKey(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_MAKEDEATHRATEHASHKEY
//#define TEST_AGENT_MAKEDEATHRATEHASHKEY_IN_RANGE

#ifdef TEST_AGENT_MAKEDEATHRATEHASHKEY
    std::string my_year;
    std::string my_sex;
    std::string my_age;
    std::string result_0;
    std::string expected_0;
    std::string expected_min_0;
    std::string expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_year = XYZ;
        my_sex = XYZ;
        my_age = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: std::string Agent::makeDeathRateHashKey(std::string year, std::string sex, std::string age) {
// Agent.cpp: 	std::hash<std::string> tp_hash;
// Agent.cpp: 	std::string hash_index("");
// Agent.cpp: 	hash_index = year + sex + age;
// Agent.cpp: 	return std::to_string(tp_hash(hash_index));
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.makeDeathRateHashKey(my_year, my_sex, my_age);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_MAKEDEATHRATEHASHKEY_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_MAKEDEATHRATEHASHKEY
#ifdef TEST_AGENT_MAKEDEATHRATEHASHKEY_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_makeDeathRateHashKey elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_initPotentialBuddies(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_INITPOTENTIALBUDDIES
//#define TEST_AGENT_INITPOTENTIALBUDDIES_IN_RANGE

#ifdef TEST_AGENT_INITPOTENTIALBUDDIES


    // Set the ranges if you need them. Replace the XYZ

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){


        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::initPotentialBuddies() {
// Agent.cpp: 	mDrinkingBuddies = mpSocialNetwork->getSuccessors(this);
// Agent.cpp: 	mPotentialBuddies = mpSocialNetwork->generateAgentPotentialBuddyList(this);
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       thisClass.initPotentialBuddies();
       std::cout << "After using Agent\n";

#ifdef TEST_AGENT_INITPOTENTIALBUDDIES_IN_RANGE


#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_INITPOTENTIALBUDDIES
#ifdef TEST_AGENT_INITPOTENTIALBUDDIES_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_initPotentialBuddies elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_removeAgentFromPotentialBuddies(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_REMOVEAGENTFROMPOTENTIALBUDDIES
//#define TEST_AGENT_REMOVEAGENTFROMPOTENTIALBUDDIES_IN_RANGE

#ifdef TEST_AGENT_REMOVEAGENTFROMPOTENTIALBUDDIES
    repast::AgentId my_agentId;
    void result_0;
    void expected_0;
    void expected_min_0;
    void expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_agentId = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::removeAgentFromPotentialBuddies(repast::AgentId agentId) {
// Agent.cpp: 	for (std::vector<Agent*>::iterator it = mPotentialBuddies.begin(); it!= mPotentialBuddies.end(); ++it) {
// Agent.cpp: 		if ( (*it)->getId().hashcode() == agentId.hashcode() ) {
// Agent.cpp: 			mPotentialBuddies.erase((it));
// Agent.cpp: 			break;
// Agent.cpp: 		}
// Agent.cpp: 	}
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.removeAgentFromPotentialBuddies(my_agentId);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_REMOVEAGENTFROMPOTENTIALBUDDIES_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_REMOVEAGENTFROMPOTENTIALBUDDIES
#ifdef TEST_AGENT_REMOVEAGENTFROMPOTENTIALBUDDIES_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_removeAgentFromPotentialBuddies elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_calculateSumEffectVector(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_CALCULATESUMEFFECTVECTOR
//#define TEST_AGENT_CALCULATESUMEFFECTVECTOR_IN_RANGE

#ifdef TEST_AGENT_CALCULATESUMEFFECTVECTOR
    std::vector<int> my_friendVector;
    std::vector<double> my_internalVector;
    std::vector<double> result_0;
    std::vector<double> expected_0;
    std::vector<double> expected_min_0;
    std::vector<double> expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_friendVector = XYZ;
        my_internalVector = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: std::vector<double> Agent::calculateSumEffectVector(std::vector<int> friendVector, std::vector<double> internalVector){
// Agent.cpp: 	std::vector<int> friendCopy = friendVector;
// Agent.cpp: 	std::vector<double> totalVector;
// Agent.cpp: 	int index = 0;
// Agent.cpp: 	//change objective function for changing a friend by flipping value of x_ij in the friendCopy vector
// Agent.cpp: 	for(std::vector<int>::const_iterator iter = friendVector.begin(); iter != friendVector.end(); ++iter){
// Agent.cpp: 		if (*iter == 0){
// Agent.cpp: 			friendCopy[index] = 1;
// Agent.cpp: 		}else{
// Agent.cpp: 			friendCopy[index] = 0;
// Agent.cpp: 		}
// Agent.cpp: 
// Agent.cpp: 		double currentSum = std::inner_product(std::begin(friendCopy), std::end(friendCopy),
// Agent.cpp: 										std::begin(internalVector), 0.0);
// Agent.cpp: 		totalVector.push_back(currentSum);
// Agent.cpp: 
// Agent.cpp: 		++index;
// Agent.cpp: 		friendCopy = friendVector;
// Agent.cpp: 	}
// Agent.cpp: 	return totalVector;
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.calculateSumEffectVector(my_friendVector, my_internalVector);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_CALCULATESUMEFFECTVECTOR_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_CALCULATESUMEFFECTVECTOR
#ifdef TEST_AGENT_CALCULATESUMEFFECTVECTOR_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_calculateSumEffectVector elapsed time: " << elapsed_seconds.count() << "[s]\n";
}


void test_Agent_selectIndexFromProbabilityVector(int nTimes, Agent thisClass){
    auto start = std::chrono::system_clock::now();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
//#define TEST_AGENT_SELECTINDEXFROMPROBABILITYVECTOR
//#define TEST_AGENT_SELECTINDEXFROMPROBABILITYVECTOR_IN_RANGE

#ifdef TEST_AGENT_SELECTINDEXFROMPROBABILITYVECTOR
    std::vector<double> my_probVector;
    int result_0;
    int expected_0;
    int expected_min_0;
    int expected_max_0;
    // Set the ranges if you need them. Replace the XYZ
    expected_0 = XYZ;
    expected_min_0 = XYZ;
    expected_max_0 = XYZ;
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_probVector = XYZ;

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: int Agent::selectIndexFromProbabilityVector(std::vector<double> probVector){
// Agent.cpp: 	double chooser = repast::Random::instance()->nextDouble();
// Agent.cpp: 
// Agent.cpp: 	int index = 0;
// Agent.cpp: 	int selected = 0;
// Agent.cpp: 	#ifdef AGENTDEBUG
// Agent.cpp: 	std::cout << "chooser: " << chooser << "\n";
// Agent.cpp: 	#endif
// Agent.cpp: 	std::vector<double>::const_iterator it = probVector.begin();
// Agent.cpp: 	while (selected == 0 && it != probVector.end()){
// Agent.cpp: 		if (chooser < *it){
// Agent.cpp: 			selected = 1;
// Agent.cpp: 		}else{
// Agent.cpp: 			++index;
// Agent.cpp: 			++it;
// Agent.cpp: 		}
// Agent.cpp: 	}
// Agent.cpp: 	if (it == probVector.end()){
// Agent.cpp: 		index = 0;
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	return index;
// Agent.cpp: }


       std::cout << "Before using Agent\n";
       result_0 = thisClass.selectIndexFromProbabilityVector(my_probVector);
       std::cout << "After using Agent\n";
       if(result_0 != expected_0){
           nUnequal++;
       }
#ifdef TEST_AGENT_SELECTINDEXFROMPROBABILITYVECTOR_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    auto end = std::chrono::system_clock::now();

#ifdef TEST_AGENT_SELECTINDEXFROMPROBABILITYVECTOR
#ifdef TEST_AGENT_SELECTINDEXFROMPROBABILITYVECTOR_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "test_Agent_selectIndexFromProbabilityVector elapsed time: " << elapsed_seconds.count() << "[s]\n";
}

//takes as inputs two probabilities (from TheoryMediator) [0,1], which sum to LESS than 1,  and updates the agent variables:
//mTodayOpportunity.  sum > 1 should throw error, both probabilities = 0 should give warning, no error.
//mTodayOpportunity is an Opportunity object which can have values Opportunity::NONE, Opportunity::IN, Opportunity::OUT.
void test_Agent_generateOpportunity(int nTimes, Agent thisClass){
    std::chrono::duration<double>methodTime;
    methodTime = std::chrono::steady_clock::duration::zero();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
#define TEST_AGENT_GENERATEOPPORTUNITY
//#define TEST_AGENT_GENERATEOPPORTUNITY_IN_RANGE

#ifdef TEST_AGENT_GENERATEOPPORTUNITY
    double my_probOppIn;
    double my_probOppOut;
    
    Opportunity expectedOpportunity;
    Opportunity resultOpportunity;

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        if (i == 0){
            my_probOppIn = 0;
            my_probOppOut = 0;
        }else{
            my_probOppIn = repast::Random::instance()->nextDouble();
            my_probOppOut = repast::Random::instance()->nextDouble();
        }   
        if (my_probOppIn + my_probOppOut == 0){
            expectedOpportunity = Opportunity::NONE;
        } else if(my_probOppIn + my_probOppOut > 1){
            expectedOpportunity = Opportunity::TEST_ERROR;
        }else{
            expectedOpportunity = Opportunity::IN;
        }

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::generateOpportunity(double probOppIn, double probOppOut) {
// Agent.cpp: 	if (probOppIn==0 && probOppOut==0) {
// Agent.cpp: 		std::cout << "Warning: both probOppIn and probOppOut = 0" << "\n";
// Agent.cpp: 	}
// Agent.cpp: 	if (probOppIn + probOppOut > 1) {
// Agent.cpp: 		std::cerr << "In Agent::doOpportunity, probOppIn + probOppOut must be <= 1" << "\n";
// Agent.cpp: 		throw MPI::Exception(MPI::ERR_ARG);
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	double randNumber = repast::Random::instance()->nextDouble();
// Agent.cpp: 	if (randNumber < probOppIn)
// Agent.cpp: 		mTodayOpportunity = Opportunity::IN;
// Agent.cpp: 	else if (randNumber < probOppIn + probOppOut)
// Agent.cpp: 		mTodayOpportunity = Opportunity::OUT;
// Agent.cpp: 	else mTodayOpportunity = Opportunity::NONE;
// Agent.cpp: }

        auto start = std::chrono::system_clock::now();
        thisClass.generateOpportunity(my_probOppIn, my_probOppOut);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsedMethodSeconds = end - start;
        methodTime = methodTime + elapsedMethodSeconds;

        resultOpportunity = thisClass.utGet_mTodayOpportunity();
        std::string resultString;
        std::string expectedString;

        if (resultOpportunity == Opportunity::TEST_ERROR){
            resultString = "Error";
        }else{
            resultString = "NotError";
        }
        if (expectedOpportunity == Opportunity::TEST_ERROR){
            expectedString = "Error";
        }else{
            expectedString = "NotError";
        }


       if(expectedOpportunity != Opportunity::IN && expectedOpportunity != resultOpportunity){
           nUnequal++;
           std::cout << "my_probOppIn: " << my_probOppIn << ", my_probOppOut: " << my_probOppOut << std::endl;
           std::cout << "resultOpporutnity: " << resultString << ". expectedOpportunity: " << expectedString << std::endl;
       }

#ifdef TEST_AGENT_GENERATEOPPORTUNITY_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif

#ifdef TEST_AGENT_GENERATEOPPORTUNITY
#ifdef TEST_AGENT_GENERATEOPPORTUNITY_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    
    std::cout << "test_Agent_generateOpportunity elapsed time: " << methodTime.count() << "[s]\n";
}

//gets a drinking plan by looking back over the past year of drinking plans and picking the most common.
//input "days" is the lookback window, and must be between 0, 365.
//returns a DrinkingPlan which is a schema and a probability.
//need to have set: mPastYearSchemas.
void test_Agent_getHabitualDrinkingPlan(int nTimes, Agent thisClass){
    std::chrono::duration<double>methodTime;
    methodTime = std::chrono::steady_clock::duration::zero();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
#define TEST_AGENT_GETHABITUALDRINKINGPLAN
//#define TEST_AGENT_GETHABITUALDRINKINGPLAN_IN_RANGE

#ifdef TEST_AGENT_GETHABITUALDRINKINGPLAN
    DrinkingPlan result_0;
    int my_days;
    DrinkingSchema expectedSchema1;
    DrinkingSchema expectedSchema2;
    DrinkingSchema expectedSchema3;
    DrinkingSchema expectedSchema4;
    DrinkingSchema expectedSchema5;
    int expectedSchemaCount;
    double expectedSchemaProbability;

    DrinkingSchema resultSchema;
    double resultSchemaProbability;

    std::vector<DrinkingSchema> testPastYearSchemas;
    for (int n = 0; n != 360; n++){
        if (n%6 == 0){
            testPastYearSchemas.push_back(DrinkingSchema::NONE);
        }else if (n%6 == 1){
            testPastYearSchemas.push_back(DrinkingSchema::ABSTAIN);
        }else if (n%6 == 2){
            testPastYearSchemas.push_back(DrinkingSchema::LOW);
        }else if (n%6 == 3){
            testPastYearSchemas.push_back(DrinkingSchema::MED);
        }else if (n%6 == 4){
            testPastYearSchemas.push_back(DrinkingSchema::HIGH);
        }else if (n%6 == 5){
            testPastYearSchemas.push_back(DrinkingSchema::VERY_HIGH);
        }
    }
    thisClass.utSet_mPastYearSchemas(testPastYearSchemas);
    std::cout << testPastYearSchemas.size() << std::endl; 
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
        my_days = 1 + i%360;
        double numerator = ceil ((double) my_days / 6);
        double divisor = 5 * floor (double(my_days) / 6) + my_days%6;
        //std::cout << "days" << my_days << ". numerator" << numerator << ". divisor: " << divisor <<std::endl; 
        
        if (my_days%6 == 1){
            expectedSchema1 = DrinkingSchema::VERY_HIGH;
            expectedSchema2 = DrinkingSchema::NONE;
            expectedSchema3 = DrinkingSchema::NONE;
            expectedSchema4 = DrinkingSchema::NONE;
            expectedSchema5 = DrinkingSchema::NONE;
            expectedSchemaCount = 1;
            expectedSchemaProbability = numerator / divisor;
        }else if (my_days%6 == 2){
            expectedSchema1 = DrinkingSchema::VERY_HIGH;
            expectedSchema2 = DrinkingSchema::HIGH;
            expectedSchema3 = DrinkingSchema::NONE;
            expectedSchema4 = DrinkingSchema::NONE;
            expectedSchema5 = DrinkingSchema::NONE;
            expectedSchemaCount = 2;
            expectedSchemaProbability = numerator / divisor;
        }else if (my_days%6 == 3){
            expectedSchema1 = DrinkingSchema::VERY_HIGH;
            expectedSchema2 = DrinkingSchema::HIGH;
            expectedSchema3 = DrinkingSchema::MED;
            expectedSchema4 = DrinkingSchema::NONE;
            expectedSchema5 = DrinkingSchema::NONE;
            expectedSchemaCount = 3;
            expectedSchemaProbability = numerator / divisor;
        }else if(my_days%6 == 4){
            expectedSchema1 = DrinkingSchema::VERY_HIGH;
            expectedSchema2 = DrinkingSchema::HIGH;
            expectedSchema3 = DrinkingSchema::MED;
            expectedSchema4 = DrinkingSchema::LOW;
            expectedSchema5 = DrinkingSchema::NONE;
            expectedSchemaCount = 4;
            expectedSchemaProbability = numerator / divisor;
        }else if(my_days%6 == 5){
            expectedSchema1 = DrinkingSchema::VERY_HIGH;
            expectedSchema2 = DrinkingSchema::HIGH;
            expectedSchema3 = DrinkingSchema::MED;
            expectedSchema4 = DrinkingSchema::LOW;
            expectedSchema5 = DrinkingSchema::ABSTAIN;
            expectedSchemaCount = 5;
            expectedSchemaProbability = numerator / divisor;
        }else if(my_days%6 == 0){
            expectedSchema1 = DrinkingSchema::VERY_HIGH;
            expectedSchema2 = DrinkingSchema::HIGH;
            expectedSchema3 = DrinkingSchema::MED;
            expectedSchema4 = DrinkingSchema::LOW;
            expectedSchema5 = DrinkingSchema::ABSTAIN;
            expectedSchemaCount = 5;
            expectedSchemaProbability = numerator / divisor;
        }
        
        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: DrinkingPlan Agent::getHabitualDrinkingPlan(int days) {
// Agent.cpp: 	if (days < 0 || days > 365) {
// Agent.cpp: 		std::cerr << "Agent::getHabitualDrinkingPlan. Condition: 1 <= days <= 365" << "\n";
// Agent.cpp: 		throw MPI::Exception(MPI::ERR_ARG);
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	DrinkingPlan plan;
// Agent.cpp: 	int totalActiveSchema = 0;
// Agent.cpp: 	int countSchema[5] = {0};
// Agent.cpp: 
// Agent.cpp: 	vector<DrinkingSchema>::iterator it = mPastYearSchemas.end();
// Agent.cpp: 	while (days > 0) {
// Agent.cpp: 		switch (*it) {
// Agent.cpp: 			case DrinkingSchema::ABSTAIN:
// Agent.cpp: 				countSchema[0]++;
// Agent.cpp: 				totalActiveSchema++;
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::LOW:
// Agent.cpp: 				countSchema[1]++;
// Agent.cpp: 				totalActiveSchema++;
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::MED:
// Agent.cpp: 				countSchema[2]++;
// Agent.cpp: 				totalActiveSchema++;
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::HIGH:
// Agent.cpp: 				countSchema[3]++;
// Agent.cpp: 				totalActiveSchema++;
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::VERY_HIGH:
// Agent.cpp: 				countSchema[4]++;
// Agent.cpp: 				totalActiveSchema++;
// Agent.cpp: 				break;
// Agent.cpp: 		}
// Agent.cpp: 		--it;
// Agent.cpp: 		--days;
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	int maxIndex = -1;
// Agent.cpp: 	int maxValue = -1;
// Agent.cpp: 	for (int i=0; i<5; i++) {
// Agent.cpp: 		if (countSchema[i] > maxValue) {
// Agent.cpp: 			maxValue = countSchema[i];
// Agent.cpp: 			maxIndex = i;
// Agent.cpp: 		}
// Agent.cpp: 		if (countSchema[i] == maxValue &&
// Agent.cpp: 			repast::Random::instance()->nextDouble() < 0.5) { //if equal, there is 50% chance that the max value will change (not essential)
// Agent.cpp: 				maxValue = countSchema[i];
// Agent.cpp: 				maxIndex = i;
// Agent.cpp: 		}
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	plan.schema = DrinkingSchema::NONE;
// Agent.cpp: 	switch (maxIndex) {
// Agent.cpp: 		case 0:
// Agent.cpp: 			plan.schema = DrinkingSchema::ABSTAIN;
// Agent.cpp: 			break;
// Agent.cpp: 		case 1:
// Agent.cpp: 			plan.schema = DrinkingSchema::LOW;
// Agent.cpp: 			break;
// Agent.cpp: 		case 2:
// Agent.cpp: 			plan.schema = DrinkingSchema::MED;
// Agent.cpp: 			break;
// Agent.cpp: 		case 3:
// Agent.cpp: 			plan.schema = DrinkingSchema::HIGH;
// Agent.cpp: 			break;
// Agent.cpp: 		case 4:
// Agent.cpp: 			plan.schema = DrinkingSchema::VERY_HIGH;
// Agent.cpp: 			break;
// Agent.cpp: 	}
// Agent.cpp: 	if (plan.schema != DrinkingSchema::NONE)
// Agent.cpp: 		plan.probability = (double) countSchema[maxIndex] / (double) totalActiveSchema;
// Agent.cpp: 	else {
// Agent.cpp: 		//this point is reached when there is no most common index found, all schemas are NONE
// Agent.cpp: 		std::cout << "Warning: can't find the most common DrinkingSchema. Return: schema NONE, prob 1.0." << std::endl;
// Agent.cpp: 		plan.probability = 1;
// Agent.cpp: 	}
// Agent.cpp: 
// Agent.cpp: 	return plan;
// Agent.cpp: }

        auto start = std::chrono::system_clock::now();
        result_0 = thisClass.getHabitualDrinkingPlan(my_days);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsedMethodSeconds = end - start;
        methodTime = methodTime + elapsedMethodSeconds;

       resultSchema = result_0.schema;
       resultSchemaProbability = result_0.probability;
       std::string resultString;
        if (resultSchema == DrinkingSchema::NONE){
            resultString = "NONE";
           }else if(resultSchema == DrinkingSchema::ABSTAIN){
            resultString = "ABSTAIN";
           }else if(resultSchema == DrinkingSchema::LOW){
            resultString = "LOW";
           }else if(resultSchema == DrinkingSchema::MED){
            resultString = "MED";
           }else if(resultSchema == DrinkingSchema::HIGH){
            resultString = "HIGH";
           }else if(resultSchema == DrinkingSchema::VERY_HIGH){
            resultString = "VERY_HIGH";
           }else{
            resultString = "error";
           }
           //TODO: figure out the check expression.  Becuase it is wrong, even though the method is right.
       if((expectedSchemaCount == 1 && (expectedSchema1 != resultSchema || expectedSchemaProbability != resultSchemaProbability)) ||
          (expectedSchemaCount == 2 && ((resultSchema != expectedSchema1 && resultSchema != expectedSchema2)|| expectedSchemaProbability != resultSchemaProbability)) ||
          (expectedSchemaCount == 3 && ((resultSchema != expectedSchema1 && resultSchema != expectedSchema2 && resultSchema != expectedSchema3) || expectedSchemaProbability != resultSchemaProbability)) ||
          (expectedSchemaCount == 4 && ((resultSchema != expectedSchema1 && resultSchema != expectedSchema2 && resultSchema != expectedSchema3 && resultSchema != expectedSchema4 ) || expectedSchemaProbability != resultSchemaProbability)) ||
          (expectedSchemaCount == 5 && ((resultSchema != expectedSchema1 && resultSchema != expectedSchema2 && resultSchema != expectedSchema3 && resultSchema != expectedSchema4 && resultSchema != expectedSchema5) ||expectedSchemaProbability != resultSchemaProbability))          
          ){
           nUnequal++;
           std::cout << "run number: " << i << std::endl;
           std::cout << "resultSchema" << resultString << ". resultSchemaProbability: " << resultSchemaProbability << std::endl;
           std::cout << "expectedSchemaProbability: " << expectedSchemaProbability << std::endl;
       }
#ifdef TEST_AGENT_GETHABITUALDRINKINGPLAN_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif

#ifdef TEST_AGENT_GETHABITUALDRINKINGPLAN
#ifdef TEST_AGENT_GETHABITUALDRINKINGPLAN_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::cout << "test_Agent_getHabitualDrinkingPlan elapsed time: " << methodTime.count() << "[s]\n";
}

//takes in a schema, updates several agent "m" drinking variables.
//need to set mSex, mTotalDrinksPerAnnum, mIs12MonthDrinker. And test that:
//mTodaySchema, mIsDrinkingToday, mNumberDrinksToday, mTotalDrinksPerAnnum, and mIs12MonthDrinker are as expected.
void test_Agent_doDrinking(int nTimes, Agent thisClass){
    std::chrono::duration<double>methodTime;
    methodTime = std::chrono::steady_clock::duration::zero();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
#define TEST_AGENT_DODRINKING
//#define TEST_AGENT_DODRINKING_IN_RANGE

#ifdef TEST_AGENT_DODRINKING
    DrinkingSchema my_schema;

    int testTotalDrinksPerAnnum = 100;
    int testIs12MonthDrinker;

    DrinkingSchema expectedTodaySchema;
    bool expectedIsDrinkingToday;
    int expectedNumberDrinksTodayMin;
    int expectedNumberDrinksTodayMax;
    int expectedTotalDrinksPerAnnumMin;
    int expectedTotalDrinksPerAnnumMax;
    bool expectedIs12MonthDrinker;

    DrinkingSchema resultTodaySchema;
    bool resultIsDrinkingToday;
    int resultNumberDrinksToday;
    int resultTotalDrinksPerAnnum;
    bool resultIs12MonthDrinker;

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        // set the arguments here. Use random values if you wish
       bool mySex = i%2;
        thisClass.utSet_mSex(mySex);
        thisClass.utSet_mTotalDrinksPerAnnum(testTotalDrinksPerAnnum);
        thisClass.utSet_mIs12MonthDrinker(false);

        // set the arguments here. Use random values if you wish
        if (i == 0){
            my_schema = DrinkingSchema::NONE;
            expectedNumberDrinksTodayMax = 0;
            expectedNumberDrinksTodayMin = 0;
            expectedTodaySchema = my_schema;
            expectedIsDrinkingToday = false;
            expectedTotalDrinksPerAnnumMin = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMin;
            expectedTotalDrinksPerAnnumMax = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMax;
            expectedIs12MonthDrinker = false;

        }else if(i == 1){
            my_schema = DrinkingSchema::ABSTAIN;
            expectedNumberDrinksTodayMax = 0;
            expectedNumberDrinksTodayMin = 0;
            expectedTodaySchema = my_schema;
            expectedIsDrinkingToday = false;
            expectedTotalDrinksPerAnnumMin = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMin;
            expectedTotalDrinksPerAnnumMax = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMax;
            expectedIs12MonthDrinker = false;
        }else if (i < 200){
            my_schema = DrinkingSchema::LOW;
            if (mySex == MALE) {
                expectedNumberDrinksTodayMin = 1;
                expectedNumberDrinksTodayMax = 2;
                expectedTodaySchema = my_schema;
                expectedIsDrinkingToday = true;
                expectedTotalDrinksPerAnnumMin = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMin;
                expectedTotalDrinksPerAnnumMax = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMax;
                expectedIs12MonthDrinker = true;
            }else{
                expectedNumberDrinksTodayMin = 1;
                expectedNumberDrinksTodayMax = 1;
                expectedTodaySchema = my_schema;
                expectedIsDrinkingToday = true;
                expectedTotalDrinksPerAnnumMin = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMin;
                expectedTotalDrinksPerAnnumMax = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMax;
                expectedIs12MonthDrinker = true;
            }
        }else if (i < 400){
            my_schema = DrinkingSchema::MED;
            if (mySex == MALE) {
                expectedNumberDrinksTodayMin = 3;
                expectedNumberDrinksTodayMax = 4;
                expectedTodaySchema = my_schema;
                expectedIsDrinkingToday = true;
                expectedTotalDrinksPerAnnumMin = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMin;
                expectedTotalDrinksPerAnnumMax = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMax;
                expectedIs12MonthDrinker = true;
            }else{
                expectedNumberDrinksTodayMin = 2;
                expectedNumberDrinksTodayMax = 2;
                expectedTodaySchema = my_schema;
                expectedIsDrinkingToday = true;
                expectedTotalDrinksPerAnnumMin = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMin;
                expectedTotalDrinksPerAnnumMax = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMax;
                expectedIs12MonthDrinker = true;
            }
        }else if (i < 600){
            my_schema = DrinkingSchema::HIGH;
            if (mySex == MALE) {
                expectedNumberDrinksTodayMin = 5;
                expectedNumberDrinksTodayMax = 7;
                expectedTodaySchema = my_schema;
                expectedIsDrinkingToday = true;
                expectedTotalDrinksPerAnnumMin = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMin;
                expectedTotalDrinksPerAnnumMax = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMax;
                expectedIs12MonthDrinker = true;
            }else{
                expectedNumberDrinksTodayMin = 3;
                expectedNumberDrinksTodayMax = 4;
                expectedTodaySchema = my_schema;
                expectedIsDrinkingToday = true;
                expectedTotalDrinksPerAnnumMin = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMin;
                expectedTotalDrinksPerAnnumMax = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMax;
                expectedIs12MonthDrinker = true;
            }
        }else{
            my_schema = DrinkingSchema::VERY_HIGH;
            if (mySex == MALE) {
                expectedNumberDrinksTodayMin = 8;
                expectedNumberDrinksTodayMax = MAX_DRINKS;
                expectedTodaySchema = my_schema;
                expectedIsDrinkingToday = true;
                expectedTotalDrinksPerAnnumMin = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMin;
                expectedTotalDrinksPerAnnumMax = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMax;
                expectedIs12MonthDrinker = true;
            }else{
                expectedNumberDrinksTodayMin = 5;
                expectedNumberDrinksTodayMax = MAX_DRINKS;
                expectedTodaySchema = my_schema;
                expectedIsDrinkingToday = true;
                expectedTotalDrinksPerAnnumMin = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMin;
                expectedTotalDrinksPerAnnumMax = testTotalDrinksPerAnnum + expectedNumberDrinksTodayMax;
                expectedIs12MonthDrinker = true;
            }
        }

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::doDrinking(DrinkingSchema schema) {
// Agent.cpp: 	mTodaySchema = schema;
// Agent.cpp: 	int numberOfDrinks = getDrinksFromSchema(schema);
// Agent.cpp: 
// Agent.cpp: 	mIsDrinkingToday = (numberOfDrinks > 0);
// Agent.cpp: 	mNumberDrinksToday = numberOfDrinks;
// Agent.cpp: 
// Agent.cpp: 	mTotalDrinksPerAnnum += numberOfDrinks;
// Agent.cpp: 	#ifdef DEBUG
// Agent.cpp: 		if (mId.id() == IDFOROUTPUT){
// Agent.cpp: 			std::cout<<"mTotalDrinksPerAnnum = "<<mTotalDrinksPerAnnum<<std::endl;
// Agent.cpp: 			std::cout<<"mNumberDrinksToday = "<<mNumberDrinksToday<<std::endl;
// Agent.cpp: 		}
// Agent.cpp: 	#endif
// Agent.cpp: 
// Agent.cpp: 	if (mIsDrinkingToday) {mIs12MonthDrinker = true;} //turn on the 12-month drinker flag if this agent drinks
// Agent.cpp: }
        auto start = std::chrono::system_clock::now();
        thisClass.doDrinking(my_schema);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsedMethodSeconds = end - start;
        methodTime = methodTime + elapsedMethodSeconds;

        resultTodaySchema = thisClass.utGet_mTodaySchema();
        resultIsDrinkingToday = thisClass.utGet_mIsDrinkingToday();
        resultNumberDrinksToday = thisClass.utGet_mNumberDrinksToday();
        resultTotalDrinksPerAnnum = thisClass.utGet_mTotalDrinksPerAnnum();
        resultIs12MonthDrinker = thisClass.utGet_mIs12MonthDrinker();
       
       if(resultTodaySchema != expectedTodaySchema ||
          resultIsDrinkingToday != expectedIsDrinkingToday ||
          resultNumberDrinksToday < expectedNumberDrinksTodayMin || resultNumberDrinksToday > expectedNumberDrinksTodayMax ||
          resultTotalDrinksPerAnnum < expectedTotalDrinksPerAnnumMin || resultTotalDrinksPerAnnum > expectedTotalDrinksPerAnnumMax || 
          resultIs12MonthDrinker != expectedIs12MonthDrinker){
           
            nUnequal++;
            std::cout << "resultIsDrinkingToday: " << resultIsDrinkingToday << ". expectedIsDrinkingToday: " << expectedIsDrinkingToday;
            std::cout << ". resultNumberDrinksToday: " << resultNumberDrinksToday << ". expectedNumberDrinksToday: " << expectedNumberDrinksTodayMin << ", " << expectedNumberDrinksTodayMax;
            std::cout << ". resultTotalDrinksPerAnnum: " << resultTotalDrinksPerAnnum << ". expectedTotalDrinksPerAnnum: " << expectedTotalDrinksPerAnnumMin << ", " << expectedTotalDrinksPerAnnumMax;
            std::cout << ". resultIs12MonthDrinker: " << resultIs12MonthDrinker << ". expectedIs12MonthDrinker: " << expectedIs12MonthDrinker;
            std::cout << std::endl;
       }

       // std::cout << "resultIsDrinkingToday: " << resultIsDrinkingToday << ". expectedIsDrinkingToday: " << expectedIsDrinkingToday;
       //      std::cout << ". resultNumberDrinksToday: " << resultNumberDrinksToday << ". expectedNumberDrinksToday: " << expectedNumberDrinksTodayMin << ", " << expectedNumberDrinksTodayMax;
       //      std::cout << ". resultTotalDrinksPerAnnum: " << resultTotalDrinksPerAnnum << ". expectedTotalDrinksPerAnnum: " << expectedTotalDrinksPerAnnumMin << ", " << expectedTotalDrinksPerAnnumMax;
       //      std::cout << ". resultIs12MonthDrinker: " << resultIs12MonthDrinker << ". expectedIs12MonthDrinker: " << expectedIs12MonthDrinker;
       //      std::cout << std::endl;

#ifdef TEST_AGENT_DODRINKING_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    

#ifdef TEST_AGENT_DODRINKING
#ifdef TEST_AGENT_DODRINKING_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    
    std::cout << "test_Agent_doDrinking elapsed time: " << methodTime.count() << "[s]\n";
}

//for a given schema level, returns a number of drinks within the range.
//need to define mSex
void test_Agent_getDrinksFromSchema(int nTimes, Agent thisClass){
    std::chrono::duration<double>methodTime;
    methodTime = std::chrono::steady_clock::duration::zero();
    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
#define TEST_AGENT_GETDRINKSFROMSCHEMA
//#define TEST_AGENT_GETDRINKSFROMSCHEMA_IN_RANGE

#ifdef TEST_AGENT_GETDRINKSFROMSCHEMA
    DrinkingSchema my_schema;
    int result_0;
    int expectedMin;
    int expectedMax;
    
    
    
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        bool mySex = i%2;
        thisClass.utSet_mSex(mySex);
        // set the arguments here. Use random values if you wish
        if (i == 0){
            my_schema = DrinkingSchema::NONE;
            expectedMax = 0;
            expectedMin = 0;
        }else if(i == 1){
            my_schema = DrinkingSchema::ABSTAIN;
            expectedMax = 0;
            expectedMin = 0;
        }else if (i < 200){
            my_schema = DrinkingSchema::LOW;
            if (mySex == MALE) {
                expectedMin = 1;
                expectedMax = 2;
            }else{
                expectedMin = 1;
                expectedMax = 1;
            }
        }else if (i < 400){
            my_schema = DrinkingSchema::MED;
            if (mySex == MALE) {
                expectedMin = 3;
                expectedMax = 4;
            }else{
                expectedMin = 2;
                expectedMax = 2;
            }
        }else if (i < 600){
            my_schema = DrinkingSchema::HIGH;
            if (mySex == MALE) {
                expectedMin = 5;
                expectedMax = 7;
            }else{
                expectedMin = 3;
                expectedMax = 4;
            }
        }else{
            my_schema = DrinkingSchema::VERY_HIGH;
            if (mySex == MALE) {
                expectedMin = 8;
                expectedMax = MAX_DRINKS;
            }else{
                expectedMin = 5;
                expectedMax = MAX_DRINKS;
            }
        }

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: int Agent::getDrinksFromSchema(DrinkingSchema schema) {
// Agent.cpp: 	int numberOfDrinks = 0;
// Agent.cpp: 	if (mSex == MALE) {
// Agent.cpp: 		switch (schema) {
// Agent.cpp: 			case DrinkingSchema::NONE:
// Agent.cpp: 			case DrinkingSchema::ABSTAIN:
// Agent.cpp: 				numberOfDrinks = 0;
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::LOW:
// Agent.cpp: 				numberOfDrinks = int (repast::Random::instance()->nextDouble() * 40 / 14);
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::MED:
// Agent.cpp: 				numberOfDrinks = int ((40 + repast::Random::instance()->nextDouble() * 20) / 14);
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::HIGH:
// Agent.cpp: 				numberOfDrinks = int ((60 + repast::Random::instance()->nextDouble() * 40) / 14);
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::VERY_HIGH:
// Agent.cpp: 				numberOfDrinks = int ((100 + repast::Random::instance()->nextDouble() * (MAX_DRINKS*14-100))/14);
// Agent.cpp: 				break;
// Agent.cpp: 		}
// Agent.cpp: 	} else {
// Agent.cpp: 		switch (schema) {
// Agent.cpp: 			case DrinkingSchema::NONE:
// Agent.cpp: 			case DrinkingSchema::ABSTAIN:
// Agent.cpp: 				numberOfDrinks = 0;
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::LOW:
// Agent.cpp: 				numberOfDrinks = int (repast::Random::instance()->nextDouble() * 20 / 14);
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::MED:
// Agent.cpp: 				numberOfDrinks = int ((20 + repast::Random::instance()->nextDouble() * 20) / 14);
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::HIGH:
// Agent.cpp: 				numberOfDrinks = int ((40 + repast::Random::instance()->nextDouble() * 20) / 14);
// Agent.cpp: 				break;
// Agent.cpp: 			case DrinkingSchema::VERY_HIGH:
// Agent.cpp: 				numberOfDrinks = int ((60 + repast::Random::instance()->nextDouble() * (MAX_DRINKS*14-60))/14);
// Agent.cpp: 				break;
// Agent.cpp: 		}
// Agent.cpp: 	}
// Agent.cpp: 	return numberOfDrinks;
// Agent.cpp: }

        auto start = std::chrono::system_clock::now();
        result_0 = thisClass.getDrinksFromSchema(my_schema);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsedMethodSeconds = end - start;
        methodTime = methodTime + elapsedMethodSeconds;

       
       if(result_0 > expectedMax || result_0 < expectedMin){
           nUnequal++;
           std::cout << "run number: " << i << "sex: " << mySex;
           std::cout << ". result: " << result_0 << ". expectedMin: " << expectedMin << ". expectedMax: " << expectedMax << std::endl;
       }

#ifdef TEST_AGENT_GETDRINKSFROMSCHEMA_IN_RANGE
       if( result_0 < expected_min_0) oor_min++;
       if( result_0 > expected_max_0) oor_max++;
#endif
    }
#endif
    

#ifdef TEST_AGENT_GETDRINKSFROMSCHEMA
#ifdef TEST_AGENT_GETDRINKSFROMSCHEMA_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    
    std::cout << "test_Agent_getDrinksFromSchema elapsed time: " << methodTime.count() << "[s]\n";
}

//In this test, inputs are #drinks, and an abstain probability which should be .01 or .001
//output is a drinkingSchema (one of NONE, ABSTAIN, LOW, MED, HIGH, VERY_HIGH)
//agent values that must be defined before the test: mSex.
//The way it is used is it takes the mPastYearDrinks vector and is applied to each element of the vector.
//Right now the levels are incorrect, because it's based on an aggregate.  Will test to be sure.

//test will increment from 0 to MAX_DRINKS, with special attention paid to 0 for NONE/ABSTAIN
void test_Agent_getSchemaFromDrinks(int nTimes, Agent thisClass){
    std::chrono::duration<double>methodTime;
    methodTime = std::chrono::steady_clock::duration::zero();

    int nUnequal = 0;
#define TEST_AGENT_GETSCHEMAFROMDRINKS
//#define TEST_AGENT_GETSCHEMAFROMDRINKS_IN_RANGE

#ifdef TEST_AGENT_GETSCHEMAFROMDRINKS
    int my_drinks;
    double my_abstainRate;

    DrinkingSchema result_0;
    DrinkingSchema expected_0;
    // Set the ranges if you need them. Replace the XYZ
    
    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        bool mySex = i%2;
        thisClass.utSet_mSex(mySex);
       
        if (i < 100){
            my_drinks = 0;
            my_abstainRate = i < 50 ? 1 : 0;
        }else{
            my_drinks = 1 + i%MAX_DRINKS;
            my_abstainRate = 1;
        }

        if (i < 50){
            expected_0 = DrinkingSchema::ABSTAIN;
        }else if (i >= 50 && i < 100){
            expected_0 = DrinkingSchema::NONE;
        }else if(mySex == FEMALE){
            if ( my_drinks == 1){
                expected_0 = DrinkingSchema::LOW;
            }else if(my_drinks ==2){
                expected_0 = DrinkingSchema::MED;
            }else if(2 < my_drinks && my_drinks <=4){
                expected_0 = DrinkingSchema::HIGH;
            }else if(4 < my_drinks){
                expected_0 = DrinkingSchema::VERY_HIGH;
            }
        }else if(mySex == MALE){
            if (0 < my_drinks && my_drinks <=2){
                expected_0 = DrinkingSchema::LOW;
            }else if(2 < my_drinks && my_drinks <=4){
                expected_0 = DrinkingSchema::MED;
            }else if(4 < my_drinks && my_drinks <=7){
                expected_0 = DrinkingSchema::HIGH;
            }else if(7 < my_drinks){
                expected_0 = DrinkingSchema::VERY_HIGH;
            }
        }

        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: DrinkingSchema Agent::getSchemaFromDrinks(int drinks, double abstainRate) {
// Agent.cpp: 	DrinkingSchema schema = DrinkingSchema::NONE;
// Agent.cpp: 	if (mSex == MALE) {
// Agent.cpp: 		if (drinks == 0 && repast::Random::instance()->nextDouble() < abstainRate)
// Agent.cpp: 			schema = DrinkingSchema::ABSTAIN;
// Agent.cpp: 		else if (0 < drinks && drinks <= 40)
// Agent.cpp: 			schema = DrinkingSchema::LOW;
// Agent.cpp: 		else if (40 < drinks && drinks <= 60)
// Agent.cpp: 			schema = DrinkingSchema::MED;
// Agent.cpp: 		else if (60 < drinks && drinks <= 100)
// Agent.cpp: 			schema = DrinkingSchema::HIGH;
// Agent.cpp: 		else if (100 < drinks)
// Agent.cpp: 			schema = DrinkingSchema::VERY_HIGH;
// Agent.cpp: 	} else {
// Agent.cpp: 		if (drinks == 0 && repast::Random::instance()->nextDouble() < abstainRate)
// Agent.cpp: 			schema = DrinkingSchema::ABSTAIN;
// Agent.cpp: 		else if (0 < drinks && drinks <= 20)
// Agent.cpp: 			schema = DrinkingSchema::LOW;
// Agent.cpp: 		else if (20 < drinks && drinks <= 40)
// Agent.cpp: 			schema = DrinkingSchema::MED;
// Agent.cpp: 		else if (40 < drinks && drinks <= 60)
// Agent.cpp: 			schema = DrinkingSchema::HIGH;
// Agent.cpp: 		else if (60 < drinks)
// Agent.cpp: 			schema = DrinkingSchema::VERY_HIGH;
// Agent.cpp: 	}
// Agent.cpp: 	return schema;
// Agent.cpp: }

        auto start = std::chrono::system_clock::now();
        result_0 = thisClass.getSchemaFromDrinks(my_drinks, my_abstainRate);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsedMethodSeconds = end - start;
        methodTime = methodTime + elapsedMethodSeconds;

       if(result_0 != expected_0){
           nUnequal++;
           std::string resultString;
           std::string expectedString;
           if (result_0 == DrinkingSchema::NONE){
            resultString = "NONE";
           }else if(result_0 == DrinkingSchema::ABSTAIN){
            resultString = "ABSTAIN";
           }else if(result_0 == DrinkingSchema::LOW){
            resultString = "LOW";
           }else if(result_0 == DrinkingSchema::MED){
            resultString = "MED";
           }else if(result_0 == DrinkingSchema::HIGH){
            resultString = "HIGH";
           }else if(result_0 == DrinkingSchema::VERY_HIGH){
            resultString = "VERY_HIGH";
           }else{
            resultString = "error";
           }

           if (expected_0 == DrinkingSchema::NONE){
            expectedString = "NONE";
           }else if(expected_0 == DrinkingSchema::ABSTAIN){
            expectedString = "ABSTAIN";
           }else if(expected_0 == DrinkingSchema::LOW){
            expectedString = "LOW";
           }else if(expected_0 == DrinkingSchema::MED){
            expectedString = "MED";
           }else if(expected_0 == DrinkingSchema::HIGH){
            expectedString = "HIGH";
           }else if(expected_0 == DrinkingSchema::VERY_HIGH){
            expectedString = "VERY_HIGH";
           }else{
            expectedString = "error";
           }
           std::cout << "number Drinks: " << my_drinks << ". sex: " << mySex <<  ". result: " << resultString << " != expected: " << expectedString << std::endl;
       }
       //std::cout << "result: " << result_0 << " !=?? expected: " << expected_0 << std::cout;
    }
#endif

#ifdef TEST_AGENT_GETSCHEMAFROMDRINKS
#ifdef TEST_AGENT_GETSCHEMAFROMDRINKS_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    
    std::cout << "test_Agent_getSchemaFromDrinks elapsed time: " << methodTime.count() << "[s]\n";
}

//should create mPastYearSchemas: a vector of schemas that is 365 elements long.
//needs to be able to tak a person who has all 0s and assign it AT LEAST 1 ABSTAIN.
//must define a mPastYearDrinks vector, mIs12MonthDrink, mSex.
void test_Agent_initPastYearSchemas(int nTimes, Agent thisClass){
     std::chrono::duration<double>methodTime;
    methodTime = std::chrono::steady_clock::duration::zero();

    int oor_min = 0;
    int oor_max = 0;
    int nUnequal = 0;
#define TEST_AGENT_INITPASTYEARSCHEMAS
//#define TEST_AGENT_INITPASTYEARSCHEMAS_IN_RANGE

#ifdef TEST_AGENT_INITPASTYEARSCHEMAS
    std::vector<int> testPastYearDrinks;
    int expectedCountNONEMax;
    int expectedCountABSTAINMin;
    int expectedCountLOW;
    int expectedCountMED;
    int expectedCountHIGH;
    int expectedCountVERY_HIGH;
    std::vector<DrinkingSchema> resultPastYearSchemas;
    int resultCountNONE;
    int resultCountABSTAIN;
    int resultCountLOW;
    int resultCountMED;
    int resultCountHIGH;
    int resultCountVERY_HIGH;

    // for loop goes here
    for(int i = 0 ; i < nTimes; i++){
        testPastYearDrinks.clear();
        bool mySex = i%2;
        thisClass.utSet_mSex(mySex);

        expectedCountNONEMax = 0;
        expectedCountABSTAINMin =0 ;
        expectedCountLOW =0 ;
        expectedCountMED=0;
        expectedCountHIGH=0;
        expectedCountVERY_HIGH=0;

        if (i < 50){ //true abstainers
            thisClass.utSet_mIs12MonthDrinker(false);
            for (int n = 0; n != 365; n++){
                testPastYearDrinks.push_back(0);
            }
            expectedCountNONEMax = 364;
            expectedCountABSTAINMin = 1; //abstainers have to choose to abstain at least once.  (Can't always be no opportunity = NONE.)
            expectedCountLOW = 0;
            expectedCountMED = 0;
            expectedCountHIGH = 0;
            expectedCountVERY_HIGH = 0;
        }else if (i < 100){ //pastyear = yes, past 30 days = no; low frequency, low quantity drinkers.
            thisClass.utSet_mIs12MonthDrinker(true);
            int numDrinks = 1 + i%11;

            expectedCountNONEMax = 365 - numDrinks;
            expectedCountABSTAINMin = 0; // drinkers may never choose to abstain. 
            expectedCountLOW = numDrinks;
            expectedCountMED = 0;
            expectedCountHIGH = 0;
            expectedCountVERY_HIGH = 0;

            for (int n = 0; n != 365 - numDrinks; n++){
                testPastYearDrinks.push_back(0);
            }
            while (numDrinks != 0){
                testPastYearDrinks.push_back(1);
                numDrinks--;
            }
        }else{
            thisClass.utSet_mIs12MonthDrinker(true);
            expectedCountABSTAINMin = 0; // drinkers may never choose to abstain.
            for (int m = 0; m != 365; m++){
                int randomDrink = repast::Random::instance()->nextDouble() * MAX_DRINKS;
                testPastYearDrinks.push_back(randomDrink);
                                
                if (mySex == FEMALE){
                    if (randomDrink == 0){
                        expectedCountNONEMax++;
                    }else if (randomDrink == 1){
                        expectedCountLOW++;
                    }else if (randomDrink <= 2){
                        expectedCountMED++;
                    }else if (randomDrink <= 4){
                        expectedCountHIGH++;
                    }else{
                        expectedCountVERY_HIGH++;
                    }
                }else{
                    if (randomDrink == 0){
                        expectedCountNONEMax++;
                    }else if (randomDrink <= 2){
                        expectedCountLOW++;
                    }else if (randomDrink <= 4){
                        expectedCountMED++;
                    }else if (randomDrink <= 7){
                        expectedCountHIGH++;
                    }else{
                        expectedCountVERY_HIGH++;
                    }
                }
            }
        }

        thisClass.utSet_mPastYearDrinks(testPastYearDrinks);
        // Definition of the class in your code:
        // Do not uncomment the following lines.
// Agent.cpp: void Agent::initPastYearSchemas() {
// Agent.cpp: 	DrinkingSchema schema;
// Agent.cpp: 	bool flagNone = true;
// Agent.cpp: 	for (int i=0; i<365; i++) {
// Agent.cpp: 		int drinks = mPastYearDrinks[i];
// Agent.cpp: 		if (!mIs12MonthDrinker) {
// Agent.cpp: 			schema = getSchemaFromDrinks(drinks, 0.01); // assume abstainers have 1% opp and choose to abstain
// Agent.cpp: 			if (schema!=DrinkingSchema::NONE) flagNone = false;
// Agent.cpp: 			if (i==364 && flagNone) schema = DrinkingSchema::ABSTAIN; //make sure abstainer has at least 1 ABSTAIN schema
// Agent.cpp: 		} else {
// Agent.cpp: 			schema = getSchemaFromDrinks(drinks, 0.001); // assume drinkers have 0.1% rate choosing not to drink
// Agent.cpp: 		}
// Agent.cpp: 		mPastYearSchemas.push_back(schema);
// Agent.cpp: 	}
// Agent.cpp: }

        auto start = std::chrono::system_clock::now();
       thisClass.initPastYearSchemas();
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsedMethodSeconds = end - start;
        methodTime = methodTime + elapsedMethodSeconds;

    resultCountNONE = 0;
    resultCountABSTAIN = 0;
    resultCountLOW = 0;
    resultCountMED = 0;
    resultCountHIGH = 0 ;
    resultCountVERY_HIGH = 0;

    resultPastYearSchemas.clear();
    resultPastYearSchemas = thisClass.utGet_mPastYearSchemas();
    for (int n = 0; n != 365; n++){
        DrinkingSchema localSchema = resultPastYearSchemas[n];
        if (localSchema == DrinkingSchema::ABSTAIN){
            resultCountABSTAIN++;
        }else if(localSchema == DrinkingSchema::NONE){
            resultCountNONE++;
        }else if(localSchema == DrinkingSchema::LOW){
            resultCountLOW++;
        }else if(localSchema == DrinkingSchema::MED){
            resultCountMED++;
        }else if(localSchema == DrinkingSchema::HIGH){
            resultCountHIGH++;
        }else if(localSchema == DrinkingSchema::VERY_HIGH){
            resultCountVERY_HIGH++;
        }
        // if (localSchema == DrinkingSchema::VERY_HIGH ){
        //     std::cout << "VERY HIGH" << std::endl;
        // }else{
        //     std::cout << "not very high " << std::endl;
        // }
    }
    if(resultCountABSTAIN < expectedCountABSTAINMin ||
        resultCountNONE > expectedCountNONEMax ||
        resultCountLOW != expectedCountLOW ||
        resultCountMED != expectedCountMED ||
        resultCountHIGH != expectedCountHIGH ||
        resultCountVERY_HIGH != expectedCountVERY_HIGH){

        nUnequal++;
        std::cout << "run number: " << i << "sex: "<< mySex << std::endl;
        std::cout << "resultCountABSTAIN = " << resultCountABSTAIN << ". expectedCountABSTAINMin " << expectedCountABSTAINMin << std::endl;
        std::cout << "resultCountNONE = " << resultCountNONE << ". expectedCountNONEMax " << expectedCountNONEMax << std::endl;
        std::cout << "resultCountLOW = " << resultCountLOW << ". expectedCountLOW " << expectedCountLOW << std::endl;
        std::cout << "resultCountMED = " << resultCountMED << ". expectedCountMED " << expectedCountMED << std::endl;
        std::cout << "resultCountHIGH = " << resultCountHIGH << ". expectedCountHIGH " << expectedCountHIGH << std::endl;
        std::cout << "resultCountVERY_HIGH = " << resultCountVERY_HIGH << ". expectedCountVERY_HIGH " << expectedCountVERY_HIGH << std::endl;
    }


#ifdef TEST_AGENT_INITPASTYEARSCHEMAS_IN_RANGE


#endif
    }
#endif
   

#ifdef TEST_AGENT_INITPASTYEARSCHEMAS
#ifdef TEST_AGENT_INITPASTYEARSCHEMAS_IN_RANGE
    std::cout << oor_min << " values below minimum out of " << nTimes << "\n";
    std::cout << oor_max << " values above maximum out of " << nTimes << "\n";
#endif
    std::cout << nUnequal << " values incorrect out of " << nTimes << "\n";
#endif
    std::cout << "test_Agent_initPastYearSchemas elapsed time: " << methodTime.count() << "[s]\n";
}


// definition of main
int main(int argc, char **argv){
    if(argc < 3){
        std::cout <<"Usage: test_Agent.exe target nTimes\n";
        return(0);
    }

    std::string target = argv[1];
    int nTimes = std::stoi(argv[2]);
    // examples of how to construct the class based on the cpp file
// Agent::Agent(repast::AgentId id){
//
// Agent::Agent(repast::AgentId id, bool sex, int age, std::string race, int maritalStatus, int parenthoodstatus, int employmentStatus, int income,
//  int drinking, int drinkFrequencyLevel, int monthlyDrinks, double monthlyDrinksSDPct, int year){
//
	Agent thisClass = Agent();
    if(target == "all" || target == "test_constructor_1"){
        test_Agent_1(nTimes );
    }

    if(target == "all" || target == "test_constructor_2"){
        test_Agent_2(nTimes );
    }

    if(target == "all" || target == "setMediator"){
        test_Agent_setMediator(nTimes, thisClass);
    }

    if(target == "all" || target == "initDispositions"){
        test_Agent_initDispositions(nTimes, thisClass);
    }

    if(target == "all" || target == "setDispositionByIndex"){
        test_Agent_setDispositionByIndex(nTimes, thisClass);
    }

    if(target == "all" || target == "doSituation"){
        test_Agent_doSituation(nTimes, thisClass);
    }

    if(target == "all" || target == "doDisposition"){
        test_Agent_doDisposition(nTimes, thisClass);
    }

    if(target == "all" || target == "doDrinkingEngine"){
        test_Agent_doDrinkingEngine(nTimes, thisClass);
    }

    if(target == "all" || target == "doAction"){
        test_Agent_doAction(nTimes, thisClass);
    }

    if(target == "all" || target == "ageAgent"){
        test_Agent_ageAgent(nTimes, thisClass);
    }

    if(target == "all" || target == "reset12MonthDrinker"){
        test_Agent_reset12MonthDrinker(nTimes, thisClass);
    }

    if(target == "all" || target == "resetTotalDrinksPerAnnum"){
        test_Agent_resetTotalDrinksPerAnnum(nTimes, thisClass);
    }

    if(target == "all" || target == "findAgeGroup"){
        test_Agent_findAgeGroup(nTimes, thisClass);
    }

    if(target == "all" || target == "isHaveKDrinksOverNDays"){
        test_Agent_isHaveKDrinksOverNDays(nTimes, thisClass);
    }

    if(target == "all" || target == "getAvgDrinksNDays"){
        test_Agent_getAvgDrinksNDays(nTimes, thisClass);
    }

    if(target == "all" || target == "getNumDaysHavingKDrinksOverNDays"){
        test_Agent_getNumDaysHavingKDrinksOverNDays(nTimes, thisClass);
    }

    if(target == "all" || target == "shuffleList"){
        test_Agent_shuffleList(nTimes, thisClass);
    }

    if(target == "all" || target == "initPastYearDrinks"){
        test_Agent_initPastYearDrinks(nTimes, thisClass);
    }

    if(target == "all" || target == "updatePastYearDrinks"){
        test_Agent_updatePastYearDrinks(nTimes, thisClass);
    }

    if(target == "all" || target == "updateForwardMeanVariance"){
        test_Agent_updateForwardMeanVariance(nTimes, thisClass);
    }

    if(target == "all" || target == "updateBackwardMeanVariance"){
        test_Agent_updateBackwardMeanVariance(nTimes, thisClass);
    }

    if(target == "all" || target == "makeMigrationOutHashKey"){
        test_Agent_makeMigrationOutHashKey(nTimes, thisClass);
    }

    if(target == "all" || target == "makeDeathRateHashKey"){
        test_Agent_makeDeathRateHashKey(nTimes, thisClass);
    }

    if(target == "all" || target == "initPotentialBuddies"){
        test_Agent_initPotentialBuddies(nTimes, thisClass);
    }

    if(target == "all" || target == "removeAgentFromPotentialBuddies"){
        test_Agent_removeAgentFromPotentialBuddies(nTimes, thisClass);
    }

    if(target == "all" || target == "calculateSumEffectVector"){
        test_Agent_calculateSumEffectVector(nTimes, thisClass);
    }

    if(target == "all" || target == "selectIndexFromProbabilityVector"){
        test_Agent_selectIndexFromProbabilityVector(nTimes, thisClass);
    }

    if(target == "all" || target == "generateOpportunity"){
        test_Agent_generateOpportunity(nTimes, thisClass);
    }

    if(target == "all" || target == "getHabitualDrinkingPlan"){
        test_Agent_getHabitualDrinkingPlan(nTimes, thisClass);
    }

    if(target == "all" || target == "doDrinking"){
        test_Agent_doDrinking(nTimes, thisClass);
    }

    if(target == "all" || target == "getDrinksFromSchema"){
        test_Agent_getDrinksFromSchema(nTimes, thisClass);
    }

    if(target == "all" || target == "getSchemaFromDrinks"){
        test_Agent_getSchemaFromDrinks(nTimes, thisClass);
    }

    if(target == "all" || target == "initPastYearSchemas"){
        test_Agent_initPastYearSchemas(nTimes, thisClass);
    }

}
