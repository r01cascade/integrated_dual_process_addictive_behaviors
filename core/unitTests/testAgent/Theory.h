#ifndef INCLUDE_THEORY_H_
#define INCLUDE_THEORY_H_

class Agent; 

#include <utility>
#include "DrinkingPlan.h"

class Theory {

public:
	Agent *mpAgent;

public:
	virtual ~Theory() {};

	void setAgent(Agent *agent);
	virtual void doSituation() = 0;
	virtual void doGatewayDisposition() = 0;
	virtual void doNextDrinksDisposition() = 0;
	virtual void doNonDrinkingActions() = 0;

	
	std::pair<double, double> generateCorrectedMeanSD (double desiredMean, double desiredSd);
	std::pair<double, double> doLookup(double mean, double sd);
	std::pair<double, double> doFunctionLookup(double desiredMean, double desiredSd);

	/* NEW ACTION MECH */

	virtual double getProbOppIn() = 0;
	virtual double getProbOppOut() = 0;
	virtual double getAttitude(DrinkingSchema k) = 0;
	virtual double getNorm(DrinkingSchema k) = 0;
	virtual double getPerceivedBehaviourControl(DrinkingSchema k) = 0;
// for unit test purposes only
//    Theory(){ }
    void utSet_mpAgent( Agent* ut_mpAgent)
        { mpAgent = ut_mpAgent; }

    Agent* utGet_mpAgent( ) { return mpAgent; }

};

#endif /* INCLUDE_THEORY_H_ */

