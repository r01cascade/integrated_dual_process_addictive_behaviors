#ifndef INCLUDE_THEORYMEDIATOR_H_
#define INCLUDE_THEORYMEDIATOR_H_

class Agent;
#include "Theory.h"
#include "DrinkingPlan.h"
#include <vector>

class TheoryMediator {

protected:
	std::vector<Theory*> mTheoryList;
	Agent *mpAgent;

	/* NEW ACTION MECH */
	//mediated Attitude, Norm, PBC
	double mProbOppIn;
	double mProbOppOut;

	double mAttutide;
	double mNorm;
	double mPbc;

public:
	TheoryMediator(std::vector<Theory*> theoryList);
	virtual ~TheoryMediator();
	void linkAgent(Agent *agent); //link agent to this mediator and all theories in the theory list

	template <typename derivedTheory>
	bool getTheory(derivedTheory** ppTheory) { //write a theory (from mTheoryList) with the matching type to derivedTheory, return true if success.
		for (std::vector<Theory*>::iterator it=mTheoryList.begin(); it!=mTheoryList.end(); ++it) {
				if (dynamic_cast<derivedTheory*>(*it)!=0) {
					*ppTheory = dynamic_cast<derivedTheory*>(*it);
					return true;
				}
			}
			return false;
	};

	virtual void mediateSituation() = 0;
	virtual void mediateAction() = 0;
	virtual void mediateGatewayDisposition() = 0;
	virtual void mediateNextDrinksDisposition() = 0;
	virtual void mediateNonDrinkingActions() = 0;
	
	/* NEW ACTION MECH */
	double getProbOppIn() {return mProbOppIn;}
	double getProbOppOut() {return mProbOppOut;}
	virtual void mediateThoughtPathway() = 0; //must be overrided to mediate Attitude, Norm, PBC
	DrinkingPlan getIntention();
// for unit test purposes only
//    TheoryMediator(){ }
    void utSet_mTheoryList( std::vector<Theory*> ut_mTheoryList)
        { mTheoryList = ut_mTheoryList; }

    void utSet_mpAgent( Agent* ut_mpAgent)
        { mpAgent = ut_mpAgent; }
//make test can't handle variables declared separated by commas.
    // void utSet_mProbOppOut( double mProbOppIn, ut_mProbOppOut)
    //     { mProbOppOut = ut_mProbOppOut; }

    // void utSet_mPbc[6]( double mAttutide[6], mNorm[6], ut_mPbc[6])
    //     { mPbc[6] = ut_mPbc[6]; }

    std::vector<Theory*> utGet_mTheoryList( ) { return mTheoryList; }

    Agent* utGet_mpAgent( ) { return mpAgent; }
    
//make test can't handle variables declared separated by commas.
    // double mProbOppIn, utGet_mProbOppOut( ) { return mProbOppOut; }

    // double mAttutide[6], mNorm[6], utGet_mPbc[6]( ) { return mPbc[6]; }

};

#endif /* INCLUDE_THEORYMEDIATOR_H_ */

