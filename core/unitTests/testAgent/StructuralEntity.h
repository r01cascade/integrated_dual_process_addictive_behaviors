#ifndef INCLUDE_STRUCTURALENTITY_H_
#define INCLUDE_STRUCTURALENTITY_H_

#include "Regulator.h"
#include "vector"

class StructuralEntity {

public:
	StructuralEntity(std::vector<Regulator*> regulatorList, std::vector<double> powerList, int transformationalInterval) {
		mpRegulatorList = regulatorList;
		mpPowerList = powerList;
		mTransformationalInterval = transformationalInterval;
	};

	virtual ~StructuralEntity() {
		for(std::vector<Regulator*>::iterator iter = mpRegulatorList.begin(); iter != mpRegulatorList.end(); iter++) {delete (*iter);}
	};

	virtual void doTransformation() = 0;

	int mTransformationalTriggerCount = 0; 
	
	
	std::vector<double> getPowerList(){return mpPowerList;}
	int getTransformationalInterval(){return mTransformationalInterval;}

	
	void setPowerList(std::vector<double> vdInput){mpPowerList=vdInput;}
	void setTransformationalInterval(int iInput){mTransformationalInterval=iInput;} 

public:
	std::vector<Regulator*> mpRegulatorList;
	std::vector<double> mpPowerList;

	int mTransformationalInterval;

// for unit test purposes only
//    StructuralEntity(){ }
    void utSet_mpRegulatorList( std::vector<Regulator*> ut_mpRegulatorList)
        { mpRegulatorList = ut_mpRegulatorList; }

    void utSet_mpPowerList( std::vector<double> ut_mpPowerList)
        { mpPowerList = ut_mpPowerList; }

    void utSet_mTransformationalInterval( int ut_mTransformationalInterval)
        { mTransformationalInterval = ut_mTransformationalInterval; }

    std::vector<Regulator*> utGet_mpRegulatorList( ) { return mpRegulatorList; }

    std::vector<double> utGet_mpPowerList( ) { return mpPowerList; }

    int utGet_mTransformationalInterval( ) { return mTransformationalInterval; }

};

#endif /* INCLUDE_STRUCTURALENTITY_H_ */

