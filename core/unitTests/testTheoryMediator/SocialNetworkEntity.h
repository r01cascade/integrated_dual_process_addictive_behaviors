#ifndef INCLUDE_SOCIAL_NETWORK_ENTITY_H_
#define INCLUDE_SOCIAL_NETWORK_ENTITY_H_

#include "StructuralEntity.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedNetwork.h"
#include "Agent.h"




class SocialNetworkEntity : public StructuralEntity {

public:
	repast::SharedContext<Agent> 										*mpContext;
	repast::SharedNetwork<Agent, repast::RepastEdge<Agent>,	
						  repast::RepastEdgeContent<Agent>, 
						  repast::RepastEdgeContentManager<Agent> > 	*mpNetwork;					  
	double																mAverageOutdegree;
	double																mProportionTiesReciprocated;
	double																mMedianGeodesicDistance;
	double																mTransitivity;
	double 																mNetworkDensity;
	double 																mMoransIDrink;
	double																mMoransISex;
	double																mMoransIAge;	

public:
	SocialNetworkEntity(std::vector<Regulator*> regulatorList, std::vector<double> powerList,
			int transformationalInterval, repast::SharedContext<Agent> *context);
	~SocialNetworkEntity();

	void doTransformation() override;
	
	void connectAgentNetwork();
	void warmUpAgentNetwork(int warmUpTime);	
	void connectAgents(Agent* ego);

	std::vector<Agent*> getSuccessors(Agent* ego);
	std::vector<Agent*> getPredecessors(Agent* ego);
	void addSocialEdge(Agent* ego, Agent* alter);
	void removeSocialEdge(Agent* ego, Agent* alter);
	std::vector<Agent*> generateAgentPotentialBuddyList(Agent* ego);
	std::vector<Agent*> updatePotentialBuddies(std::vector<Agent*> potentialBuddies, Agent* ego, Agent* alter);


	
	double getAverageOutdegree() 				{return mAverageOutdegree;}
	double getProportionTiesReciprocated()      {return mProportionTiesReciprocated;}
	double getMedianGeodesicDistance()          {return mMedianGeodesicDistance;}
	double getTransitivity() 					{return mTransitivity;}
	double getNetworkDensity()					{return mNetworkDensity;}
	double getMoransIDrink()					{return mMoransIDrink;}
	double getMoransISex()						{return mMoransISex;}
	double getMoransIAge()						{return mMoransIAge;}

	void calculateNetworkStats();

	double calculateAverageOutdegree();
	double calculateProportionTiesReciprocated();
	double calculateGeodesicDistance();
	double calculateTransitivityCoefficient();
	double calculateNetworkDensity(); 
	double calculateMoransIDrink(); 
	double calculateMoransISex();
	double calculateMoransIAge();

	
	void setAverageOutdegree(double dInput) 				{ mAverageOutdegree = dInput;}
	void setProportionTiesReciprocated(double dInput)     	{ mProportionTiesReciprocated = dInput;}
	void setMedianGeodesicDistance(double dInput)			{ mMedianGeodesicDistance = dInput;}
	void setTransitivity(double dInput)						{ mTransitivity = dInput;}
	void setNetworkDensity(double dInput)					{ mNetworkDensity = dInput;}
	void setMoransIDrink(double dInput)						{ mMoransIDrink = dInput;}
	void setMoransISex(double dInput)						{ mMoransISex = dInput;}
	void setMoransIAge(double dInput)						{ mMoransIAge = dInput;}
// for unit test purposes only
//    SocialNetworkEntity(){ }
    void utSet_mpContext( repast::SharedContext<Agent>* ut_mpContext)
        { mpContext = ut_mpContext; }

    // void utSet_repast::RepastEdge<Agent>,( repast::SharedNetwork<Agent, ut_repast::RepastEdge<Agent>,)
    //     { repast::RepastEdge<Agent>, = ut_repast::RepastEdge<Agent>,; }

    // void utSet_mpNetwork( repast::RepastEdgeContentManager<Agent> >* ut_mpNetwork)
    //     { mpNetwork = ut_mpNetwork; }

    void utSet_mAverageOutdegree( double ut_mAverageOutdegree)
        { mAverageOutdegree = ut_mAverageOutdegree; }

    void utSet_mProportionTiesReciprocated( double ut_mProportionTiesReciprocated)
        { mProportionTiesReciprocated = ut_mProportionTiesReciprocated; }

    void utSet_mMedianGeodesicDistance( double ut_mMedianGeodesicDistance)
        { mMedianGeodesicDistance = ut_mMedianGeodesicDistance; }

    void utSet_mTransitivity( double ut_mTransitivity)
        { mTransitivity = ut_mTransitivity; }

    void utSet_mNetworkDensity( double ut_mNetworkDensity)
        { mNetworkDensity = ut_mNetworkDensity; }

    void utSet_mMoransIDrink( double ut_mMoransIDrink)
        { mMoransIDrink = ut_mMoransIDrink; }

    void utSet_mMoransISex( double ut_mMoransISex)
        { mMoransISex = ut_mMoransISex; }

    void utSet_mMoransIAge( double ut_mMoransIAge)
        { mMoransIAge = ut_mMoransIAge; }

    repast::SharedContext<Agent>* utGet_mpContext( ) { return mpContext; }

    // repast::SharedNetwork<Agent, utGet_repast::RepastEdge<Agent>,( ) { return repast::RepastEdge<Agent>,; }

    // repast::RepastEdgeContentManager<Agent> >* utGet_mpNetwork( ) { return mpNetwork; }

    double utGet_mAverageOutdegree( ) { return mAverageOutdegree; }

    double utGet_mProportionTiesReciprocated( ) { return mProportionTiesReciprocated; }

    double utGet_mMedianGeodesicDistance( ) { return mMedianGeodesicDistance; }

    double utGet_mTransitivity( ) { return mTransitivity; }

    double utGet_mNetworkDensity( ) { return mNetworkDensity; }

    double utGet_mMoransIDrink( ) { return mMoransIDrink; }

    double utGet_mMoransISex( ) { return mMoransISex; }

    double utGet_mMoransIAge( ) { return mMoransIAge; }

};
#endif	
