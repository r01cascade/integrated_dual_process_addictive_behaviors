/* Agent.h */


#ifndef AGENT
#define AGENT

#include "globals.h"
#include "TheoryMediator.h"
#include "Theory.h"

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "SocialNetworkEntity.h"
#include "DrinkingPlan.h"

#include <cmath>

class SocialNetworkEntity;

enum class Opportunity {
    NONE=0,
    IN=1,
    OUT=2,
    TEST_ERROR=3
// for unit test purposes only
//    SocialNetworkEntity;(){ }
};

/* Agents */

class Agent {
	
public:
    repast::AgentId  mId;
    int              mAge;
    bool             mSex;
    std::string      mRace;
    bool       	     mIsDrinkingToday;
    int 			 mNumberDrinksToday;
    bool 			 mIs12MonthDrinker; 
    int              mDrinkFrequencyLevel;
    std::vector<int> mPastYearDrinks;
    int              mTotalDrinksPerAnnum;
    int              mMaritalStatus;
    int              mParenthoodStatus;
    int              mEmploymentStatus;
    double           mMeanDrinksToday;
    double           mSDDrinksToday;
    int              mDayOfRoleTransition;
    bool             mRoleChangingTransient;
    int              mRoleChangeStatus;
    int              mIncome;

    int             mYear;
    std::string     mMortalityHashKey;
    std::string     mMigrationHashKey;
    double          mMortality;
    double          mMigrationOutRate;

    TheoryMediator      *mpMediator;
    std::vector<double> mDispositions;

    void initDispositions(); 
    void doDisposition(); 
    void doDrinkingEngine(); 
    static int myrandom (int i); 

    int mPastYearN; 
    double mPastYearMeanDrink; 
    double mPastYearSquaredDistanceDrink; 
    void updateForwardMeanVariance(int addedValue);
    void updateBackwardMeanVariance(int removedValue);

    template<typename T>
    void shuffleList(std::vector<T>& elementList); 


    std::string makeMigrationOutHashKey(std::string year, std::string sex, std::string race, std::string age);
    std::string makeDeathRateHashKey(std::string year, std::string sex, std::string age);

    
    SocialNetworkEntity *mpSocialNetwork;
    std::vector<Agent*> mPotentialBuddies;
    std::vector<Agent*> mDrinkingBuddies;
    int selectIndexFromProbabilityVector(std::vector<double> probVector);

    /* NEW ACTION MECHANIMS */

    double mThresholdThoughtHabit;
    std::vector<DrinkingSchema> mPastYearSchemas;
    void initPastYearSchemas();
    Opportunity mTodayOpportunity;
    DrinkingSchema mTodaySchema;

public:
    Agent(repast::AgentId id);  
    Agent(repast::AgentId id, bool sex, int age, std::string race, int maritalStatus, int parenthoodStatus, int employmentStatus, int income, int drinking, int drinkFrequencyLevel, int monthlyDrinks, double monthlyDrinksSDPct, int year);
    virtual ~Agent(); 
	
    /* Required Getters */

    virtual repast::AgentId& getId(){                   return mId;    }
    virtual const repast::AgentId& getId() const{       return mId;    }
	
    /* Getters specific to this kind of Agent */

    int    getAge(){                                    return mAge;    }
    bool   getSex(){                                    return mSex;    }
    std::string getRace(){                              return mRace;   }
    bool   isDrinkingToday(){                        return mIsDrinkingToday;}
    int    getNumberDrinksToday(){						return mNumberDrinksToday;}
    bool   is12MonthDrinker() {						return mIs12MonthDrinker;}
    bool   isHaveKDrinksOverNDays(int numberOfDays, int kNumberDrinks);
    double getAvgDrinksNDays(int numberOfDays);
    double getAvgDrinksNDays(int numberOfDays, bool perOccasion);
    int    getNumDaysHavingKDrinksOverNDays(int numberOfDays, int kNumberDrinks);
    int    getTotalDrinksPerAnnum(){                    return mTotalDrinksPerAnnum;}
    int    getMaritalStatus(){                          return mMaritalStatus;}
    int    getParenthoodStatus(){                       return mParenthoodStatus;}
    int    getEmploymentStatus(){                       return mEmploymentStatus;}
    int    getIncome(){                                 return mIncome;}
    double getMeanDrinksToday(){                        return mMeanDrinksToday;}
    double getSDDrinksToday(){                          return mSDDrinksToday;}
    int    getDayOfRoleTransition(){                    return mDayOfRoleTransition;}
    int    getRoleChangeStatus(){                       return mRoleChangeStatus;}
    bool   getRoleChangedTransient(){                   return mRoleChangingTransient;}
    
    double getPastYearMeanDrink() {                     return mPastYearMeanDrink; }
    double getPastYearVarianceDrink() {                 return (mPastYearN<=1 ? 0 : mPastYearSquaredDistanceDrink / (mPastYearN)); }
    double getPastYearSdDrink() {                       return sqrt(getPastYearVarianceDrink()); }
    int    getDrinkFrequencyLevel(){                    return mDrinkFrequencyLevel;}
    std::vector<int> getPastYearDrinks(){               return mPastYearDrinks;     }
    std::vector<double> getDisposition(){               return mDispositions;       }
    int getPastYearN(){                                 return mPastYearN;}
    
    
    template <typename derivedTheory>
    bool getTheory(derivedTheory** ppTheory) {
    	mpMediator->getTheory(ppTheory);
    }
	
    
    double getMortalityRate(){                          return mMortality;}
    double getMigrationOutRate(){                       return mMigrationOutRate;}
    
    /* Setter */

    void set(int currentRank, int age, bool sex, bool currentDrinking, int currentQuantity);
    void set(int currentRank, int age, bool sex, bool currentDrinking, int currentQuantity, int currentFrequency,
             std::vector<int> pastYearDrinks);
    void setMediator(TheoryMediator *mediator);
    void setDispositionByIndex(int index, double value);
    void setMaritalStatus(int maritalStatus){mMaritalStatus = maritalStatus;}
    void setParenthoodStatus(int parenthoodStatus){mParenthoodStatus = parenthoodStatus;}
    void setEmploymentStatus(int employmentStatus){mEmploymentStatus = employmentStatus;}
    void setIncome(int income){mIncome = income;}
    void setRoleChangeStatus(int roleChangeStatus){mRoleChangeStatus = roleChangeStatus;}
    bool setRoleChangedTransient(bool roleChangedTransient){mRoleChangingTransient = roleChangedTransient;}

    void setDrinkFrequencyLevel(int iInput){mDrinkFrequencyLevel=iInput;}

    /* Situational mechanisms */

    void doSituation();

    /* Action mechanisms */

    void doAction();

    
    void ageAgent();

    void reset12MonthDrinker();
    void resetTotalDrinksPerAnnum();

    int findAgeGroup(); 

    void initPastYearDrinks(int monthlyDrinks, double monthlyDrinksSDPct);
    void updatePastYearDrinks();


    /*initialize potential friends override*/

    void initPotentialBuddies();
    void removeAgentFromPotentialBuddies(repast::AgentId agentId);

    void selectDrinkingBuddies(bool age, bool sex, bool race, bool isDrinkingToday, bool numberDrinksToday, bool is12MonthDrinker,  bool drinkFrequencyLevel, 
                            bool totalDrinksPerAnnum, bool maritalStatus, bool parenthoodStatus, bool employmentStatus, 
                            bool meanDrinksToday, bool sDDrinksToday, bool income, bool pastYearN, bool pastYearMeanDrink, 
                            bool outdegree, bool reciprocity, bool preferentialAttachment, bool transitiveTriples);
    std::vector<double> calculateSumEffectVector(std::vector<int> friendVector, std::vector<double> internalVector);
    std::vector<double> generateChoiceProbabilityVector(
                                                std::vector<double> totalAgeSimilarityVector, 
                                                std::vector<double> totalSexSimilarityVector, 
                                                std::vector<double> totalRaceSimilarityVector,
                                                std::vector<double> totalIsDrinkingTodaySimilarityVector, 
                                                std::vector<double> totalNumberDrinksTodaySimilarityVector,
                                                std::vector<double> totalIs12MonthDrinkersSimilarityVector, 
                                                std::vector<double> totalDrinkFrequencyLevelSimilarityVector,
                                                std::vector<double> totalTotalDrinksPerAnnumSimilarityVector, 
                                                std::vector<double> totalMaritalStatusSimilarityVector, 
                                                std::vector<double> totalParenthoodStatusSimilarityVector, 
                                                std::vector<double> totalEmploymentStatusSimilarityVector, 
                                                std::vector<double> totalMeanDrinksTodaySimilarityVector, 
                                                std::vector<double> totalSDDrinksTodaySimilarityVector, 
                                                std::vector<double> totalIncomeSimilarityVector,
                                                std::vector<double> totalPastYearNSimilarityVector, 
                                                std::vector<double> totalPastYearMeanDrinkSimilarityVector, 
                                                std::vector<double> totalReciprocityVector, 
                                                std::vector<double> totalOutdegreeVector, 
                                                std::vector<double> totalPreferentialAttachmentVector, 
                                                std::vector<double> totalTransitiveTriplesVector);  
    void changeFriendTie (int choiceIndex, std::vector<Agent*> peopleConsidered, std::vector<int> friendVector);

    void setSocialNetwork(SocialNetworkEntity *pSocialNetwork) {mpSocialNetwork = pSocialNetwork;}
    std::vector<Agent*> getDrinkingBuddies() {return mDrinkingBuddies;}

    /* NEW ACTION MECHANIMS */

    void generateOpportunity(double probOppIn, double probOppOut);
    DrinkingPlan getHabitualDrinkingPlan(int days);
    void doDrinking(DrinkingSchema schema); 
    int getDrinksFromSchema(DrinkingSchema schema);
    DrinkingSchema getSchemaFromDrinks(int drinks, double abstainRate);
// for unit test purposes only
//    Agent(){ }
    void utSet_mId( repast::AgentId ut_mId)
        { mId = ut_mId; }

    void utSet_mAge( int ut_mAge)
        { mAge = ut_mAge; }

    void utSet_mSex( bool ut_mSex)
        { mSex = ut_mSex; }

    void utSet_mRace( std::string ut_mRace)
        { mRace = ut_mRace; }

    void utSet_mIsDrinkingToday( bool ut_mIsDrinkingToday)
        { mIsDrinkingToday = ut_mIsDrinkingToday; }

    void utSet_mNumberDrinksToday( int ut_mNumberDrinksToday)
        { mNumberDrinksToday = ut_mNumberDrinksToday; }

    void utSet_mDrinkFrequencyLevel( int ut_mDrinkFrequencyLevel)
        { mDrinkFrequencyLevel = ut_mDrinkFrequencyLevel; }

    void utSet_mPastYearDrinks( std::vector<int> ut_mPastYearDrinks)
        { mPastYearDrinks = ut_mPastYearDrinks; }

    void utSet_mIncome( int ut_mIncome)
        { mIncome = ut_mIncome; }

    void utSet_mYear( int ut_mYear)
        { mYear = ut_mYear; }

    void utSet_mMortalityHashKey( std::string ut_mMortalityHashKey)
        { mMortalityHashKey = ut_mMortalityHashKey; }

    void utSet_mMigrationHashKey( std::string ut_mMigrationHashKey)
        { mMigrationHashKey = ut_mMigrationHashKey; }

    void utSet_mMortality( double ut_mMortality)
        { mMortality = ut_mMortality; }

    void utSet_mMigrationOutRate( double ut_mMigrationOutRate)
        { mMigrationOutRate = ut_mMigrationOutRate; }

    void utSet_mpMediator( TheoryMediator* ut_mpMediator)
        { mpMediator = ut_mpMediator; }

    void utSet_mDispositions( std::vector<double> ut_mDispositions)
        { mDispositions = ut_mDispositions; }

    void utSet_mpSocialNetwork( SocialNetworkEntity* ut_mpSocialNetwork)
        { mpSocialNetwork = ut_mpSocialNetwork; }

    void utSet_mPotentialBuddies( std::vector<Agent*> ut_mPotentialBuddies)
        { mPotentialBuddies = ut_mPotentialBuddies; }

    void utSet_mDrinkingBuddies( std::vector<Agent*> ut_mDrinkingBuddies)
        { mDrinkingBuddies = ut_mDrinkingBuddies; }

    void utSet_mThresholdThoughtHabit( double ut_mThresholdThoughtHabit)
        { mThresholdThoughtHabit = ut_mThresholdThoughtHabit; }

    void utSet_mPastYearSchemas( std::vector<DrinkingSchema> ut_mPastYearSchemas)
        { mPastYearSchemas = ut_mPastYearSchemas; }

    void utSet_mTodayOpportunity( Opportunity ut_mTodayOpportunity)
        { mTodayOpportunity = ut_mTodayOpportunity; }

    void utSet_mTodaySchema( DrinkingSchema ut_mTodaySchema)
        { mTodaySchema = ut_mTodaySchema; }

    repast::AgentId utGet_mId( ) { return mId; }

    int utGet_mAge( ) { return mAge; }

    bool utGet_mSex( ) { return mSex; }

    std::string utGet_mRace( ) { return mRace; }

    bool utGet_mIsDrinkingToday( ) { return mIsDrinkingToday; }

    int utGet_mNumberDrinksToday( ) { return mNumberDrinksToday; }

    int utGet_mDrinkFrequencyLevel( ) { return mDrinkFrequencyLevel; }

    std::vector<int> utGet_mPastYearDrinks( ) { return mPastYearDrinks; }

    int utGet_mIncome( ) { return mIncome; }

    int utGet_mYear( ) { return mYear; }

    std::string utGet_mMortalityHashKey( ) { return mMortalityHashKey; }

    std::string utGet_mMigrationHashKey( ) { return mMigrationHashKey; }

    double utGet_mMortality( ) { return mMortality; }

    double utGet_mMigrationOutRate( ) { return mMigrationOutRate; }

    TheoryMediator* utGet_mpMediator( ) { return mpMediator; }

    std::vector<double> utGet_mDispositions( ) { return mDispositions; }

    SocialNetworkEntity* utGet_mpSocialNetwork( ) { return mpSocialNetwork; }

    std::vector<Agent*> utGet_mPotentialBuddies( ) { return mPotentialBuddies; }

    std::vector<Agent*> utGet_mDrinkingBuddies( ) { return mDrinkingBuddies; }

    double utGet_mThresholdThoughtHabit( ) { return mThresholdThoughtHabit; }

    std::vector<DrinkingSchema> utGet_mPastYearSchemas( ) { return mPastYearSchemas; }

    Opportunity utGet_mTodayOpportunity( ) { return mTodayOpportunity; }

    DrinkingSchema utGet_mTodaySchema( ) { return mTodaySchema; }

};

/* Serializable Agent Package */

struct AgentPackage{
	
public:
    int     id;
    int     rank;
    int     type;
    int     currentRank;
    int     age;
    bool    sex;
    bool    isDrinkingToday; 
	int 	numberDrinksToday;
    int     drinkFrequencyLevel; 
    std::vector<int> pastYearDrinks;

    /* Constructors */

    AgentPackage(); 
    AgentPackage(int _id, int _rank, int _type, int _currentRank, int _age, bool _sex, bool _isDrinkingToday, int _numberDrinksToday);
	AgentPackage(int _id, int _rank, int _type, int _currentRank, int _age, bool _sex, bool _isDrinkingToday, 
                 int _numberDrinksToday, int _drinkFrequencyLevel, std::vector<int> _pastYearDrinks);
    

    /* For archive packaging */

    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        ar & age;
        ar & sex;
        ar & isDrinkingToday;
        ar & numberDrinksToday;
        ar & drinkFrequencyLevel;
        ar & pastYearDrinks;
    }

    void utSet_id( int ut_id)
        { id = ut_id; }

    void utSet_rank( int ut_rank)
        { rank = ut_rank; }

    void utSet_type( int ut_type)
        { type = ut_type; }

    void utSet_currentRank( int ut_currentRank)
        { currentRank = ut_currentRank; }

    void utSet_age( int ut_age)
        { age = ut_age; }

    void utSet_sex( bool ut_sex)
        { sex = ut_sex; }

    void utSet_numberDrinksToday( int ut_numberDrinksToday)
        { numberDrinksToday = ut_numberDrinksToday; }

    void utSet_pastYearDrinks( std::vector<int> ut_pastYearDrinks)
        { pastYearDrinks = ut_pastYearDrinks; }

    int utGet_id( ) { return id; }

    int utGet_rank( ) { return rank; }

    int utGet_type( ) { return type; }

    int utGet_currentRank( ) { return currentRank; }

    int utGet_age( ) { return age; }

    bool utGet_sex( ) { return sex; }

    int utGet_numberDrinksToday( ) { return numberDrinksToday; }

    std::vector<int> utGet_pastYearDrinks( ) { return pastYearDrinks; }

};


#endif
