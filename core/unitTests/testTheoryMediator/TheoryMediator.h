#ifndef INCLUDE_THEORYMEDIATOR_H_
#define INCLUDE_THEORYMEDIATOR_H_

class Agent;
#include "Theory.h"
#include "DrinkingPlan.h"
#include <vector>

class TheoryMediator {

public:
	std::vector<Theory*> mTheoryList;
	Agent *mpAgent;

	/* NEW ACTION MECH */

	
	double mProbOppIn;
	double mProbOppOut;

	double mAttitude;
	double mNorm;
	double mPbc;

public:
	TheoryMediator(std::vector<Theory*> theoryList);
	virtual ~TheoryMediator();
	void linkAgent(Agent *agent); 

	template <typename derivedTheory>
	bool getTheory(derivedTheory** ppTheory) { 
		for (std::vector<Theory*>::iterator it=mTheoryList.begin(); it!=mTheoryList.end(); ++it) {
				if (dynamic_cast<derivedTheory*>(*it)!=0) {
					*ppTheory = dynamic_cast<derivedTheory*>(*it);
					return true;
				}
			}
			return false;
	};

	// virtual void mediateSituation() = 0;
	// virtual void mediateAction() = 0;
	// virtual void mediateGatewayDisposition() = 0;
	// virtual void mediateNextDrinksDisposition() = 0;
	// virtual void mediateNonDrinkingActions() = 0;
	void mediateSituation();
	void mediateAction() ;
	void mediateGatewayDisposition() ;
	void mediateNextDrinksDisposition();
	void mediateNonDrinkingActions();

	
	/* NEW ACTION MECH */

	double getProbOppIn() {return mProbOppIn;}
	double getProbOppOut() {return mProbOppOut;}
	//virtual void mediateThoughtPathway() = 0; 
	//void mediateThoughtPathway(); 
	DrinkingPlan getIntention();
// for unit test purposes only
    TheoryMediator(){ }
    void utSet_mTheoryList( std::vector<Theory*> ut_mTheoryList)
        { mTheoryList = ut_mTheoryList; }

    void utSet_mpAgent( Agent* ut_mpAgent)
        { mpAgent = ut_mpAgent; }

    void utSet_mProbOppIn( double ut_mProbOppIn)
        { mProbOppIn = ut_mProbOppIn; }

    void utSet_mProbOppOut( double ut_mProbOppOut)
        { mProbOppOut = ut_mProbOppOut; }

    void utSet_mAttitude( double ut_mAttitude)
        { mAttitude = ut_mAttitude; }

    void utSet_mNorm( double ut_mNorm)
        { mNorm = ut_mNorm; }

    void utSet_mPbc( double ut_mPbc)
        { mPbc = ut_mPbc; }

    std::vector<Theory*> utGet_mTheoryList( ) { return mTheoryList; }

    Agent* utGet_mpAgent( ) { return mpAgent; }

    double utGet_mProbOppIn( ) { return mProbOppIn; }

    double utGet_mProbOppOut( ) { return mProbOppOut; }

    double utGet_mAttitude( ) { return mAttitude; }

    double utGet_mNorm( ) { return mNorm; }

    double utGet_mPbc( ) { return mPbc; }

};

#endif /* INCLUDE_THEORYMEDIATOR_H_ */

