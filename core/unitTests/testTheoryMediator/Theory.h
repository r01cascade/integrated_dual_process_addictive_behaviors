#ifndef INCLUDE_THEORY_H_
#define INCLUDE_THEORY_H_

class Agent; 

#include <utility>
#include "DrinkingPlan.h"

class Theory {

public:
	Agent *mpAgent;
	double mProbOppIn;
	double mProbOppOut;

public:
	virtual ~Theory() {};

	void setAgent(Agent *agent);
	virtual void doSituation() = 0;
	virtual void doGatewayDisposition() = 0;
	virtual void doNextDrinksDisposition() = 0;
	virtual void doNonDrinkingActions() = 0;

	
	std::pair<double, double> generateCorrectedMeanSD (double desiredMean, double desiredSd);
	std::pair<double, double> doLookup(double mean, double sd);
	std::pair<double, double> doFunctionLookup(double desiredMean, double desiredSd);

	/* NEW ACTION MECH */

	virtual double calcProbabilities() = 0;
	double getProbOppIn() {return mProbOppIn;}
	double getProbOppOut() {return mProbOppOut;}

	virtual double getAttitude() = 0;
	virtual double getNorm() = 0;
	virtual double getPerceivedBehaviourControl() = 0;
// for unit test purposes only
//    Theory(){ }
    void utSet_mpAgent( Agent* ut_mpAgent)
        { mpAgent = ut_mpAgent; }

    void utSet_mProbOppIn( double ut_mProbOppIn)
        { mProbOppIn = ut_mProbOppIn; }

    void utSet_mProbOppOut( double ut_mProbOppOut)
        { mProbOppOut = ut_mProbOppOut; }

    Agent* utGet_mpAgent( ) { return mpAgent; }

    double utGet_mProbOppIn( ) { return mProbOppIn; }

    double utGet_mProbOppOut( ) { return mProbOppOut; }

};

#endif /* INCLUDE_THEORY_H_ */

