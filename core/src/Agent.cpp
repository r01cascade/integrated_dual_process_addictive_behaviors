/* Agent.cpp */

#include "Agent.h"
#include "globals.h"
#include "repast_hpc/Random.h"
#include <numeric>
#include <string>

#include "NormTheory.h"
#include "DescriptiveNormEntity.h"
#include "InjunctiveNormEntity.h"
#include "NormGlobals.h"

//#include <boost/random.hpp>
//#include <boost/random/gamma_distribution.hpp>
#include <boost/math/distributions/gamma.hpp>

/* Serializable Agent Package Data */

//#define AGENTDEBUG
//#define UNIT_TEST

AgentPackage::AgentPackage(){ }

AgentPackage::AgentPackage(int _id, int _rank, int _type, int _currentRank, int _age, bool _sex, bool _isDrinkingToday, int _numberDrinksToday):
id(_id), rank(_rank), type(_type), currentRank(_currentRank), age(_age), sex(_sex), isDrinkingToday(_isDrinkingToday), 
numberDrinksToday(_numberDrinksToday){ }

AgentPackage::AgentPackage(int _id, int _rank, int _type, int _currentRank, int _age, bool _sex, bool _isDrinkingToday, 
	int _numberDrinksToday, int _drinkFrequencyLevel, std::vector<int> _pastYearDrinks):
id(_id), rank(_rank), type(_type), currentRank(_currentRank), age(_age), sex(_sex), isDrinkingToday(_isDrinkingToday), 
numberDrinksToday(_numberDrinksToday), drinkFrequencyLevel(_drinkFrequencyLevel), pastYearDrinks(_pastYearDrinks){ }

Agent::Agent(repast::AgentId id){
	// These are temporary parameterizations that will be replaced by the calibration process
	mId = id;
	mAge = int (repast::Random::instance()->nextDouble() * 100);
	mSex = bool (repast::Random::instance()->nextDouble() < 0.5 ? 1 : 0);
	mMaritalStatus = bool (repast::Random::instance()->nextDouble() < 0.5 ? 1 : 0);
	mParenthoodStatus = bool (repast::Random::instance()->nextDouble() < 0.5 ? 1 : 0);
	mEmploymentStatus = bool (repast::Random::instance()->nextDouble() < 0.5 ? 1 : 0);
	mIncome = int(100000 * repast::Random::instance()->nextDouble());
	mIsDrinkingToday = bool (repast::Random::instance()->nextDouble() < 0.5 ? 1 : 0);
	mNumberDrinksToday = 0;
	mIs12MonthDrinker = false;
	mDrinkFrequencyLevel = 1 + repast::Random::instance()->nextDouble() * MAX_DRINK_LEVEL;
	mTotalDrinksPerAnnum = 0;

	initDispositions();
}

Agent::Agent(repast::AgentId id, bool sex, int age, std::string race, int maritalStatus, int parenthoodstatus, int employmentStatus, int income,
			 int drinking, int drinkFrequencyLevel, int monthlyDrinks, double monthlyDrinksSDPct, int year, double traitImpulsivity, 
			 int annualFrequency, double gramsPerDay, int habiUpdateInterval) {
	mId = id;
	mSex = sex;
	mAge = age;
	mRace = race;
	mMaritalStatus = maritalStatus;
	mParenthoodStatus = parenthoodstatus;
	mEmploymentStatus = employmentStatus;
	mIncome = income;
	mIsDrinkingToday = drinking;
	mNumberDrinksToday = drinking;
	mIs12MonthDrinker = drinking;
	
	mDrinkFrequencyLevel = drinkFrequencyLevel;
	mBaselineMonthlyDrink = monthlyDrinks;

	mAnnualFrequency = annualFrequency;
	mGramsPerDay = gramsPerDay;

	mHabitUpdateInterval = habiUpdateInterval;
	
	//initPastYearDrinks(monthlyDrinks, monthlyDrinksSDPct);
	//initPastYearSchemas();
	initPastYearSchemasDrinks(initSchemaProfiles());
	
	initDispositions();
	
	mTotalDrinksPerAnnum = 0;
	mDayOfRoleTransition = repast::Random::instance()->nextDouble() * 364;
	mRoleChangingTransient = false;
	mRoleChangeStatus = 0;

//	std::cout << " mId " << mId<< " mMaritalStatus " << mMaritalStatus << " mParenthoodStatus " <<  mParenthoodStatus << " mEmploymentStatus " 
//	<< mEmploymentStatus << " mIncome " << mIncome << std::endl;
	mYear = year;
	std::string mortalityHashKey = makeDeathRateHashKey(std::to_string(mYear), std::to_string(mSex), std::to_string(mAge));
	std::string migrationHashKey =  makeMigrationOutHashKey(std::to_string(mYear), std::to_string(mSex), mRace, std::to_string(mAge));;
	mMortality = deathRateTable[mortalityHashKey];
	mMigrationOutRate = migrationOutTable[migrationHashKey];

	mTraitImpulsivity = traitImpulsivity;
}

void Agent::setMediator(TheoryMediator *mediator) {
	mpMediator = mediator;
	mpMediator->linkAgent(this);
	for (int i=0; i<NUM_SCHEMA; i++) {
		mpMediator->setIntentionProbability(i, mHabitProbability[i]);
	}

	if (AGENT_LEVEL_OUTPUT) {
		/*if (mId.id() <= 1000) {
			string filename = "./outputs/agent" + to_string(mId.id()) + ".csv";
			mpActionMechFile = fopen (filename.c_str(),"w");
			fprintf(mpActionMechFile,"id,tick,schema,path,drinks,desire1,desire2,desire3,desire4,desire5,habit1,habit2,habit3,habit4,habit5,intention1,intention2,intention3,intention4,intention5,des1,des2,des3,des4,des5,inj1,inj2,inj3,inj4,inj5\n");
		}*/
	}
}

Agent::~Agent(){
//	delete mpMediator;
	if (mpActionMechFile!=NULL) fclose(mpActionMechFile);
}

// Initialise vector of dispositions
void Agent::initDispositions(){
	std::vector<double> vect(MAX_DRINKS, 0.0);
	mDispositions = vect;
}

void Agent::set(int currentRank, int age, bool sex, bool currentDrinking,
				int currentQuantity, int currentFrequency, std::vector<int> pastYearDrinks){
    mId.currentRank(currentRank);
    mAge = age;
    mSex = sex;
    mIsDrinkingToday = currentDrinking;
    mNumberDrinksToday = currentQuantity;
    mDrinkFrequencyLevel = currentFrequency;
    mPastYearDrinks = pastYearDrinks;
}

void Agent::setDispositionByIndex(int index, double value) {
	mDispositions[index] = value;
}


// Apply situational mechanisms
void Agent::doSituation() {
	#ifndef UNIT_TEST
	mpMediator->mediateSituation();
	#endif
}

// Calculate disposition vector from theories
void Agent::doDisposition() {
	#ifndef UNIT_TEST
	mpMediator->mediateGatewayDisposition();
	mpMediator->mediateNextDrinksDisposition();
	#endif
}

// Use drinking engine to calculate the number of drinks
void Agent::doDrinkingEngine() {
    bool stillDrinking = true;
	int numberOfDrinks = 0;
	do {
		if(repast::Random::instance()->nextDouble() > mDispositions[numberOfDrinks]){
			stillDrinking = false;
		} else{
			numberOfDrinks++;
		}
	} while (stillDrinking == true && numberOfDrinks < MAX_DRINKS);
//	std::cout << "id: "<< getId().id() << " number drinks: " << numberOfDrinks << std::endl;
	mIsDrinkingToday = (numberOfDrinks > 0);
	mNumberDrinksToday = numberOfDrinks;
	
	mTotalDrinksPerAnnum += numberOfDrinks;
	#ifdef DEBUG
		if (mId.id() == IDFOROUTPUT){
			std::cout<<"mTotalDrinksPerAnnum = "<<mTotalDrinksPerAnnum<<std::endl;
			std::cout<<"mNumberDrinksToday = "<<mNumberDrinksToday<<std::endl;
		}
	#endif

	if (mIsDrinkingToday) {mIs12MonthDrinker = true;} //turn on the 12-month drinker flag if this agent drinks
}

// Apply action mechanisms
void Agent::doAction() {
	// OLD ACTION
	//doDisposition();
	//doDrinkingEngine();

	//NEW ACTION MECH
	int intTick = floor(repast::RepastProcess::instance()->getScheduleRunner().currentTick());
	if (intTick!=0 && intTick % mHabitUpdateInterval == 0)
		updateHabitProbabilities(mHabitUpdateInterval);

#if !defined(UNIT_TEST)
	mpMediator->mediateAction();
#endif

	generateOpportunity(mpMediator->getProbOppIn(), mpMediator->getProbOppOut());

	string chosenPath = "";
	if (flagDryJanuary) {
		chosenPath = "dry_january";
		doDrinking(DrinkingSchema::ABSTAIN);
	} else {
		if (mTodayOpportunity != Opportunity::NONE) {
			DrinkingPlan drinkingPlan;
			if (repast::Random::instance()->nextDouble() < mTraitImpulsivity) {
				chosenPath="habit";
				//drinkingPlan = getProportionalSelectionHabitualDrinkingPlan();
				drinkingPlan.schema = drawSchemaFromHabitBag();
				drinkingPlan.probability = 1;
			} else {
				chosenPath="intention";
				mpMediator->mediateAction();
				drinkingPlan = mpMediator->getProportionalSelectionIntentionPlan();
			}


			doDrinking(drinkingPlan.schema);
		} else {
			doDrinking(DrinkingSchema::ABSTAIN);
		}
	}
	
	/*double tick = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
	if (mId.id()==2 && tick>730) {
		printf("%d,%.0f,%d,%s,%d\n",mId.id(),tick,static_cast<int>(mTodaySchema),
			chosenPath.c_str(),mNumberDrinksToday);
	}*/

	if (AGENT_LEVEL_OUTPUT) {
		if (mpActionMechFile==NULL && 
			mId.id() <= 1000) {
			//chosenPath=="dry_january" && mSex==MALE && findAgeGroup()==3) {
			string filename = "./outputs/agent" + to_string(mId.id()) + ".csv";
			mpActionMechFile = fopen(filename.c_str(),"a");
			if (mpActionMechFile==NULL)
				printf("Warning: Failed to open %s.\tError:%s\n.",filename.c_str(),strerror(errno));
			else {
				printf("Open %s\n",filename.c_str());
				fprintf(mpActionMechFile,"id,tick,schema,path,drinks,desire1,desire2,desire3,desire4,desire5,habit1,habit2,habit3,habit4,habit5,intention1,intention2,intention3,intention4,intention5,des1,des2,des3,des4,des5,inj1,inj2,inj3,inj4,inj5\n");
			}
		}
		if (mpActionMechFile!=NULL) {
			updateHabitProbabilities(mHabitUpdateInterval);
			double tick = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
			fprintf(mpActionMechFile,"%d,%.0f,%d,%s,%d,",mId.id(),tick,static_cast<int>(mTodaySchema),chosenPath.c_str(),mNumberDrinksToday);
			
			NormTheory* pNormTheory;
			getTheory(&pNormTheory);
			double* desires = pNormTheory->getDesires();
			for (int i=0; i<NUM_SCHEMA; i++) {
				fprintf(mpActionMechFile,"%.4f,",desires[i]);
			}
			
			for (int i=0; i<NUM_SCHEMA; i++) {
				fprintf(mpActionMechFile,"%.4f,",mHabitProbability[i]);
			}
			for (int i=0; i<NUM_SCHEMA; i++) {
				fprintf(mpActionMechFile,"%.4f,",mpMediator->getIntentionProbability(i));
			}
			
			
			InjunctiveNormEntity* pInjNormEntity = pNormTheory->getInjNormEntity();
			DescriptiveNormEntity* pDesNormEntity = pNormTheory->getDesNormEntity();
			int groupId = P_REFERENCE_GROUP->getId(getSex(),findAgeGroup());
			double* tempPrevArr = pDesNormEntity->getAvgPrevalence(groupId);
			for (int i=0; i<NUM_SCHEMA; ++i) {
				fprintf(mpActionMechFile,"%.4f,",tempPrevArr[i]);
			}
			for (int i=0; i<NUM_SCHEMA; ++i) {
				fprintf(mpActionMechFile,"%.4f,",pInjNormEntity->getInjNormPrev(groupId,i));
			}
			
			fprintf(mpActionMechFile,"\n");
		}
	}

	updatePastYearDrinks();
#ifndef UNIT_TEST
	mpMediator->mediateNonDrinkingActions();
#endif

//	#ifdef DEBUG
//	std::cout << "Current quantity: " << mCurrentQuantity << std::endl;
//	#endif
/*	//added test code for doDrinkingEngine
	int myDrinks = doDrinkingEngine(drinkingValue);
	#ifdef DEBUG
	std::cout << "my deterministic drinking number: id " << mId << ", number drinks: " << myDrinks; 
	#endif
	myDrinks = doDrinkingEngine(drinkingValue, false);
	#ifdef DEBUG
	std::cout << "my probablistic drinking number: " << myDrinks << std::endl; 
	#endif
*/
}

// Age the agent by one year (assume all agents age at the same time)
void Agent::ageAgent(){
	mAge++;
	mYear++;
	std::string mortalityHashKey = makeDeathRateHashKey(std::to_string(mYear), std::to_string(mSex), std::to_string(mAge));
	std::string migrationHashKey =  makeMigrationOutHashKey(std::to_string(mYear), std::to_string(mSex), mRace, std::to_string(mAge));;
	mMortality = deathRateTable[mortalityHashKey];
	mMigrationOutRate = migrationOutTable[migrationHashKey]; 
}

void Agent::reset12MonthDrinker() {
	mIs12MonthDrinker = false;
}

void Agent::resetTotalDrinksPerAnnum(){
	mTotalDrinksPerAnnum = 0;
}

// method to identify age group of the agent ranging from 0 to NUM_AGE_GROUPS -1
int Agent::findAgeGroup() {
	int myAgeGroup = 0;

	if (mAge <= MAX_AGE) {
		while (mAge > AGE_GROUPS[myAgeGroup]) {
			myAgeGroup++;
		}
	} else {
		myAgeGroup = NUM_AGE_GROUPS - 1;
	}

	return myAgeGroup; // Age group 0 is the youngest age groups
}

//over the past "numberOfDays" days (inclusive) have you had at least "kNumberDrinks" drinks (inclusive)?
//example: Over the past 7 days have you ever had 5 or more drinks?
bool Agent::isHaveKDrinksOverNDays(int numberOfDays, int kNumberDrinks) {
	bool nDaysKDrinking = false;
	int index = mPastYearDrinks.size()-1;
	while (numberOfDays > 0) {
		if (mPastYearDrinks[index] >= kNumberDrinks){
			nDaysKDrinking = true;
		}
		index--;
		numberOfDays--;
	}
	return nDaysKDrinking;
}

//returns the average number of drinks consumed in the past n days (counting back from today).
double Agent::getAvgDrinksNDays(int numberOfDays){
	double averageDrinks = 0;
	int totalDays = numberOfDays;
	int index = mPastYearDrinks.size()-1;
	while (numberOfDays > 0) {
		averageDrinks = averageDrinks + mPastYearDrinks[index];
		index--;
		numberOfDays--;
	}
	return averageDrinks/totalDays;
}

double  Agent::getAvgDrinksNDays(int numberOfDays, bool perOccasion){
	double averageDrinks = 0;
	int index = mPastYearDrinks.size()-1;
	int totalDays;
	if (perOccasion == false) { //avg over all days
		totalDays = numberOfDays;
		while (numberOfDays > 0) {
			averageDrinks = averageDrinks + mPastYearDrinks[index];
			index--;
			numberOfDays--;
		}
	} else { //avg over only drinking days
		totalDays = 0;
		while (numberOfDays > 0) {
			if (mPastYearDrinks[index] > 0) {
				averageDrinks = averageDrinks + mPastYearDrinks[index];
				totalDays++;
			}
			index--;
			numberOfDays--;
		}

	}
	if (totalDays == 0){
		averageDrinks = 0;
	} else {
		averageDrinks = averageDrinks/totalDays;
	}

	return averageDrinks;
}


//over the past "numberOfDays" days (inclusive) on how many days did you have at least "kNumberDrinks" drinks (inclusive)?
//example: Over the past 30 days how many days did you have 5 or more drinks?
int Agent::getNumDaysHavingKDrinksOverNDays(int numberOfDays, int kNumberDrinks) {
	int countDays = 0;
	int index = mPastYearDrinks.size()-1;
	while (numberOfDays > 0) {
		if (mPastYearDrinks[index] >= kNumberDrinks){
			countDays++;
		}
		index--;
		numberOfDays--;
	}
	return countDays;
}


//shuffle a list using Fisher-Yates shuffle
template<typename T>
void Agent::shuffleList(std::vector<T>& elementList){
  if(elementList.size() <= 1) return;
  repast::DoubleUniformGenerator rnd = repast::Random::instance()->createUniDoubleGenerator(0, 1);
  T swap;
  for(int pos = elementList.size() - 1; pos > 0; pos--){
	  int range = pos + 1;
	  int other = (int)(rnd.next() * (range));
	  swap = elementList[pos];
	  elementList[pos] = elementList[other];
	  elementList[other] = swap;
  }
}

//generates and returns the mPastYearDrinks vector based on drink frequency data, monthly drink amount data,
//and a 10% std deviation, set by the model.props.
//TODO add a gigo detector. If the monthly frequency and monthly quantity don't make sense, there is a problem
//with the input file.
void Agent::initPastYearDrinks(int monthlyDrinks, double monthlyDrinksSDPct){
	double doubleMonthlyDrinks = double (monthlyDrinks);

	int pastYearDrinksLength; //number of non-negative entries in pastYearDrinks vector.  365 for everyday, 0 for abstinence
	std::vector<int> pastYearDrinks;

	double meanDrinksToday = 0;
	double drinksSD = 0;

	switch(mDrinkFrequencyLevel){
		case 1:
			if (mIs12MonthDrinker == false){
				pastYearDrinksLength = 0;
			}else {
				pastYearDrinksLength = 1 + repast::Random::instance()->nextDouble() * 10;
				mDrinkFrequencyLevel = 2;
			}
			break;
		case 2:
			pastYearDrinksLength = 11 + repast::Random::instance()->nextDouble() * 7;
			break;
		case 3:
			pastYearDrinksLength = 19 + repast::Random::instance()->nextDouble() * 11;
			break;
		case 4:
			pastYearDrinksLength = 31 + repast::Random::instance()->nextDouble() * 22;
			break;
		case 5:
			pastYearDrinksLength = 54 + repast::Random::instance()->nextDouble() * 117;
			break;
		case 6:
			pastYearDrinksLength = 172 + repast::Random::instance()->nextDouble() * 164;
			break;
		case 7:
			pastYearDrinksLength = 337 + repast::Random::instance()->nextDouble() * 28;
			break;
	}
	if (pastYearDrinksLength == 0){                                  //nondrinkers get a year of 0
		std::vector<int> pastYearDrinksTemp(365, 0);
		pastYearDrinks = pastYearDrinksTemp;
	}else if (mDrinkFrequencyLevel > 1 && doubleMonthlyDrinks == 0){ //0 past month drinks >0 past year drinking, 1 for all drinking days
		int fillers = 365 - pastYearDrinksLength;
		int counter = pastYearDrinksLength;
		std::vector<int> pastYearDrinksTemp(fillers, 0);
		pastYearDrinks = pastYearDrinksTemp;
		while (counter != 0){
			pastYearDrinks.push_back(1);
			--counter;
		}
	//	std::cout << "I drink VERY VERY little" << std::endl;
	}else{
		meanDrinksToday = doubleMonthlyDrinks / double(pastYearDrinksLength) * 12 ;
		drinksSD = meanDrinksToday * monthlyDrinksSDPct;
		if (meanDrinksToday < 1) //TUONG: add this to fix problem of high freq but low drink => sample 0 every day
			drinksSD = 1;
		repast::NormalGenerator normGen = repast::Random::instance()->createNormalGenerator(meanDrinksToday, drinksSD);
		int yearCounter = 0;
		int quantityCounter = 0;
		while (yearCounter != 365){
			if (pastYearDrinksLength == quantityCounter){
				pastYearDrinks.push_back(0);
			}else{
				double doubleDrinksToday = normGen.next();
				int numDrinksToday;
				if (doubleDrinksToday < 1){
					numDrinksToday = 1;
				}else{
					numDrinksToday = round(doubleDrinksToday);
				}
				pastYearDrinks.push_back(numDrinksToday);
				++quantityCounter;
			}
			++yearCounter;
		}
	}

	shuffleList(pastYearDrinks);
	mPastYearDrinks = pastYearDrinks;
	mMeanDrinksToday = meanDrinksToday;
	mSDDrinksToday = drinksSD;

	//calculate mean and variance from history using Welford method
	mPastYearN = 0;
	mPastYearMeanDrink = 0;
	mPastYearSquaredDistanceDrink = 0;
	double oldMean = 0;
	for (int k=1; k<=365; k++) { //length of past-year-drink vector must be 365
		int x = pastYearDrinks[k-1];
		if (x!=0) { //only account for drinking days (number of drinks > 0)
			mPastYearN++;
			oldMean = mPastYearMeanDrink;
			mPastYearMeanDrink = mPastYearMeanDrink + (x - mPastYearMeanDrink) / mPastYearN;
			mPastYearSquaredDistanceDrink = mPastYearSquaredDistanceDrink + (x - mPastYearMeanDrink)*(x-oldMean);
		}
	}
}

void Agent::updatePastYearDrinks(){
	/* update drinks vector */
	if (mIsDrinkingToday == true){
		mPastYearDrinks.push_back(mNumberDrinksToday);
		updateForwardMeanVariance(mNumberDrinksToday);
	} else {
		mPastYearDrinks.push_back(0);
	}

	if ( mPastYearDrinks.front() != 0 ) {
		updateBackwardMeanVariance(mPastYearDrinks.front());
	}

	mPastYearDrinks.erase(mPastYearDrinks.begin());

	/* update schema vector */
	mPastYearSchemas.push_back(mTodaySchema);
	mPastYearSchemas.erase(mPastYearSchemas.begin());
}

//forward update past-year mean and variance using Welford method
void Agent::updateForwardMeanVariance(int addedValue) {
	mPastYearN++;
	if (mPastYearN != 0) {
		double oldMean = mPastYearMeanDrink;
		mPastYearMeanDrink = mPastYearMeanDrink + (addedValue - mPastYearMeanDrink) / mPastYearN;
		mPastYearSquaredDistanceDrink = mPastYearSquaredDistanceDrink + (addedValue - oldMean)*(addedValue - mPastYearMeanDrink);
	} else {
		mPastYearMeanDrink = 0;
		mPastYearSquaredDistanceDrink = 0;
	}

	//limit estimated-mean between 0 and MAX_DRINKS
	if (mPastYearMeanDrink < 0) {mPastYearMeanDrink = 0;}
	if (mPastYearMeanDrink > MAX_DRINKS) {mPastYearMeanDrink = MAX_DRINKS;}
}

//backward update past-year mean and variance using Welford method
void Agent::updateBackwardMeanVariance(int removedValue) {
	mPastYearN--;
	if (mPastYearN != 0) {
		double oldMean = mPastYearMeanDrink;
		mPastYearMeanDrink = ((mPastYearN+1)*mPastYearMeanDrink - removedValue) / mPastYearN;
		mPastYearSquaredDistanceDrink = mPastYearSquaredDistanceDrink - (removedValue - oldMean)*(removedValue - mPastYearMeanDrink);
	} else {
		mPastYearMeanDrink = 0;
		mPastYearSquaredDistanceDrink = 0;
	}

	//limit estimated-mean between 0 and MAX_DRINKS
	if (mPastYearMeanDrink < 0) {mPastYearMeanDrink = 0;}
	if (mPastYearMeanDrink > MAX_DRINKS) {mPastYearMeanDrink = MAX_DRINKS;}
}

std::string Agent::makeMigrationOutHashKey(std::string year, std::string sex, std::string race, std::string age) {
	std::hash<std::string> tp_hash;
	std::string hash_index("");
	hash_index = year + sex + race + age;
	return std::to_string(tp_hash(hash_index));
}

std::string Agent::makeDeathRateHashKey(std::string year, std::string sex, std::string age) {
	std::hash<std::string> tp_hash;
	std::string hash_index("");
	hash_index = year + sex + age;
	return std::to_string(tp_hash(hash_index));
}


void Agent::initPotentialBuddies() {
	mDrinkingBuddies = mpSocialNetwork->getSuccessors(this);
	mPotentialBuddies = mpSocialNetwork->generateAgentPotentialBuddyList(this);
}


void Agent::removeAgentFromPotentialBuddies(repast::AgentId agentId) {
	for (std::vector<Agent*>::iterator it = mPotentialBuddies.begin(); it!= mPotentialBuddies.end(); ++it) {
		if ( (*it)->getId().hashcode() == agentId.hashcode() ) {
			mPotentialBuddies.erase((it));
			break;
		}
	}
}

//select drinking buddies takes as input a list of AGENT characteristics (any agent variable
//prepended with an "m" that is a DOUBLE, INT, BOOL, STRING) AND social network structure variables, and 
//performs friend selection based on those characteristics and social network structures. The 
//method is "generic" in that all possible characteristics are coded, and at run time only 
//the selected characteristics are
//calculated.  Betas for all characteristics must be defined before run time. If read in from file,
//they may be globals (if no agent level heterogeneity) or theory "m" variables.
//for all agent characteristics, selection is calculated using the "sum similarity" algorithm.
void Agent::selectDrinkingBuddies(bool age, 
											bool sex, 
											bool race, 
											bool isDrinkingToday, 
											bool numberDrinksToday, 
											bool is12MonthDrinker, 
											bool drinkFrequencyLevel, 
											bool totalDrinksPerAnnum, 
											bool maritalStatus, 
											bool parenthoodStatus, 
											bool employmentStatus, 
											bool meanDrinksToday, 
											bool sDDrinksToday, 
											bool income, 
											bool pastYearN, 
											bool pastYearMeanDrink, 
											bool outdegree, 
											bool reciprocity, 
											bool preferentialAttachment, 
											bool transitiveTriples){
	//std::cout << "entered method" << std::endl;
	std::vector<Agent*> potentialDrinkingBuddies = mPotentialBuddies;
	//std::cout << "mPotentialBuddies.size(): " << mPotentialBuddies.size() << std::endl;
	//std::cout << "mDrinkingBuddies.size(): " << mDrinkingBuddies.size() << std::endl;

	std::vector<Agent*> orderedEveryoneVector;
	orderedEveryoneVector.push_back(this);
	orderedEveryoneVector.insert(orderedEveryoneVector.end(), mDrinkingBuddies.begin(), mDrinkingBuddies.end());
	orderedEveryoneVector.insert(orderedEveryoneVector.end(), potentialDrinkingBuddies.begin(), potentialDrinkingBuddies.end());
	//std::cout << "orderedEveryoneVector.size(): " << orderedEveryoneVector.size() << std::endl;

	//friendVector stores 1 for each mDrinkingBuddies and 0 for each mPotentialBuddies
	std::vector<int> friendVector(orderedEveryoneVector.size(),0);
	//std::cout << "friendVector.size(): " << friendVector.size() << std::endl;
	int m = 0;
	while (m != mDrinkingBuddies.size() + 1){
		friendVector[m] = 1;
		m++;
	}
#ifdef AGENTDEBUG
	std::cout << "me: " << mId << ". friend vector: {";
	for (int n = 0; n != orderedEveryoneVector.size(); n++){
		std::cout << friendVector[n] << " ";
	}
	std::cout << "}" << std::endl;
#endif
	/* declaring all totalVectors and initializing to have 1 element = 0*/
	std::vector<double> totalAgeSimilarityVector(1,0);
	std::vector<double> totalSexSimilarityVector(1,0);
	std::vector<double> totalRaceSimilarityVector(1,0);
	std::vector<double> totalIsDrinkingTodaySimilarityVector(1,0);
	std::vector<double> totalNumberDrinksTodaySimilarityVector(1,0);
	std::vector<double> totalIs12MonthDrinkersSimilarityVector(1,0);
	std::vector<double> totalDrinkFrequencyLevelSimilarityVector(1,0);
	std::vector<double> totalTotalDrinksPerAnnumSimilarityVector(1,0);
	std::vector<double> totalMaritalStatusSimilarityVector(1,0);
	std::vector<double> totalParenthoodStatusSimilarityVector(1,0);
	std::vector<double> totalEmploymentStatusSimilarityVector(1,0);
	std::vector<double> totalMeanDrinksTodaySimilarityVector(1,0);
	std::vector<double> totalSDDrinksTodaySimilarityVector(1,0);
	std::vector<double> totalIncomeSimilarityVector(1,0);
	std::vector<double> totalPastYearNSimilarityVector(1,0);
	std::vector<double> totalPastYearMeanDrinkSimilarityVector(1,0);
	std::vector<double> totalReciprocityVector(1,0);
	std::vector<double> totalOutdegreeVector(1,0);
	std::vector<double> totalPreferentialAttachmentVector(1,0);
	std::vector<double> totalTransitiveTriplesVector(1,0);

	//Calculating all totalVectors ONLY for selected effects.
	if (age){
		std::vector<double> internalAgeSimilarityVector;
		internalAgeSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			internalAgeSimilarityVector.push_back(1 - (abs(mAge - double((*buddyIter)->getAge())) / AGE_RANGE));
		}
		totalAgeSimilarityVector = calculateSumEffectVector(friendVector, internalAgeSimilarityVector);
	}
	if (sex){
		std::vector<double> internalSexSimilarityVector;
		internalSexSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			double sexSame;
			if ((mSex - (*buddyIter)->getSex()) == 0){
				sexSame = 1;
			}else{
				sexSame = 0;
			}
			internalSexSimilarityVector.push_back(sexSame);
		}
		totalSexSimilarityVector = calculateSumEffectVector(friendVector, internalSexSimilarityVector);
	}
	if (race){
		std::vector<double> internalRaceSimilarityVector;
		internalRaceSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			double raceSame;
			if ((mRace == (*buddyIter)->getRace())){
				raceSame = 1;
			}else{
				raceSame = 0;
			}
			internalRaceSimilarityVector.push_back(raceSame);
		}
		totalRaceSimilarityVector = calculateSumEffectVector(friendVector, internalRaceSimilarityVector);
	}
	if(isDrinkingToday){
		std::vector<double> internalIsDrinkingTodaySimilarityVector;
		internalIsDrinkingTodaySimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			double isDrinkingTodaySame;
			if ((mIsDrinkingToday - (*buddyIter)->isDrinkingToday()) == 0){
				isDrinkingTodaySame = 1;
			}else{
				isDrinkingTodaySame = 0;
			}
			internalIsDrinkingTodaySimilarityVector.push_back(isDrinkingTodaySame);
		}
		totalIsDrinkingTodaySimilarityVector = calculateSumEffectVector(friendVector, internalIsDrinkingTodaySimilarityVector);
	}
	if(numberDrinksToday){
		std::vector<double> internalNumberDrinksTodaySimilarityVector;
		internalNumberDrinksTodaySimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			internalNumberDrinksTodaySimilarityVector.push_back(1 - (abs(mNumberDrinksToday - double((*buddyIter)->getNumberDrinksToday())) / MAX_DRINKS));
		}
		totalNumberDrinksTodaySimilarityVector = calculateSumEffectVector(friendVector, internalNumberDrinksTodaySimilarityVector);
	}
	if(is12MonthDrinker){
		std::vector<double> internalIs12MonthDrinkerSimilarityVector;
		internalIs12MonthDrinkerSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			double is12MonthDrinkerSame;
			if ((mIs12MonthDrinker - (*buddyIter)->is12MonthDrinker()) == 0){
				is12MonthDrinkerSame = 1;
			}else{
				is12MonthDrinkerSame = 0;
			}
			internalIs12MonthDrinkerSimilarityVector.push_back(is12MonthDrinkerSame);
		}
		totalIs12MonthDrinkersSimilarityVector = calculateSumEffectVector(friendVector, internalIs12MonthDrinkerSimilarityVector);
	}
	if(drinkFrequencyLevel){	
		std::vector<double> internalDrinkFrequencyLevelSimilarityVector;
		internalDrinkFrequencyLevelSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			internalDrinkFrequencyLevelSimilarityVector.push_back(1 - (abs(mDrinkFrequencyLevel - double((*buddyIter)->getDrinkFrequencyLevel())) / DRINK_RANGE));
		}
		totalDrinkFrequencyLevelSimilarityVector = calculateSumEffectVector(friendVector, internalDrinkFrequencyLevelSimilarityVector);
	}
	if(totalDrinksPerAnnum){	
		std::vector<double> internalTotalDrinksPerAnnumSimilarityVector;
		internalTotalDrinksPerAnnumSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			internalTotalDrinksPerAnnumSimilarityVector.push_back(1 - (abs(mTotalDrinksPerAnnum - double((*buddyIter)->getTotalDrinksPerAnnum())) / MAX_DRINKS * 365));
		}
		totalTotalDrinksPerAnnumSimilarityVector = calculateSumEffectVector(friendVector, internalTotalDrinksPerAnnumSimilarityVector);
	}
	if(maritalStatus){
		std::vector<double> internalMaritalStatusSimilarityVector;
		internalMaritalStatusSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			double maritalStatusSame;
			if ((mMaritalStatus - (*buddyIter)->getMaritalStatus()) == 0){
				maritalStatusSame = 1;
			}else{
				maritalStatusSame = 0;
			}
			internalMaritalStatusSimilarityVector.push_back(maritalStatusSame);
		}
		totalMaritalStatusSimilarityVector = calculateSumEffectVector(friendVector, internalMaritalStatusSimilarityVector);
	}
	if(parenthoodStatus){
		std::vector<double> internalParenthoodStatusSimilarityVector;
		internalParenthoodStatusSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			double parenthoodStatusSame;
			if ((mParenthoodStatus - (*buddyIter)->getParenthoodStatus()) == 0){
				parenthoodStatusSame = 1;
			}else{
				parenthoodStatusSame = 0;
			}
			internalParenthoodStatusSimilarityVector.push_back(parenthoodStatusSame);
		}
		totalParenthoodStatusSimilarityVector = calculateSumEffectVector(friendVector, internalParenthoodStatusSimilarityVector);
	}
	if(employmentStatus){
		std::vector<double> internalEmploymentStatusSimilarityVector;
		internalEmploymentStatusSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			double employmentStatusSame;
			if ((mEmploymentStatus - (*buddyIter)->getEmploymentStatus()) == 0){
				employmentStatusSame = 1;
			}else{
				employmentStatusSame = 0;
			}
			internalEmploymentStatusSimilarityVector.push_back(employmentStatusSame);
		}
		totalEmploymentStatusSimilarityVector = calculateSumEffectVector(friendVector, internalEmploymentStatusSimilarityVector);
	}
	if(meanDrinksToday){	
		std::vector<double> internalMeanDrinksTodaySimilarityVector;
		internalMeanDrinksTodaySimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			internalMeanDrinksTodaySimilarityVector.push_back(1 - (abs(mMeanDrinksToday - double((*buddyIter)->getMeanDrinksToday())) / MAX_DRINKS));
		}
		totalMeanDrinksTodaySimilarityVector = calculateSumEffectVector(friendVector, internalMeanDrinksTodaySimilarityVector);
	}
	if(sDDrinksToday){	
		std::vector<double> internalSDDrinksTodaySimilarityVector;
		internalSDDrinksTodaySimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			internalSDDrinksTodaySimilarityVector.push_back(1 - (abs(mSDDrinksToday - double((*buddyIter)->getSDDrinksToday())) / (MAX_DRINKS/2)));
		}
		totalSDDrinksTodaySimilarityVector = calculateSumEffectVector(friendVector, internalSDDrinksTodaySimilarityVector);
	}
	if(income){
		std::vector<double> internalIncomeSimilarityVector;
		internalIncomeSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			internalIncomeSimilarityVector.push_back(1 - (abs(mIncome - double((*buddyIter)->getIncome())) / MAX_INCOME));
		}
		totalIncomeSimilarityVector = calculateSumEffectVector(friendVector, internalIncomeSimilarityVector);
	}
	if (pastYearN){	
		std::vector<double> internalPastYearNSimilarityVector;
		internalPastYearNSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			internalPastYearNSimilarityVector.push_back(1 - (abs(mPastYearN - double((*buddyIter)->getPastYearN())) / 365));
		}
		totalPastYearNSimilarityVector = calculateSumEffectVector(friendVector, internalPastYearNSimilarityVector);
	}
	if(pastYearMeanDrink){	
		std::vector<double> internalPastYearMeanDrinkSimilarityVector;
		internalPastYearMeanDrinkSimilarityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			internalPastYearMeanDrinkSimilarityVector.push_back(1 - (abs(mPastYearMeanDrink - double((*buddyIter)->getPastYearMeanDrink())) / MAX_DRINKS));
		}
		totalPastYearMeanDrinkSimilarityVector = calculateSumEffectVector(friendVector, internalPastYearMeanDrinkSimilarityVector);
	}
	if(outdegree){
		std::vector<double> internalOutdegreeVector(orderedEveryoneVector.size(),1);
		internalOutdegreeVector[0] = 0;
		totalOutdegreeVector = calculateSumEffectVector(friendVector, internalOutdegreeVector);
	}
	if(reciprocity){
		std::vector<Agent*> myInLinks = mpSocialNetwork->getPredecessors(this);
		std::vector<double> internalReciprocityVector;
		internalReciprocityVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			double isInLink;
			if (std::find(myInLinks.begin(), myInLinks.end(), *buddyIter) !=myInLinks.end() == true){
				isInLink = 1;
			} else {
				isInLink = 0;
			}
			internalReciprocityVector.push_back(isInLink);
		}
		totalReciprocityVector = calculateSumEffectVector(friendVector, internalReciprocityVector);
	}
	if(preferentialAttachment){
		std::vector<double> internalPreferentialAttachmentVector;
		internalPreferentialAttachmentVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			internalPreferentialAttachmentVector.push_back(sqrt(mpSocialNetwork->getPredecessors(*buddyIter).size()));
		}
		totalPreferentialAttachmentVector = calculateSumEffectVector(friendVector, internalPreferentialAttachmentVector);
	}
	if(transitiveTriples){
		std::vector<double> internalTransitiveTriplesVector;
		internalTransitiveTriplesVector.push_back(0);
		for (std::vector<Agent*>::const_iterator buddyIter = orderedEveryoneVector.begin(); buddyIter != orderedEveryoneVector.end(); buddyIter++){
			std::vector<Agent*> friendInLinks = mpSocialNetwork->getPredecessors(*buddyIter);
			double sumFriendOfFriend = 0.0;
			for (std::vector<Agent*>::const_iterator it = friendInLinks.begin(); it != friendInLinks.end(); ++it){
#ifdef AGENTDEBUG
//std::cout  << "doing friend of friend bit ";
#endif
				if (*it == this){
#ifdef AGENTDEBUG
//std::cout  << "\n" << (*it)->getId().id() << " is me: so I'll do nothing ";
#endif
				}else{
#ifdef AGENTDEBUG
//std::cout  << (*it)->getId() << " is my (potential) friend's friend. ";
#endif
					if (std::find(mDrinkingBuddies.begin(), mDrinkingBuddies.end(), *it) != mDrinkingBuddies.end()){
#ifdef AGENTDEBUG
//std::cout  << (*it)->getId().id() << " and I " << mId.id() << " are ALSO friends" << "\n";
#endif
						++sumFriendOfFriend;
					}
				}
			}
			internalTransitiveTriplesVector.push_back(sumFriendOfFriend);
		}
		totalTransitiveTriplesVector = calculateSumEffectVector(friendVector, internalTransitiveTriplesVector);
	}

	#ifdef AGENTDEBUG
	std::cout  << "age similarity vector: ";
	for (std::vector<double>::const_iterator it = totalAgeSimilarityVector.begin(); it != totalAgeSimilarityVector.end(); ++it){
		std::cout  << *it << " ";
	}
	std::cout  << "\n";
	std::cout  << "sex Similarity vector: ";
	for (std::vector<double>::const_iterator it = totalSexSimilarityVector.begin(); it != totalSexSimilarityVector.end(); ++it){
		std::cout  << *it << " ";
	}
	std::cout  << "\n";

	std::cout  << "\n" << "drinking Frequency vector: " ;
	for (std::vector<double>::const_iterator it = totalDrinkFrequencyLevelSimilarityVector.begin(); it !=totalDrinkFrequencyLevelSimilarityVector.end(); ++it){
		std::cout  << *it << " ";
	}
	std::cout  << "\n";

	std::cout  << "\n" << "drinking rangeQuantityDrinking vector: " ;
	for (std::vector<double>::const_iterator it = totalPastYearMeanDrinkSimilarityVector.begin(); it != totalPastYearMeanDrinkSimilarityVector.end(); ++it){
		std::cout  << *it << " ";
	}
	std::cout  << "\n";

	std::cout  << "outdegree vector: " ;
	for (std::vector<double>::const_iterator it = totalOutdegreeVector.begin(); it != totalOutdegreeVector.end(); ++it){
		std::cout  << *it << " ";
	}
	std::cout  << "\n";

	std::cout  << "reciprocity vector: " ;
	for (std::vector<double>::const_iterator it = totalReciprocityVector.begin(); it != totalReciprocityVector.end(); ++it){
		std::cout  << *it << " ";
	}
	std::cout  << "\n";

	std::cout  << "preferential attachement vector: ";
	for (std::vector<double>::const_iterator it = totalPreferentialAttachmentVector.begin(); it != totalPreferentialAttachmentVector.end(); ++it){
		std::cout  << *it << " ";
	}
	std::cout  << "\n";
	std::cout  << "transitive triplets vector: ";
	for (std::vector<double>::const_iterator it = totalTransitiveTriplesVector.begin(); it != totalTransitiveTriplesVector.end(); ++it){
		std::cout  << *it << " ";
	}
	std::cout  << "\n";
	
	#endif

	//Generate friend selection choice probability vector 	
	std::vector<double> choiceProbabilities = generateChoiceProbabilityVector(
												totalAgeSimilarityVector, totalSexSimilarityVector, totalRaceSimilarityVector,
												totalIsDrinkingTodaySimilarityVector, totalNumberDrinksTodaySimilarityVector,
												totalIs12MonthDrinkersSimilarityVector, totalDrinkFrequencyLevelSimilarityVector,
												totalTotalDrinksPerAnnumSimilarityVector, totalMaritalStatusSimilarityVector, 
												totalParenthoodStatusSimilarityVector, totalEmploymentStatusSimilarityVector, 
												totalMeanDrinksTodaySimilarityVector, totalSDDrinksTodaySimilarityVector, totalIncomeSimilarityVector,
												totalPastYearNSimilarityVector, totalPastYearMeanDrinkSimilarityVector, totalReciprocityVector, 
												totalOutdegreeVector, totalPreferentialAttachmentVector, totalTransitiveTriplesVector);

#ifdef AGENTDEBUG
	std::cout  << "choiceProbabilities vector ";
	for (std::vector<double>::const_iterator it = choiceProbabilities.begin(); it != choiceProbabilities.end(); ++it){
		std::cout  << *it << " ";
	}
	std::cout  << "\n";
#endif

	//Select which agent to do something with based on choice probabilities
	int choiceIndex = selectIndexFromProbabilityVector(choiceProbabilities);
	#ifdef AGENTDEBUG
	std::cout  << "index " << choiceIndex << "\n";
	#endif

	// Make or break tie with chosen agent
	changeFriendTie(choiceIndex, orderedEveryoneVector, friendVector);
}

std::vector<double> Agent::calculateSumEffectVector(std::vector<int> friendVector, std::vector<double> internalVector){
	std::vector<int> friendCopy = friendVector;
	std::vector<double> totalVector;
	int index = 0;
	//change objective function for changing a friend by flipping value of x_ij in the friendCopy vector
	for(std::vector<int>::const_iterator iter = friendVector.begin(); iter != friendVector.end(); ++iter){
		if (*iter == 0){
			friendCopy[index] = 1;
		}else{
			friendCopy[index] = 0;
		}

		double currentSum = std::inner_product(std::begin(friendCopy), std::end(friendCopy),
										std::begin(internalVector), 0.0);
		totalVector.push_back(currentSum);
		
		++index;
		friendCopy = friendVector;
	}
	return totalVector;
}

std::vector<double> Agent::generateChoiceProbabilityVector(
												std::vector<double> totalAgeSimilarityVector, 
												std::vector<double> totalSexSimilarityVector, 
												std::vector<double> totalRaceSimilarityVector,
												std::vector<double> totalIsDrinkingTodaySimilarityVector, 
												std::vector<double> totalNumberDrinksTodaySimilarityVector,
												std::vector<double> totalIs12MonthDrinkersSimilarityVector, 
												std::vector<double> totalDrinkFrequencyLevelSimilarityVector,
												std::vector<double> totalTotalDrinksPerAnnumSimilarityVector, 
												std::vector<double> totalMaritalStatusSimilarityVector, 
												std::vector<double> totalParenthoodStatusSimilarityVector, 
												std::vector<double> totalEmploymentStatusSimilarityVector, 
												std::vector<double> totalMeanDrinksTodaySimilarityVector, 
												std::vector<double> totalSDDrinksTodaySimilarityVector, 
												std::vector<double> totalIncomeSimilarityVector,
												std::vector<double> totalPastYearNSimilarityVector, 
												std::vector<double> totalPastYearMeanDrinkSimilarityVector, 
												std::vector<double> totalReciprocityVector, 
												std::vector<double> totalOutdegreeVector, 
												std::vector<double> totalPreferentialAttachmentVector, 
												std::vector<double> totalTransitiveTriplesVector){
	std::vector<double> choiceProbs;
	std::vector<double> numerators;
	double numerator = 0.0;
	double denominator = 0.0;
	int vectorLength = 1 + mDrinkingBuddies.size() + mPotentialBuddies.size(); //not sure which of the vector inputs will be "on"
	double age[vectorLength] = {};
	double sex[vectorLength] = {};
	double race[vectorLength] = {};
	double isDrinkingToday[vectorLength] = {};
	double numberDrinksToday[vectorLength] = {};
	double is12MonthDrinker[vectorLength] = {};
	double drinkFrequencyLevel[vectorLength] = {};
	double totalDrinksPerAnnum[vectorLength] = {};
	double maritalStatus[vectorLength] = {};
	double parenthoodStatus[vectorLength] = {};
	double employmentStatus[vectorLength] = {};
	double meanDrinksToday[vectorLength] = {};
	double sDDrinksToday[vectorLength] = {};
	double income[vectorLength] = {};
	double pastYearN[vectorLength] = {};
	double pastYearMeanDrink[vectorLength] = {};
	double reciprocity[vectorLength] = {};
	double outdegree[vectorLength] = {};
	double preferentialAttachment[vectorLength] = {};
	double transitiveTriples[vectorLength] = {};

	if(totalAgeSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			age[i] = AGE_SIMILARITY_BETA * totalAgeSimilarityVector[i];
		}
	}
	if(totalSexSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			sex[i] = SEX_SIMILARITY_BETA * totalSexSimilarityVector[i];
		}
	}
	if(totalRaceSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			race[i] = RACE_SIMILARITY_BETA * totalRaceSimilarityVector[i];
		}
	}
	if(totalIsDrinkingTodaySimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			isDrinkingToday[i] = IS_DRINKING_TODAY_SIMILARITY_BETA * totalIsDrinkingTodaySimilarityVector[i];
		}
	}
	if(totalNumberDrinksTodaySimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			numberDrinksToday[i] = NUMBER_DRINKS_TODAY_SIMILARITY_BETA * totalNumberDrinksTodaySimilarityVector[i];
		}
	}
	if(totalIs12MonthDrinkersSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			is12MonthDrinker[i] = IS_12_MONTH_DRINKERS_SIMILARITY_BETA * totalIs12MonthDrinkersSimilarityVector[i];
		}
	}
	if(totalDrinkFrequencyLevelSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			drinkFrequencyLevel[i] = SELECTION_FREQUENCY_BETA * totalDrinkFrequencyLevelSimilarityVector[i];
		}
	}
	if(totalTotalDrinksPerAnnumSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			totalDrinksPerAnnum[i] = TOTAL_DRINKS_PER_ANNUM_SIMILARITY_BETA * totalTotalDrinksPerAnnumSimilarityVector[i];
		}
	}
	if(totalMaritalStatusSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			maritalStatus[i] = MARITAL_STATUS_SIMILARITY_BETA * totalMaritalStatusSimilarityVector[i];
		}
	}
	if(totalParenthoodStatusSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			parenthoodStatus[i] = PARENTHOOD_STATUS_SIMILARITY_BETA * totalParenthoodStatusSimilarityVector[i];
		}
	}
	if(totalEmploymentStatusSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			employmentStatus[i] = EMPLOYMENT_STATUS_SIMILARITY_BETA * totalEmploymentStatusSimilarityVector[i];
		}
	}
	if(totalMeanDrinksTodaySimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			meanDrinksToday[i] = MEAN_DRINKS_TODAY_SIMILARITY_BETA * totalMeanDrinksTodaySimilarityVector[i];
		}
	}
	if(totalSDDrinksTodaySimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			sDDrinksToday[i] = SD_DRINKS_TODAY_SIMILARITY_BETA * totalSDDrinksTodaySimilarityVector[i];
		}
	}
	if(totalIncomeSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			income[i] = INCOME_SIMILARITY_BETA * totalIncomeSimilarityVector[i];
		}
	}
	if(totalPastYearNSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			pastYearN[i] = PAST_YEAR_N_SIMILARITY_BETA * totalPastYearNSimilarityVector[i];
		}
	}
	if(totalPastYearMeanDrinkSimilarityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			pastYearMeanDrink[i] = SELECTION_QUANTITY_BETA * totalPastYearMeanDrinkSimilarityVector[i];
		}
	}
	if(totalReciprocityVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			reciprocity[i] = RECIPROCITY_BETA * totalReciprocityVector[i];
		}
	}
	if(totalOutdegreeVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			outdegree[i] = OUTDEGREE_BETA * totalOutdegreeVector[i];
		}
	}
	if(totalPreferentialAttachmentVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			preferentialAttachment[i] = PREFERENTIAL_ATTACHMENT_BETA * totalPreferentialAttachmentVector[i];
		}
	}
	if(totalTransitiveTriplesVector.size() > 1){
		for (int i = 0; i != vectorLength; ++i){
			transitiveTriples[i]= TRANSITIVE_TRIPLES_BETA * totalTransitiveTriplesVector[i];
		}
	}

	for (int i = 0; i != vectorLength; ++i){	

		numerator = exp(age[i]
		  				+ sex[i]
		  				+ race[i]
		  				+ isDrinkingToday[i]
						+ numberDrinksToday[i]
						+ is12MonthDrinker[i]
						+ drinkFrequencyLevel[i]
						+ totalDrinksPerAnnum[i]
						+ maritalStatus[i]
						+ parenthoodStatus[i]
						+ employmentStatus[i]
						+ meanDrinksToday[i]
						+ sDDrinksToday[i]
						+ income[i]
						+ pastYearN[i]
						+ pastYearMeanDrink[i]
						+ reciprocity[i]
						+ outdegree[i]
						+ preferentialAttachment[i]
						+ transitiveTriples[i]);
		
		#ifdef AGENTDEBUG
		
		std::cout  << "i: " << i <<
		". age " << age[i] << 
		". sex " << sex[i] <<
		". race " << race[i]<< 
		". isDrinking " << isDrinkingToday[i]<<
		". numberDrinksToday " << numberDrinksToday[i]<<
		".  is12MonthDrinker " << is12MonthDrinker[i] << 
		". drinkFrequencyLevel " << drinkFrequencyLevel[i] <<
		".  totalDrinksPerAnnum " << totalDrinksPerAnnum[i] << 
		".  maritalStatus " << maritalStatus[i] << 
		".  parenthoodStatus " << parenthoodStatus[i] << 
		".  employmentStatus " << employmentStatus[i] << 
		".  meanDrinksToday " << meanDrinksToday[i] << 
		".  sDDrinksToday " << sDDrinksToday[i] << 
		".  income " << income[i] << 
		".  pastYearN " << pastYearN[i] << 
		".  pastYearMeanDrink " << pastYearMeanDrink[i] << 
		".  reciprocity " << reciprocity[i] << 
		".  outdegree " << outdegree[i] << 
		".  preferentialAttachment " << preferentialAttachment[i] << 
		".  transitiveTriples " << transitiveTriples[i] << std::endl;
		
		#endif

		if (std::isinf(numerator)) {
			std::cerr << "ContagionTheory::generateChoiceProbabilityVector. Numerator is infinity." << "\n";
			throw MPI::Exception(MPI::ERR_ARG);
		}
		
		numerators.push_back(numerator);
		denominator += numerator;
		#ifdef AGENTDEBUG
		std::cout << "numerator: "<< numerator << std::endl; 
		#endif
	}
	double accumulator = 0.0;
	#ifdef AGENTDEBUG
	if (mId.id() == 15){
	std::cout  << "\n" << "choice Probabilities: ";
	}
	#endif
	for (std::vector<double>::const_iterator it = numerators.begin(); it != numerators.end(); ++it){
		accumulator += double ((*it)/denominator);
		choiceProbs.push_back(accumulator);
		#ifdef AGENTDEBUG
		if (mId.id() == 15){
		std::cout  << accumulator << " ";
		}
		#endif
	}
	#ifdef AGENTDEBUG
	if (mId.id() == 15){
	std::cout  << "\n";
	}	
	#endif
	return choiceProbs;
}

void Agent::changeFriendTie(int choiceIndex, std::vector<Agent*> peopleConsidered, 
									  std::vector<int> friendVector){
	int action = 0;
	Agent* changeAgent = peopleConsidered[choiceIndex];
	#ifdef AGENTDEBUG
	std::cout  << "my friend to change with: " << changeAgent->getId() << "\n";
	#endif
	if (choiceIndex != 0){ // if index == 0, do nothing.
		if (friendVector[choiceIndex] == 0){ //if not a friend now, action = add friend
			action = 1;
		}else{ //if a friend, action is drop friend
			action = -1;
		} 
	}
	#ifdef AGENTDEBUG
	std::cout  << "action that I'll do with that friend: " << action << "\n";
	#endif
	if (action == 1) {
		mpSocialNetwork->addSocialEdge(this, changeAgent); //Add edge to network
		#ifdef AGENTDEBUG
		std::cout  << "connecting " << getId() << " to " << changeAgent->getId() << "\n";
		#endif
		mDrinkingBuddies = mpSocialNetwork->getSuccessors(this);
		mPotentialBuddies = mpSocialNetwork->updatePotentialBuddies(mPotentialBuddies, this, changeAgent);
	}else if (action == -1){
		mpSocialNetwork->removeSocialEdge(this, changeAgent);
		#ifdef AGENTDEBUG
		std::cout  << "Disconnecting: " << getId() << " to " << changeAgent->getId() << "\n";
		#endif
		mDrinkingBuddies = mpSocialNetwork->getSuccessors(this);
		mPotentialBuddies = mpSocialNetwork->updatePotentialBuddies(mPotentialBuddies, this, this);
	}else{
		mPotentialBuddies = mpSocialNetwork->updatePotentialBuddies(mPotentialBuddies, this, this);
	}
}

int Agent::selectIndexFromProbabilityVector(std::vector<double> probVector){
	double chooser = repast::Random::instance()->nextDouble();

	int index = 0;
	int selected = 0;
	#ifdef AGENTDEBUG
	std::cout << "chooser: " << chooser << "\n";
	#endif
	std::vector<double>::const_iterator it = probVector.begin();
	while (selected == 0 && it != probVector.end()){
		if (chooser < *it){
			selected = 1;
		}else{
			++index;
			++it;
		}
	}
	if (it == probVector.end()){
		index = 0;
	}
	
	return index;
}

/**
 * @brief Set opportunity
 * @details Set mTodayOpportunity to either not drinking, drink in, drink out
 * 	
 * @param probOppIn Probability drinking in provided by theory
 * @param probOppOut Probability drinking out provided by theory
 */
void Agent::generateOpportunity(double probOppIn, double probOppOut) {
	if (probOppIn==0 && probOppOut==0) {
		std::cout << "Warning: both probOppIn and probOppOut = 0." << "\n";
	}

#ifndef UNIT_TEST
	if (probOppIn + probOppOut > 1) {
		std::cerr << "In Agent::generateOpportunity, probOppIn + probOppOut must be <= 1." << "\n";
		throw MPI::Exception(MPI::ERR_ARG);
	}
	double randNumber = repast::Random::instance()->nextDouble();
	if (randNumber < probOppIn){
		//std::cout << "randNumber < probOppIn: " << randNumber << " < " << probOppIn << std::endl;
		mTodayOpportunity = Opportunity::IN;
	}else if (randNumber < probOppIn + probOppOut){
		//std::cout << "randNumber < probOppIn + probOppOut: " << randNumber << " < " << probOppIn << " + " << probOppOut <<  std::endl;
		mTodayOpportunity = Opportunity::OUT;
	}else {
		//std::cout << "randNumber > probOppIn + probOppOut: " << randNumber << " > " << probOppIn << " + " << probOppOut <<  std::endl;
		mTodayOpportunity = Opportunity::NONE;
	}
#endif


#ifdef UNIT_TEST
	if (probOppIn + probOppOut > 1) {
		std::cerr << "In Agent::generateOpportunity, probOppIn + probOppOut must be <= 1." << "\n";
		mTodayOpportunity = Opportunity::TEST_ERROR;
	}else{
		double randNumber = repast::Random::instance()->nextDouble();
		if (randNumber < probOppIn){
			std::cout << "randNumber < probOppIn: " << randNumber << " < " << probOppIn << std::endl;
			mTodayOpportunity = Opportunity::IN;
		}else if (randNumber < probOppIn + probOppOut){
			std::cout << "randNumber < probOppIn + probOppOut: " << randNumber << " < " << probOppIn << " + " << probOppOut <<  std::endl;
			mTodayOpportunity = Opportunity::OUT;
		}else {
			std::cout << "randNumber > probOppIn + probOppOut: " << randNumber << " > " << probOppIn << " + " << probOppOut <<  std::endl;
			mTodayOpportunity = Opportunity::NONE;
		}
	}
#endif
}

/**
 * @brief Get the most common drinking plan (habitual)
 * @details Return the most used drinking schema from schema history.
 * 
 * @param days Days (window) looking back into the history
 * @return DrinkingPlan with (1) schema that used the most in drinking occasions over n days and 
 *                           (2) probability = number of times the schema are used / number of active schemas last n days (active = not NONE).
 */
DrinkingPlan Agent::getMaxHabitualDrinkingPlan(int days) {
	if (days < 0 || days > 365) {
		std::cerr << "Agent::getMaxHabitualDrinkingPlan. Condition: 1 <= days <= 365" << "\n";
		throw MPI::Exception(MPI::ERR_ARG);
	}

	DrinkingPlan plan;
	int totalActiveSchema = 0;
	int countSchema[NUM_SCHEMA] = {0};
	//std::cout << "inside days: " << days << std::endl;
	vector<DrinkingSchema>::iterator it = mPastYearSchemas.end();
	while (days > 0) {
		--it; // the end() iterator points to one BEYOND the last element in a vector.
		--days;
		switch (*it) {
			case DrinkingSchema::ABSTAIN:
				//std::cout << "case0" << std::endl;
				countSchema[0]++;
				totalActiveSchema++;
				break;
			case DrinkingSchema::LOW:
				//std::cout << "case1" << std::endl;
				countSchema[1]++;
				totalActiveSchema++;
				break;
			case DrinkingSchema::MED:
				//std::cout << "case2" << std::endl;
				countSchema[2]++;
				totalActiveSchema++;
				break;
			case DrinkingSchema::HIGH:
				//std::cout << "case3" << std::endl;
				countSchema[3]++;
				totalActiveSchema++;
				break;
			case DrinkingSchema::VERY_HIGH:
				//std::cout << "case4" << std::endl;
				countSchema[4]++;
				totalActiveSchema++;
				//std::cout << "totalActiveSchema0: " << totalActiveSchema << std::endl;
				break;
			case DrinkingSchema::NONE:
				//std::cout << "case5" << std::endl;
				break;
		}
		//std::cout << "totalActiveSchema1: " << totalActiveSchema << std::endl;
	}
	
	plan.schema = DrinkingSchema::NONE;
	if (totalActiveSchema != 0){
		int maxIndex = -1;
		int maxValue = -1;
		for (int i=0; i<NUM_SCHEMA; i++) {
			if (countSchema[i] > maxValue) {
				maxValue = countSchema[i];
				maxIndex = i;
			}
			if (countSchema[i] == maxValue && 
				repast::Random::instance()->nextDouble() < 0.5) { //if equal, there is 50% chance that the max value will change (not essential)
					maxValue = countSchema[i];
					maxIndex = i;
			}
		}
		
		switch (maxIndex) {
			case 0:
				plan.schema = DrinkingSchema::ABSTAIN;
				break;
			case 1:
				plan.schema = DrinkingSchema::LOW;
				break;
			case 2:
				plan.schema = DrinkingSchema::MED;
				break;
			case 3:	
				plan.schema = DrinkingSchema::HIGH;
				break;
			case 4:	
				plan.schema = DrinkingSchema::VERY_HIGH;
				break;
		}

		plan.probability = (double) countSchema[maxIndex] / (double) totalActiveSchema;
		//std::cout << "totalActiveSchema: " << totalActiveSchema <<std::endl;
	}else {
		//this point is reached when there is no most common index found, all schemas are NONE
		//printf("Warning: Agent %d can't find the most common DrinkingSchema. TotalActiveSchema=%d", mId.id(), totalActiveSchema);
		plan.probability = 1;
	}

	return plan;
}

/**
 * @brief proportional selection on habitual drinking plan
 * @details Return a drinking schema from schema history, used more -> more likely
 * 
 * @param days Days (window) looking back into the history
 * @return DrinkingPlan with (1) selected schema (in drinking occasions over n days) and 
 *                           (2) probability = number of times the schema are used / number of active schemas last n days (active = not NONE).
 */
DrinkingPlan Agent::getProportionalSelectionHabitualDrinkingPlan(int days) {
	if (days < 0 || days > 365) {
		std::cerr << "Agent::getProportionalSelectionHabitualDrinkingPlan. Condition: 1 <= days <= 365" << "\n";
		throw MPI::Exception(MPI::ERR_ARG);
	}

	DrinkingPlan plan;
	int totalActiveSchema = 0;
	int countSchema[NUM_SCHEMA] = {0};
	vector<DrinkingSchema>::iterator it = mPastYearSchemas.end();
	while (days > 0) {
		--it; // the end() iterator points to one BEYOND the last element in a vector.
		--days;
		switch (*it) {
			case DrinkingSchema::ABSTAIN:
				countSchema[0]++;
				totalActiveSchema++;
				break;
			case DrinkingSchema::LOW:
				countSchema[1]++;
				totalActiveSchema++;
				break;
			case DrinkingSchema::MED:
				countSchema[2]++;
				totalActiveSchema++;
				break;
			case DrinkingSchema::HIGH:
				countSchema[3]++;
				totalActiveSchema++;
				break;
			case DrinkingSchema::VERY_HIGH:
				countSchema[4]++;
				totalActiveSchema++;
				break;
			case DrinkingSchema::NONE:
				break;
		}
	}
	
	plan.schema = DrinkingSchema::NONE;
	double rand = repast::Random::instance()->nextDouble() * ((double)totalActiveSchema/days);
	if (totalActiveSchema != 0) {
		int selectedIndex = -1;
		//do the proportional selection
		double cummulativeProb = 0;
		for (int i=0; i<NUM_SCHEMA; i++) {
			if (countSchema[i] > 0) {
				cummulativeProb += (double)countSchema[i] / (double)totalActiveSchema;
				if (rand <= cummulativeProb) {
					selectedIndex = i;
					break;
				}
			}
		}
		if (selectedIndex==-1) selectedIndex=NUM_SCHEMA-1;
		
		switch (selectedIndex) {
			case 0:
				plan.schema = DrinkingSchema::ABSTAIN;
				break;
			case 1:
				plan.schema = DrinkingSchema::LOW;
				break;
			case 2:
				plan.schema = DrinkingSchema::MED;
				break;
			case 3:	
				plan.schema = DrinkingSchema::HIGH;
				break;
			case 4:	
				plan.schema = DrinkingSchema::VERY_HIGH;
				break;
		}

		plan.probability = (double) countSchema[selectedIndex] / (double) totalActiveSchema;
	} else {
		//this point is reached when there is no most common index found, all schemas are NONE
		//printf("Warning: Agent %d can't find the most common DrinkingSchema. TotalActiveSchema=%d", mId.id(), totalActiveSchema);
		plan.probability = 1;
	}

	return plan;
}

DrinkingPlan Agent::getProportionalSelectionHabitualDrinkingPlan() {
	//random a number from 0 -> total prob
	double totalProb = 0;
	for (int i=0; i<NUM_SCHEMA; i++)
		totalProb += mHabitProbability[i];
	double rand = repast::Random::instance()->nextDouble() * totalProb;

	//do the proportional selection
	int selectedIndex = -1;
	double cummulativeProb = 0;
	for (int i=0; i<NUM_SCHEMA; i++) {
		if (mHabitProbability[i] > 0) {
			cummulativeProb += mHabitProbability[i];
			if (rand <= cummulativeProb) {
				selectedIndex = i;
				break;
			}
		}
	}
	if (selectedIndex==-1) {
		std::cerr << "Agent::getProportionalSelectionHabitualDrinkingPlan. Proportional selection failed." << std::endl;
		throw MPI::Exception(MPI::ERR_OTHER);
	};

	DrinkingPlan plan;
	switch (selectedIndex) {
		case 0:
			plan.schema = DrinkingSchema::ABSTAIN;
			break;
		case 1:
			plan.schema = DrinkingSchema::LOW;
			break;
		case 2:
			plan.schema = DrinkingSchema::MED;
			break;
		case 3:	
			plan.schema = DrinkingSchema::HIGH;
			break;
		case 4:	
			plan.schema = DrinkingSchema::VERY_HIGH;
			break;
	}
	plan.probability = mHabitProbability[selectedIndex];
	return plan;
}
/**
 * @brief Perform drinking
 * @details Get drink from schema, update related variables. This method replaces doDrinkingEngine().
 * 
 * @param schema A selected schema
 */
void Agent::doDrinking(DrinkingSchema schema) {
	if (schema==DrinkingSchema::NONE) {
		std::cerr << "Drinking schema must NOT be NONE." << std::endl;
		throw MPI::Exception(MPI::ERR_OTHER);
	}
	if (schema<DrinkingSchema::NONE || schema>DrinkingSchema::VERY_HIGH) {
		std::cerr << "Drinking schema out of range." << std::endl;
		printf("Agent id %d. Tick %.1f.\n", mId.id(), repast::RepastProcess::instance()->getScheduleRunner().currentTick());
		throw MPI::Exception(MPI::ERR_OTHER);
	}
	mTodaySchema = schema;
	int numberOfDrinks = getDrinksFromSchema(schema);

	mIsDrinkingToday = (numberOfDrinks > 0);
	mNumberDrinksToday = numberOfDrinks;
	
	mTotalDrinksPerAnnum += numberOfDrinks;
	#ifdef DEBUG
		if (mId.id() == IDFOROUTPUT){
			std::cout<<"mTotalDrinksPerAnnum = "<<mTotalDrinksPerAnnum<<std::endl;
			std::cout<<"mNumberDrinksToday = "<<mNumberDrinksToday<<std::endl;
		}
	#endif
	if (mIsDrinkingToday) {mIs12MonthDrinker = true;} //turn on the 12-month drinker flag if this agent drinks
}

/**
 * @brief Return number of drinks from a schema
 */
int Agent::getDrinksFromSchema(DrinkingSchema schema) {
	int numberOfDrinks = 0;
	double rand = repast::Random::instance()->nextDouble();
	if (mSex == MALE) {
		switch (schema) {
			case DrinkingSchema::NONE:
			case DrinkingSchema::ABSTAIN:
				numberOfDrinks = 0;
				break;
			case DrinkingSchema::LOW: {
				if (mGramsPerDay < 40)
					numberOfDrinks = rand<0.65 ? 1 : 2;
				else if (mGramsPerDay < 60)
					numberOfDrinks = rand<0.11 ? 1 : 2;
				else if (mGramsPerDay < 100)
					numberOfDrinks = rand<0.02 ? 1 : 2;
				else
					numberOfDrinks =rand<0.001 ? 1 : 2;
				break; }
			case DrinkingSchema::MED: {
				if (mGramsPerDay < 40)
					numberOfDrinks = rand<0.62 ? 3 : 4;
				else if (mGramsPerDay < 60)
					numberOfDrinks = rand<0.58 ? 3 : 4;
				else if (mGramsPerDay < 100)
					numberOfDrinks = rand<0.46 ? 3 : 4;
				else
					numberOfDrinks = rand<0.50 ? 3 : 4;
				break; }
			case DrinkingSchema::HIGH: {
				double shape, scale;
				if (mGramsPerDay < 40) {
					shape=5.282; scale=1/0.426;
				} else {
					shape=4.856; scale=1/0.347;
				}
  				numberOfDrinks = sampleFromGammaDistPdf(shape,scale,5,6);
				break; }
			case DrinkingSchema::VERY_HIGH: {
				double shape, scale;
				if (mGramsPerDay < 40) {
					shape=5.282; scale=1/0.426;
				} else {
					shape=4.856; scale=1/0.347;
				}
  				numberOfDrinks = sampleFromGammaDistPdf(shape,scale,8,MAX_DRINKS);
				break; }
		}
	} else {
		switch (schema) {
			case DrinkingSchema::NONE:
			case DrinkingSchema::ABSTAIN:
				numberOfDrinks = 0;
				break;
			case DrinkingSchema::LOW: {
				numberOfDrinks = 1;
				break; }
			case DrinkingSchema::MED: {
				numberOfDrinks = 2;
				break; }
			case DrinkingSchema::HIGH: {
				if (mGramsPerDay < 20)
					numberOfDrinks = rand<0.62 ? 3 : 4;
				else if (mGramsPerDay < 40)
					numberOfDrinks = rand<0.58 ? 3 : 4;
				else if (mGramsPerDay < 60)
					numberOfDrinks = rand<0.46 ? 3 : 4;
				else
					numberOfDrinks = rand<0.50 ? 3 : 4;
				break; }
			case DrinkingSchema::VERY_HIGH: {
				//gamma distribution
				double shape, scale;
				if (mGramsPerDay < 40) {
					shape=4.086; scale=1/0.562;
				} else {
					shape=2.794; scale=1/0.295;
				}
				numberOfDrinks = sampleFromGammaDistPdf(shape,scale,5,MAX_DRINKS);
				break; }
		}
	}
	return numberOfDrinks;
}

/**
 * @brief Return a schema given a number of drinks and abstain-rate
 * @details Follow the definition to return a schema. Currently no overlap in number of drinks.
 * 
 * @param drinks A number of drinks
 * 
 * @return A schema
 */
DrinkingSchema Agent::getSchemaFromDrinks(int drinks) {
	DrinkingSchema schema;
	if (mSex == MALE) {
		if (drinks == 0)
			schema = DrinkingSchema::ABSTAIN;
		else if (0 < drinks && drinks <= 2)
			schema = DrinkingSchema::LOW;
		else if (2 < drinks && drinks <= 4)
			schema = DrinkingSchema::MED;
		else if (4 < drinks && drinks <= 7)
			schema = DrinkingSchema::HIGH;
		else if (7 < drinks)
			schema = DrinkingSchema::VERY_HIGH;
	} else {
		if (drinks == 0)
			schema = DrinkingSchema::ABSTAIN;
		else if (0 < drinks && drinks <= 1)
			schema = DrinkingSchema::LOW;
		else if (1 < drinks && drinks <= 2)
			schema = DrinkingSchema::MED;
		else if (2 < drinks && drinks <= 4)
			schema = DrinkingSchema::HIGH;
		else if (4 < drinks)
			schema = DrinkingSchema::VERY_HIGH;
	}
	return schema;
}

/**
 * @brief Init schemas history based on drinks hisotry
 * @details For each drinks in the history, used getSchemaFromDrinks to infer the schema.
 * Also init the habitual stock, proportion, and probability.
 */
void Agent::initPastYearSchemas() {
	//For each drink, generate a schema
	mPastYearSchemas.clear();
	for (int i=0; i<365; i++) {
		mPastYearSchemas.push_back(getSchemaFromDrinks(mPastYearDrinks[i]));
	}

	//reset habitual arrays to 0: stock and proportion
	for (int i=0; i<NUM_SCHEMA; i++) {
		mHabitStock[i]=0;
		mHabitProbability[i]=0;
	}

	//count habitual stock for the whole years
	for (int i=0; i<365; i++) {
		mHabitStock[static_cast<int>(mPastYearSchemas[i])]++;
	}
	
	for (int i=0; i<NUM_SCHEMA; i++) {
		//init habitual probability for the whole years
		mHabitProbability[i] = (double)mHabitStock[i] / 365.0;

		//update habitual stock to the HABIT_STOCK_DAYS window
		mHabitStock[i] = mHabitProbability[i]*HABIT_STOCK_DAYS;
	}
}

//over the past "numberOfDays" days (inclusive) on how many days the agents use a certain schema
int* Agent::getSchemaCountOverNDays(int numberOfDays) {
	//std::array<int, NUM_SCHEMA> countSchemas{};
	int* countSchemas = new int[NUM_SCHEMA]{};
	//int countSchemas[NUM_SCHEMA] = {0};
	int index = mPastYearDrinks.size()-1;
	while (numberOfDays > 0) {
		countSchemas[static_cast<int>(mPastYearSchemas[index])]++;
		index--;
		numberOfDays--;
	}
	return countSchemas;
}

void Agent::initPastYearSchemasDrinks(std::vector<double> schemaProfile) {
	mPastYearSchemas.clear();
	mPastYearDrinks.clear();
	//std::vector<int> pastYearDrinks;
	//std::vector<DrinkingSchema> pastYearSchemas;
	int tempTotalDrinks = 0;

	int id = repast::strToInt(to_string(mId.id()));

	double totalProb = 0;
	for (int i=0; i<NUM_SCHEMA; i++)
		totalProb += schemaProfile[i];

	//propotional select schema every day
	for (int day=0; day<365; day++) {
		//special case for abstainer
		if (!mIs12MonthDrinker) {
			mPastYearSchemas.push_back(DrinkingSchema::ABSTAIN);
			mPastYearDrinks.push_back(0);
			continue; //go to next day of the for loop
		}

		//random a number from 0 -> total prob
		double rand = repast::Random::instance()->nextDouble() * totalProb;

		//do the proportional selection
		int selectedIndex = -1;
		double cummulativeProb = 0;
		for (int i=0; i<NUM_SCHEMA; i++) {
			if (schemaProfile[i] > 0) {
				cummulativeProb += schemaProfile[i];
				if (rand <= cummulativeProb) {
					selectedIndex = i;
					break;
				}
			}
		}
		if (selectedIndex==-1) {
			std::cerr << "initPastYearSchemasDrinks. Proportional selection failed." << std::endl;
			throw MPI::Exception(MPI::ERR_OTHER);
		};

		//Add schema to past year vector

		//NOTE: both the following 2 lines (cast & static_cast) will FAIL when adding the schema to vector with push_back
		//DrinkingSchema schema = (DrinkingSchema) (selectedIndex+1);
		//DrinkingSchema schema = static_cast<DrinkingSchema>(selectedIndex+1);
		
		//MANUALLY define schema - NEED UPDATED if DrinkingSchema index changes
		DrinkingSchema schema;
		switch (selectedIndex+1) {
			case 1: schema = DrinkingSchema::ABSTAIN; break;
			case 2: schema = DrinkingSchema::LOW; break;
			case 3: schema = DrinkingSchema::MED; break;
			case 4: schema = DrinkingSchema::HIGH; break;
			case 5: schema = DrinkingSchema::VERY_HIGH; break;
			default: schema = DrinkingSchema::NONE;
		}
		mPastYearSchemas.push_back(schema);

		//equivalent init for drinks
		int drinks = getDrinksFromSchema(schema);
		mPastYearDrinks.push_back(drinks);

		tempTotalDrinks += drinks;
	}

	//calculate mean and variance from history using Welford method
	mPastYearN = 0;
	mPastYearMeanDrink = 0;
	mPastYearSquaredDistanceDrink = 0;
	double oldMean = 0;
	for (int k=1; k<=365; k++) { //length of past-year-drink vector must be 365
		int x = mPastYearDrinks[k-1];
		if (x!=0) { //only account for drinking days (number of drinks > 0)
			mPastYearN++;
			oldMean = mPastYearMeanDrink;
			mPastYearMeanDrink = mPastYearMeanDrink + (x - mPastYearMeanDrink) / mPastYearN;
			mPastYearSquaredDistanceDrink = mPastYearSquaredDistanceDrink + (x - mPastYearMeanDrink)*(x-oldMean);
		}
	}

	mMeanDrinksToday = mPastYearMeanDrink;
	mSDDrinksToday = mPastYearSquaredDistanceDrink;

	//reset habitual arrays to 0: stock and proportion
	for (int i=0; i<NUM_SCHEMA; i++) {
		mHabitStock[i]=0;
		mHabitProbability[i]=0;
	}
/*
	//count habitual stock for the whole years
	for (int i=0; i<365; i++) {
		mHabitStock[static_cast<int>(mPastYearSchemas[i])]++;
	}
	
	for (int i=0; i<NUM_SCHEMA; i++) {
		//init habitual probability for the whole years
		mHabitProbability[i] = (double)mHabitStock[i] / 365.0;

		//update habitual stock to the HABIT_STOCK_DAYS window
		mHabitStock[i] = mHabitProbability[i]*HABIT_STOCK_DAYS;
	}
*/
	//printf("%d\t%d\t%.1f\t%.1f\n", mId.id(), mSex, mBaselineMonthlyDrink, tempTotalDrinks/12.0);
}

void Agent::updateHabitProbabilities(int numberOfDays) {
	int* schemaCount = getSchemaCountOverNDays(numberOfDays);
	for (int i=0; i<NUM_SCHEMA; i++)
		mHabitProbability[i] = (double)schemaCount[i] / numberOfDays;
}

int Agent::sampleFromGammaDistPdf(double shape, double scale, int min, int max) {
	//create pdf array from min to max
	boost::math::gamma_distribution<> gd(shape,scale);
	double *pdfs{ new double[max-min+1]{} };
	double total = 0;
	for (int x=0; x<=max-min; x++) {
		pdfs[x] = boost::math::pdf(gd,x+min); //pdf value
		total += pdfs[x];
	}
	
	//do the proportional selection
	double rand = repast::Random::instance()->nextDouble() * total;
	int selectedIndex = -1;
	double cummulativeProb = 0;
	for (int i=0; i<=max-min; i++) {
		if (pdfs[i] > 0) {
			cummulativeProb += pdfs[i];
			if (rand <= cummulativeProb) {
				selectedIndex = i;
				break;
			}
		}
	}
	if (selectedIndex==-1) {
		std::cerr << "sampleFromGammaDistPdf. Proportional selection failed." << std::endl;
		throw MPI::Exception(MPI::ERR_OTHER);
	};

	//return the x-axis value in the min-max range
	return selectedIndex + min;
}

DrinkingSchema Agent::drawSchemaFromHabitBag() {
	if (mHabitBag.empty()) updateHabitBag(mHabitUpdateInterval);
	DrinkingSchema schema = mHabitBag.back();
	mHabitBag.pop_back();
	return schema;
}

void Agent::updateHabitBag(int numberOfDays) {
	mHabitBag.clear();
	for (int i=365-1; i>=365-numberOfDays; i--) {
		mHabitBag.push_back(mPastYearSchemas[i]);
	}
	//shuffleList(mHabitBag);
}

std::vector<double> Agent::initSchemaProfiles() {
	//convert monthly drink to grams per days
	double gramsPerDay = (double)mBaselineMonthlyDrink*14.0/30;

	//convert frequency level to number of drinking day
	int pastYearDrinksLength;
	switch(mDrinkFrequencyLevel){
		case 1:
			if (mIs12MonthDrinker == false){
				pastYearDrinksLength = 0;
			}else {
				pastYearDrinksLength = 1 + repast::Random::instance()->nextDouble() * 10;
				mDrinkFrequencyLevel = 2;
			}
			break;
		case 2:
			pastYearDrinksLength = 11 + repast::Random::instance()->nextDouble() * 7;
			break;
		case 3:
			pastYearDrinksLength = 19 + repast::Random::instance()->nextDouble() * 11;
			break;
		case 4:
			pastYearDrinksLength = 31 + repast::Random::instance()->nextDouble() * 22;
			break;
		case 5:
			pastYearDrinksLength = 54 + repast::Random::instance()->nextDouble() * 117;
			break;
		case 6:
			pastYearDrinksLength = 172 + repast::Random::instance()->nextDouble() * 164;
			break;
		case 7:
			pastYearDrinksLength = 337 + repast::Random::instance()->nextDouble() * 28;
			break;
	}

	double tempSchema1 = 0 + 0*gramsPerDay +  0*mAge + 0*mSex + 0*pastYearDrinksLength;
	double tempSchema2 = -4.00204 + -0.23119*gramsPerDay +  -0.0016*mAge + 0.552734*mSex + 0.034208*pastYearDrinksLength;
	double tempSchema3 = -3.90982 + -0.01887*gramsPerDay +  -0.0085*mAge + -7.26777*mSex + 0.020559*pastYearDrinksLength;
	double tempSchema4 = -4.52732 + 0.034568*gramsPerDay +  -0.01884*mAge + 0.740086*mSex + 0.013298*pastYearDrinksLength;
	double tempSchema5 = -4.26343 + 0.065567*gramsPerDay +  -0.02472*mAge + -0.99995*mSex + 0.006273*pastYearDrinksLength;
	double tempSum = exp(tempSchema1) + exp(tempSchema2) + exp(tempSchema3) + exp(tempSchema4) + exp(tempSchema5);
	
	std::vector<double> schemaProfile;
	schemaProfile.push_back(exp(tempSchema1) / tempSum);
	schemaProfile.push_back(exp(tempSchema2) / tempSum);
	schemaProfile.push_back(exp(tempSchema3) / tempSum);
	schemaProfile.push_back(exp(tempSchema4) / tempSum);
	schemaProfile.push_back(exp(tempSchema5) / tempSum);
	return schemaProfile;
}
