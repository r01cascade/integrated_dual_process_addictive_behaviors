#include "MediatorForOneTheory.h"

#include "TheoryMediator.h"
#include "Theory.h"

MediatorForOneTheory::MediatorForOneTheory(std::vector<Theory*> theoryList) : TheoryMediator(theoryList) {
	mpTheory = theoryList[0];
}

void MediatorForOneTheory::mediateSituation() {
	mpTheory->doSituation();

	//mediate drinking opportunities
	mpTheory->updateDrinkingOpportunity();
	mProbOppIn = mpTheory->getProbOppIn();
	mProbOppOut = mpTheory->getProbOppOut();
}

void MediatorForOneTheory::mediateAction() {
	//mediate thought pathway: calc TPB
	mediateThoughtPathway();

	//get probabilities from TPB
	updateIntentionProbabilities();
}

void MediatorForOneTheory::mediateGatewayDisposition() {
	mpTheory->doGatewayDisposition();
}

void MediatorForOneTheory::mediateNextDrinksDisposition() {
	mpTheory->doNextDrinksDisposition();
}

void MediatorForOneTheory::mediateNonDrinkingActions() {
	mpTheory->doNonDrinkingActions();
}

void MediatorForOneTheory::mediateThoughtPathway() {
	DrinkingSchema schema;
	for (int i=0; i<NUM_SCHEMA; i++) {
		schema = static_cast<DrinkingSchema>(i);
		mAttitude[i] = mpTheory->getAttitude(schema);
		mNorm[i] = mpTheory->getNorm(schema);
		mPbc[i] = mpTheory->getPerceivedBehaviourControl(schema);
	}
}